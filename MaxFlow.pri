message('Include MaxFlow Src Files!')

MAXFLOWPATH = $$(HOME)/SharedLibraries/include/MaxFlow

HEADERS += $$MAXFLOWPATH/*.h
HEADERS += $$MAXFLOWPATH/*.inc
SOURCES += $$MAXFLOWPATH/*.cpp
