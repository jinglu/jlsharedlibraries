message('Include ALGLIB!')
SHAREROOT = $$(HOME)/SharedLibraries
contains(QMAKE_HOST.arch, x86_64) {
    ARCH = 64
    message("Building for 64 bit")
}
!contains(QMAKE_HOST.arch, x86_64) {
    ARCH = 32
    message("Building for 32 bit")
}

INCLUDEPATH += $$SHAREROOT/include
#HEADERS += $$SHAREINCLUDEPATH/ALGLIB/*.h
#SOURCES += $$SHAREINCLUDEPATH/ALGLIB/*.cpp
LIBS += -L$$$$SHAREROOT/lib$$ARCH -lShareAlglib


