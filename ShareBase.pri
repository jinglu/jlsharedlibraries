message('Include ShareBase!')
message('Include Opencv Libriries!')
#opencv
unix{
CONFIG += link_pkgconfig
PKGCONFIG += opencv
}

JLSHARELIB = $${PWD}
SHAREINCLUDEPATH = $$JLSHARELIB/include
INCLUDEPATH += $$SHAREINCLUDEPATH
message($$SHAREINCLUDEPATH)

contains(QMAKE_HOST.arch, x86_64) {
    message("Building for 64 bit")
    LIBS += -L$$JLSHARELIB/lib64
}

!contains(QMAKE_HOST.arch, x86_64) {
    message("Building for 32 bit")
    LIBS += -L$$JLSHARELIB/lib32
}


HEADERS += $$SHAREINCLUDEPATH/Numeric/*.h
HEADERS += $$SHAREINCLUDEPATH/Primitives/*.h
HEADERS += $$SHAREINCLUDEPATH/Common/*.h
HEADERS += $$SHAREINCLUDEPATH/Image/*.h
#HEADERS += $$SHAREINCLUDEPATH/Learning/*.h


SOURCES += $$SHAREINCLUDEPATH/Numeric/*.cpp
SOURCES += $$SHAREINCLUDEPATH/Primitives/*.cpp
SOURCES += $$SHAREINCLUDEPATH/Common/*.cpp
SOURCES += $$SHAREINCLUDEPATH/Image/*.cpp
#SOURCES += $$SHAREINCLUDEPATH/Learning/*.cpp

