#check the architecture
message("Using shared configuration!")
contains(QMAKE_HOST.arch, x86_64) {
    ARCH = 64
    message("Building for 64 bit")
}
!contains(QMAKE_HOST.arch, x86_64) {
    ARCH = 32
    message("Building for 32 bit")
}
#check debug or release
CONFIG(debug, debug|release) {
    DEBUGSUFFIX = d
 message("Building debug")
}


