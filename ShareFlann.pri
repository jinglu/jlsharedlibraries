message('Include flann!')
SHAREINCLUDEPATH = $$(HOME)/SharedLibraries/include

INCLUDEPATH += $$(HOME)/SharedLibraries/include/flann

HEADERS += $$SHAREINCLUDEPATH/flann/algorithms/*.h
SOURCES += $$SHAREINCLUDEPATH/flann/algorithms/*.cpp

HEADERS += $$SHAREINCLUDEPATH/flann/nn/*.h
SOURCES += $$SHAREINCLUDEPATH/flann/nn/*.cpp

HEADERS += $$SHAREINCLUDEPATH/flann/util/*.h
SOURCES += $$SHAREINCLUDEPATH/flann/util/*.cpp

HEADERS += $$SHAREINCLUDEPATH/flann/*.h
SOURCES += $$SHAREINCLUDEPATH/flann/*.cpp
