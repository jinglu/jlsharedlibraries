message('Include Vlfeat!')
VLDIR = $$PWD/include/vlfeat
#message($$VLDIR)
!exists($$VLDIR) {
error('Cannot include vlfeat directory!')
}

INCLUDEPATH += $${VLDIR}
HEADERS += $$VLDIR/vl/*.h
SOURCES += $$VLDIR/vl/*.c

#HEADERS += $$SHAREINCLUDEPATH/vlfeat/src/*.h
#SOURCES += $$SHAREINCLUDEPATH/vlfeat/src/*.c
