#include "FileAPI.h".h"
#include <fstream>
#include <iostream>

using namespace std;

string FileAPI::extractFileName(std::string filename, bool withExtension = true)
{
  string result;
  unsigned int pos = filename.rfind("/");
  if (pos == string::npos)
    result = filename;
  else {
    result = filename.substr(pos+1);
  }
  if (!withExtension) {
    pos = result.rfind(".");
    if (pos != string::npos)
      result = result.substr(0,pos);
  }
  return result;
}

string FileAPI::extractPath(string filename)
{
      string result;
      int pos = filename.rfind("/");
      if (pos < 0) result = ".";
      else result = filename.substr(0,pos);

      return result;
}

string FileAPI::extractPrefix(string filename)
{
    string path = FileAPI::extractPath(filename);
    string rawname = FileAPI::extractFileName(filename,false);
    string ret = path + "/" + rawname;
    return ret;
}

bool FileAPI::fileExistCheck(const char* filename,ifstream& inputfile)
{
    bool flag = true;
    inputfile.open(filename);
    if(!inputfile){
        std::cout<<"Loading file failed: "<<filename<<endl;
        flag = false;
        inputfile.close();
    }
    return flag;
}
