#ifndef FILEUTIL_H
#define FILEUTIL_H
#include <string>
#include <fstream>
namespace FileAPI{

std::string extractFileName(std::string filename,bool withExtension);
std::string extractPath(std::string filename);
std::string extractPrefix(std::string fileanem);
bool fileExistCheck(const char* filename,std::ifstream& inputfile);
}
#endif // FILEUTIL_H
