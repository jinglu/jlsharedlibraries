#include "StatisticAPI.h"
#include <iostream>
#include <iomanip>
#include <list>
using namespace std;

//void StatisticAPI::analysisConfusionMat(const vector<string>& cateNames, const cv::Mat& confusionMatrix)
//{
//    float total_local_count = 0;
//    float total_correct_local_count = 0;

//    int i,j;
//    int numclass = confusionMatrix.rows;
//    cout<<endl;
//    cout<<"global confusion matrix: (" <<confusionMatrix.rows<<" * "<<confusionMatrix.cols<<")" <<endl;
//    int printw = 16;

//    cout<<setw(printw)<<" ";
//    for(i=0;i<numclass;i++)
//    {
//        cout<<setw(printw)<<left<<cateNames[i];
//    }
//    cout<<endl;
//    cout << "end name " << endl;
//    for(i=0;i<numclass;i++)
//    {
//        int count = 0;
//        for(j=0;j<numclass;j++) count+=confusionMatrix.at<int>(i,j);

//        cout.precision(3) ;
//        cout<<setw(printw)<<left<<cateNames[i];
//        for(j=0;j<numclass;j++)
//        {
//            if(count>0)
//                cout<<setw(printw)<<left<<float(confusionMatrix.at<int>(i,j));
//            else
//                cout<<setw(printw)<<left<<0;
//        }
//        cout<<endl;
//    }

//    int total_count = 0;
//    int correct_count = 0;
//    for(i=0;i<numclass;i++)
//    {
//        for(j=0;j<numclass;j++)
//            total_count+=confusionMatrix.at<int>(i,j);
//        correct_count+=confusionMatrix.at<int>(i,i);
//    }

//    double AverageCorrectRate = float(correct_count)/float(total_count);
//    cout<<endl;
//    cout<<"overall accuracy: "<<AverageCorrectRate<<"("<<correct_count<<","<<total_count<<")"<<endl;

//    //precision-recall
//    cout<<"recall-precision:"<<endl;
//    for(i=0;i<numclass;i++)
//    {
//        cout<<cateNames[i]<<": ";
//        int row_total_count = 0;
//        int col_total_count = 0;

//        for(j=0;j<numclass;j++)
//        {
//            row_total_count+=confusionMatrix.at<int>(i,j);
//            col_total_count+=confusionMatrix.at<int>(j,i);
//        }

//        if(row_total_count>0)
//        {
//            double recall = float(confusionMatrix.at<int>(i,i))/float(row_total_count);
//            cout<<recall<<" ";
//            double precision = float(confusionMatrix.at<int>(i,i))/float(col_total_count);
//            cout<<precision<<" ";
//        }
//        cout<<endl;
//    }
//}
void StatisticAPI::analysisConfusionMat(const std::vector<std::string>& cateNames, const cv::Mat& confusionMatrix)
{
    double total_local_count = 0;
    double total_correct_local_count = 0;

    int i,j;
    int numclass = confusionMatrix.rows;
    std::cout<<std::endl;
    std::cout<<"global confusion matrix: (" <<confusionMatrix.rows<<" * "<<confusionMatrix.cols<<")" <<std::endl;
    int printw = 16;

    std::cout<<std::setw(printw)<<" ";
    for(i=0;i<numclass;i++)
    {
        std::cout<<std::setw(printw)<<std::left<<cateNames[i];
    }
    std::cout<<std::endl;
    std::cout << "end name " << std::endl;
    for(i=0;i<numclass;i++)
    {
        int count = 0;
        for(j=0;j<numclass;j++) count+=confusionMatrix.at<double>(i,j);

        std::cout.precision(3) ;
        std::cout<<std::setw(printw)<<std::left<<cateNames[i];
        for(j=0;j<numclass;j++)
        {
            if(count>0)
                std::cout<<std::setw(printw)<<std::left<<double(confusionMatrix.at<double>(i,j));
            else
                std::cout<<std::setw(printw)<<std::left<<0;
        }
        std::cout<<std::endl;
    }

    int total_count = 0;
    int correct_count = 0;
    for(i=0;i<numclass;i++)
    {
        for(j=0;j<numclass;j++)
            total_count+=confusionMatrix.at<double>(i,j);
        correct_count+=confusionMatrix.at<double>(i,i);
    }

    double AverageCorrectRate = double(correct_count)/double(total_count);
    std::cout<<std::endl;
    std::cout<<"overall accuracy: "<<AverageCorrectRate<<"("<<correct_count<<","<<total_count<<")"<<std::endl;

    //precision-recall
    std::cout<<std::setw(printw)<<std::left<<"type:"<<"recall - "<<"precision:"<<std::endl;
    for(i=0;i<numclass;i++)
    {
        std::cout<<std::setw(printw)<<std::left<<cateNames[i]<<": ";
        int row_total_count = 0;
        int col_total_count = 0;

        for(j=0;j<numclass;j++)
        {
            row_total_count+=confusionMatrix.at<double>(i,j);
            col_total_count+=confusionMatrix.at<double>(j,i);
        }

        if(row_total_count>0)
        {
            double recall = double(confusionMatrix.at<double>(i,i))/double(row_total_count);
            //std::cout<<recall<<"\t";
            double precision = double(confusionMatrix.at<double>(i,i))/double(col_total_count);
            std::cout<<std::setw(printw)<<std::left<<recall<<precision<<" ";
        }
        std::cout<<std::endl;
    }
}
void StatisticAPI::printConfusionMat(const vector<string> &cateNames, const cv::Mat &confusionMatrix)
{
    int numclass = cateNames.size();
    int printw = 16;
    cout<<setw(printw)<<" ";
    for(int i=0;i<numclass;i++)
    {
        cout<<setw(printw)<<left<<cateNames[i];
    }
    cout<<endl;
    cout << "end name " << endl;
    for(int i=0;i<numclass;i++)
    {
        int count = 0;
        for(int j=0;j<numclass;j++) count+=confusionMatrix.at<double>(i,j);

        cout.precision(3) ;
        cout<<setw(printw)<<left<<cateNames[i];
        for(int j=0;j<numclass;j++)
        {
            if(count>0)
                cout<<setw(printw)<<left<<double(confusionMatrix.at<double>(i,j));
            else
                cout<<setw(printw)<<left<<0;
        }
        cout<<endl;
    }

}

void StatisticAPI::generateRandomSequence(const int s, const int e, const int length, vector<int>& seq)
{
    vector<int> randomized_list;
    for(int i = s;i<e;i++)
    {
        randomized_list.push_back(i);
    }
    std::random_shuffle(randomized_list.begin(),randomized_list.end());

    seq.clear();
    if(randomized_list.size() > length)
    {
        float sampleStep = float(randomized_list.size())/float(length);
        sampleStep = max(float(1.0),sampleStep);

        list<int> selectedIndex;
        float v = 0;
        while(v < randomized_list.size() - 1)
        {
            selectedIndex.push_back(v);
            v += sampleStep;
        }
        selectedIndex.sort();
        selectedIndex.unique();
        for(list<int>::iterator iter = selectedIndex.begin(); iter != selectedIndex.end();iter++)
        {
            seq.push_back(randomized_list.at(*iter));
            //cout<<randomized_list.at(*iter)<<" ";
        }
        //cout<<endl;
    }
    else
    {
        seq = randomized_list;
    }

//	if(s < e && length > 0)
//	{
//		int maxLength =  e - s + 1;
//		if( maxLength < length)
//		{
//			cout<<"sequence length smaller than request sequence length... ";
//			return;
//		}
//
//		seq.clear();
//		if(maxLength == length)
//		{
//			for(int i = s;i<=e;i++) seq.push_back(i);
//		}
//		else
//		{
//			vector<int> candidates;
//			for(int i = s;i<=e;i++) candidates.push_back(i);
//
//			for(int i=0;i<length;i++)
//			{
//				int idx = rand()%candidates.size();
//				seq.push_back(candidates.at(idx));
//		        candidates.erase(candidates.begin()+idx);
//			}
//		}
//	}
}

