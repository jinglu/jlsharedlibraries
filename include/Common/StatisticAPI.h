#ifndef STATISTICAPI_H
#define STATISTICAPI_H
#include <opencv2/core/core.hpp>
namespace StatisticAPI
{
    void analysisConfusionMat(const std::vector<std::string>& cateNames, const cv::Mat& confusionMatrix);
    void printConfusionMat(const std::vector<std::string>& cateNames, const cv::Mat& confusionMatrix);

    void generateRandomSequence(const int s, const int e, const int length, std::vector<int>& seq);
}

#endif // STATISTICAPI_H
