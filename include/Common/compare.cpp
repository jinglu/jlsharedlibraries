#include "compare.h"
#include <assert.h>

using namespace wjl;

template<class T>
bool greater_second<T>::operator()(const T& lhs, const T& rhs)
{
      return lhs.second > rhs.second;
};

template<class T>
bool less_second<T>::operator()(const T& lhs, const T& rhs)
{
      return lhs.second < rhs.second;
};

template<class T>
bool less_min<T>::operator()(const T& lhs, const T& rhs)
{
      return (std::min(lhs.first, lhs.second) < std::min(rhs.first,rhs.second) );
};


template class greater_second<std::pair<int, float> >;
template class greater_second<std::pair<int, int> >;
template class greater_second<std::pair<int, double> >;
template class greater_second<std::pair<unsigned int, unsigned int> >;
template class greater_second<std::pair<unsigned int, float> >;
template class greater_second<std::pair<unsigned int, double> >;

template class less_second<std::pair<int, float> >;
template class less_second<std::pair<int, int> >;
template class less_second<std::pair<int, double> >;
template class less_second<std::pair<unsigned int, unsigned int> >;
template class less_second<std::pair<unsigned int, float> >;
template class less_second<std::pair<unsigned int, double> >;

template class less_min<std::pair<int, int> >;
template class less_min<std::pair<double, double> >;



