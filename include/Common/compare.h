#ifndef COMPARE_H
#define COMPARE_H

#include <map>
#include <functional>
#include <utility>
#include <vector>
#include <assert.h>

namespace wjl {


template<class T>
class greater_second : public std::binary_function<T,T,bool>
{
public:
    inline bool operator()(const T& lhs, const T& rhs);
};

template<class T>
class less_second : public std::binary_function<T,T,bool>
{
public:
    inline bool operator()(const T& lhs, const T& rhs);
};

template<class T>
class less_min : public std::binary_function<T,T,bool>
{
public:
    inline bool operator()(const T& lhs, const T& rhs);
};

/* (a-b) > value ? (a-b) : value
 */
template <typename T1, typename T2, typename T3>
T1 subtract_above(const T1& lhs, const T2& rhs, const T3 value)
{
    T1 diff = lhs - rhs;
    return ((diff > value) ? diff : value);
}

/* (a+b) < value ? (a+b) : value
 */
template <typename T1, typename T2, typename T3>
T1 summary_below(const T1& lhs, const T2& rhs, const T3 value)
{
    T1 sum = lhs + rhs;
    return ((sum < value) ? sum : value);
}

template <typename T>
T vec_dist2(const std::vector<T>& lhs, const std::vector<T>& rhs)
{
    assert(lhs.size() == rhs.size());
    T sum = T(0);
    for (unsigned int i = 0; i < lhs.size(); ++i)
    {
        sum += (lhs[i] - rhs[i]) * (lhs[i] - rhs[i]);
    }
    return sum;
}


}

#endif // COMPARE_H
