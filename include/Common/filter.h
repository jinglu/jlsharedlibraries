#ifndef FILTER_H
#define FILTER_H

#include <vector>
#include "global.h"
#include "utilities.h"

/* make filters */
/* usage: make_fgauss(sigma) */
#define WIDTH 4.0
template <class T>
inline T square(const T &x) { return x*x; };

/* normalize mask so it integrates to one */
static void normalize(std::vector<REAL> &mask) {
  int len = mask.size();
  REAL sum = 0.;
  for (int i = 1; i < len; i++) {
    sum += fabs(mask[i]);
  }
  sum = 2*sum + fabs(mask[0]);
  for (int i = 0; i < len; i++) {
    mask[i] /= sum;
  }
}

#define MAKE_FILTER(name, fun)                                \
static std::vector<REAL> make_ ## name (REAL sigma, int width) {       \
  sigma = std::max(sigma, 0.01);                             \
  int len = (int)ceil(sigma * width) + 1;                     \
  std::vector<REAL> mask(len);                               \
  for (int i = 0; i < len; i++) {                             \
    mask[i] = fun;                                            \
  }                                                           \
    normalize(mask);                                            \
  return mask;                                                \
}

MAKE_FILTER(fgauss, exp(-0.5*square(i/sigma)));

/* convolve src with mask.  dst is not flipped! */
static void filter_odd(const std::vector<REAL>& src, const std::vector<REAL>& mask, std::vector<REAL>& dst)
{
    unsigned int len = mask.size();
    assert(len%2==1);
    unsigned int half_len = len/2;

    dst = std::vector<REAL>(src.size());

    for (unsigned int i = 0; i < src.size(); ++i)
    {
        if (i < half_len)
        {
            dst[i] = src[i];
        }
        else if (i > src.size()-1-half_len)
        {
            dst[i] = src[i];
        }
        else
        {
            REAL sum = 0;
            for (unsigned int k = 0, src_id = i-half_len; k < len; ++k, src_id++)
            {
                sum += src[src_id] * mask[k];
            }
            dst[i] = sum;
        }
    }
}

static void fgauss(const std::vector<REAL>& src, REAL sigma, int width, std::vector<REAL>& dst)
{
    std::vector<REAL> mask = make_fgauss(sigma, width);
    normalize(mask);
    filter_odd(src, mask, dst);
}

#endif // FILTER_H
