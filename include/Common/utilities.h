#ifndef UTILITIES_H
#define UTILITIES_H

#include <stdlib.h>
#include <vector>
#include "global.h"

namespace wjl {
template <typename T>
T dist2(T x0, T y0, T x1, T y1)
{
    T dx = x0 - x1;
    T dy = y0 - y1;
    return (dx*dx + dy*dy);
}


template <typename T>
T rangle_random(const T& floor, const T& ceiling)
{
    T range = ceiling - floor;
    return floor + static_cast<T>(((double)range * (double)rand()) / (RAND_MAX + 1.0));
}

template <typename T>
int minIdVec(std::vector<T> vec)
{
    unsigned int ret = std::min_element(vec.begin(), vec.end()) - vec.begin();
    return ret;
}

template <typename T>
int maxIdVec(std::vector<T> vec)
{
    unsigned int ret = std::max_element(vec.begin(), vec.end()) - vec.begin();
    return ret;
}

template <typename T>
void pairwise_min_vec(const std::vector<T> & lhs, const std::vector<T> & rhs,
                     std::vector<T> & out)
{
    assert(lhs.size() == rhs.size());
    out.resize(lhs.size());
    for (size_t i = 0; i < lhs.size(); ++i)
        out[i] = std::min(lhs[i],rhs[i]);
}

template <typename T>
void pairwise_max_vec(const std::vector<T> & lhs, const std::vector<T> & rhs,
                     std::vector<T> & out)
{
    assert(lhs.size() == rhs.size());
    out.resize(lhs.size());
    for (size_t i = 0; i < lhs.size(); ++i)
        out[i] = std::max(lhs[i],rhs[i]);
}

}

#endif // UTILITIES_H
