#include "../Image/ImageProcessor.h"
//#include "../Common/global.h"
#include <fstream>
#include "mylsd.h"
#include "generalLSD.h"

namespace wjl
{

ImageProcessor::ImageProcessor()
{
}

ImageProcessor::ImageProcessor(std::string filename)
{
    load_image(filename);
}

bool ImageProcessor::load_image(std::string filename)
{
    _image = cv::imread(filename.c_str(), CV_LOAD_IMAGE_COLOR);

    if (! _image.data)
    {
        std::cout << "Could not load image " << filename << std::endl;
        return false;
    }

    //_height = _image.rows;
    //_width = _image.cols;

    return true;
}

///////////////*********** convert color **************///////////

void ImageProcessor::rgb2hsv(Image rgb, cv::Mat &h_map, cv::Mat &s_map, cv::Mat &v_map)
{
    int img_w = rgb.width();
    int img_h = rgb.height();

    if (rgb.channels() == 1)
    {
        std::cerr << "convert to hsv should be 3 channeels" << std::endl;
        return;
    }


    for (int r = 0; r < img_h; ++r)
        for (int c = 0; c < img_w; ++c)
        {
            Color rgb_rc = rgb.pixel(c,r);
            int h;
            double s;
            double v;
            Color::rgb2hsv(rgb_rc._r, rgb_rc._g, rgb_rc._b, h, s, v);
            h_map.at<int>(r,c) = h;
            v_map.at<double>(r,c) = s;
            v_map.at<double>(r,c) = v;
        }
}

Image ImageProcessor::hue_map(const Image& img)
{
    int img_w = img.width();
    int img_h = img.height();
    Image ret(img_w, img_h,1);
    if (img.channels() == 1)
    {
        std::cerr << "convert to hsv should be 3 channeels" << std::endl;
        return ret;
    }



    for (int r = 0; r < img_h; ++r)
        for (int c = 0; c < img_w; ++c)
        {
            Color rgb = img.pixel(c,r);
            int h;
            double s;
            double v;
            Color::rgb2hsv(rgb._r, rgb._g, rgb._b, h, s, v);
            float normal_h = (float)h*255.0/360.0;
            ret.set_pixel(c,r, (uchar)normal_h);
        }
    return ret;
}

void ImageProcessor::erode(const Image &src, Image &dst, const int erosion_type, const int erosion_size)
{
    cv::Mat element = getStructuringElement( erosion_type,
                                           cv::Size( 2*erosion_size + 1, 2*erosion_size+1 ),
                                           cv::Point( erosion_size, erosion_size ) );

      /// Apply the erosion operation
    cv::erode( src._image, dst._image, element );
}

void ImageProcessor::dilate(const Image &src, Image &dst, const int dilation_type, const int dilation_size)
{
    cv::Mat element = getStructuringElement( dilation_type,
                                          cv::Size( 2*dilation_size + 1, 2*dilation_size+1 ),
                                          cv::Point( dilation_size, dilation_size ) );
     /// Apply the dilation operation
     cv::dilate( src._image, dst._image, element );
}

std::vector<Rectangle> ImageProcessor::find_connect_rect(cv::Mat img)
{
    std::vector<Rectangle> rects;
    if (img.channels() == 3)
        cv::cvtColor(img, img, CV_BGR2GRAY);
    cv::Mat binary_img;
    cv::threshold(img, binary_img, 0.0, 1.0, cv::THRESH_BINARY);

    int label_count = 2;    //start at 2 because 0,1 are used already

    cv::Mat label_img;
    binary_img.convertTo(label_img, CV_32FC1);

    for (int r = 0; r < img.rows; r++)
        for (int c = 0; c < img.cols; c++)
        {
            if ((int)label_img.at<float>(r,c) != 1)
            {
                continue;
            }

            cv::Rect rect;
            cv::floodFill(label_img, cv::Point(c,r), cv::Scalar(label_count),
                          &rect, cv::Scalar(0), cv::Scalar(0), 4);

            rects.push_back(Rectangle(rect));
            label_count++;
            //printf("label_count(%d)\n",label_count);
        }
    std::cout << "find rects " << rects.size() << std::endl;
    return rects;
}

void ImageProcessor::flood_fill(cv::Mat & img,
                                std::vector<std::vector<cv::Point> > &blobs,
                                std::vector<Rectangle> &rects, const bool isErode)
{
    if (img.channels() == 3)
        cv::cvtColor(img, img, CV_BGR2GRAY);
    cv::Mat binary_img;
    cv::threshold(img, binary_img, 0.0, 1.0, cv::THRESH_BINARY);

    int label_count = 2;    //start at 2 because 0,1 are used already

    cv::Mat label_img;
    binary_img.convertTo(label_img, CV_32FC1);
    if (isErode)
    {
        cv::Mat element = cv::getStructuringElement( cv::MORPH_RECT,
                                                     cv::Size( 2*3 + 1, 2*3+1 ),
                                                     cv::Point( 0, 0 ) );
        cv::erode( label_img, label_img, element );

    }
    for (int r = 0; r < img.rows; r++)
        for (int c = 0; c < img.cols; c++)
        {
            if ((int)label_img.at<float>(r,c) != 1)
            {
                continue;
            }

            cv::Rect rect;
            cv::floodFill(label_img, cv::Point(c,r), cv::Scalar(label_count),
                          &rect, cv::Scalar(0), cv::Scalar(0), 4);
            label_count++;

            std::vector<cv::Point> blob;

            for(int i=rect.y; i < (rect.y+rect.height); i++)
                for(int j=rect.x; j < (rect.x+rect.width); j++)
                {
                    int chk = int(label_img.at<float>(i,j));
                    //cout << chk << endl;
                    if(chk == label_count-1) {
                        blob.push_back(cv::Point(j,i));
                    }
                }

            //place the points of a single blob in a grouping
            //a vector of vector points
            blobs.push_back(blob);
            rects.push_back(Rectangle(rect));
        }

}

std::vector<std::vector<cv::Point> > ImageProcessor::find_connect_component(cv::Mat img, const bool isErosion)
{
    std::vector<std::vector<cv::Point2i> > blobs;
    if (img.channels() == 3)
        cv::cvtColor(img, img, CV_BGR2GRAY);
    cv::Mat binary_img;
    cv::threshold(img, binary_img, 0.0, 1.0, cv::THRESH_BINARY);

    int label_count = 2;    //start at 2 because 0,1 are used already

    cv::Mat label_img;
    binary_img.convertTo(label_img, CV_32FC1);


    if (isErosion)
    {
        cv::Mat element = cv::getStructuringElement( cv::MORPH_RECT,
                                                     cv::Size( 2*3 + 1, 2*3+1 ),
                                                     cv::Point( 0, 0 ) );
        cv::erode( label_img, label_img, element );

    }

    for (int r = 0; r < img.rows; r++)
        for (int c = 0; c < img.cols; c++)
        {
            if ((int)label_img.at<float>(r,c) != 1)
            {
                continue;
            }

            cv::Rect rect;
            cv::floodFill(label_img, cv::Point(c,r), cv::Scalar(label_count),
                          &rect, cv::Scalar(0), cv::Scalar(0), 4);
            label_count++;

            std::vector<cv::Point> blob;

            for(int i=rect.y; i < (rect.y+rect.height); i++)
                for(int j=rect.x; j < (rect.x+rect.width); j++)
                {
                    int chk = int(label_img.at<float>(i,j));
                    //cout << chk << endl;
                    if(chk == label_count-1) {
                        blob.push_back(cv::Point(j,i));
                    }
                }

            //place the points of a single blob in a grouping
            //a vector of vector points
            blobs.push_back(blob);
        }

    return blobs;
}

std::vector<Rectangle> ImageProcessor::find_connect_rect(Image src_img)
{
    std::vector<Rectangle> rects;
    std::vector<std::vector<cv::Point2i> > blobs;
    int img_h = src_img.height();
    int img_w = src_img.width();

    if (src_img.empty())
        return rects;
    cv::Mat img;
    if (src_img.channels() == 1)
        img = src_img._image;
    else
        cv::cvtColor(src_img._image, img, CV_BGR2GRAY);
    cv::Mat binary_img;
    cv::threshold(img, binary_img, 128.0, 1.0, cv::THRESH_BINARY);

    int label_count = 2;    //start at 2 because 0,1 are used already

    cv::Mat label_img;
    binary_img.convertTo(label_img, CV_32FC1);
    //cv::imwrite("binary.png", binary_img*255);

    /**cv::Mat element = getStructuringElement( cv::MORPH_RECT,
                                                cv::Size( 2*1 + 1, 2*1+1 ),
                                                cv::Point( 0, 0 ) );
    /// Apply the erosion operation
    cv::erode( label_img, label_img, element );**/

    int top_margin = 4, bottom_margin = img_h-4;
    int left_margin = 4, right_margin = img_w-4;

    for (int r = 0; r < img_h; r++)
        for (int c = 0; c < img_w; c++)
        {
            //std::cout << "label img at " << r << "," << c << " = " << label_img.at<float>(r,c) <<std::endl;
            if ((int)label_img.at<float>(r,c) != 1)
            {
                continue;
            }

            cv::Rect rect;
            cv::floodFill(label_img, cv::Point(c,r), cv::Scalar(label_count),
                          &rect, cv::Scalar(0), cv::Scalar(0), 4);

            //std::vector<cv::Point2i> blob;

           /* for (int i = rect.y; i < (rect.y + rect.height); i++)
                for (int j = rect.x; j < (rect.x + rect.width); j++)
                {
                    if ((int)label_img.at<float>(i,j) != label_count)
                        continue;
                    blob.push_back(cv::Point2i(j,i));
                }
            blobs.push_back(blob);*/
            if (rect.height <= 2 || rect.width <= 2 || rect.height*rect.width < 10)
                continue;
            if (rect.x < left_margin || rect.x < left_margin && (rect.x+rect.width) < left_margin)
                           continue;
            if (rect.x > right_margin || rect.x > right_margin && (rect.x+rect.width) > right_margin)
               continue;
            if (rect.y < top_margin && (rect.y+rect.height) < top_margin)
               continue;
            if (rect.y > bottom_margin && (rect.y+rect.height) > bottom_margin)
               continue;
            rects.push_back(Rectangle(rect));
            label_count++;
            //printf("label_count(%d)\n",label_count);
        }

    /** check if insect and merge the rects **/

    Rectangle::sort_by_area(rects); //sort by area increasing

    std::vector<Rectangle> ret;

    std::vector<bool> isvaild(rects.size(), true);
    for (unsigned int i = 0; i < rects.size(); i++)
    {
        if (!isvaild.at(i))
            continue;
        for (unsigned int j = i; j < rects.size(); j++)
        {

            if ((rects.at(i) & rects.at(j)).area() > 1)
            {
                rects.at(i) |= rects.at(j);
                isvaild.at(j) = false;
            }

        }
        ret.push_back(rects.at(i));
    }


    std::cout << "find rects " << ret.size() << std::endl;

    return ret;
}

std::vector<Rectangle> ImageProcessor::find_rect_opt(Image src_img, const int thresh_size)
{
    std::vector<Rectangle> rects;
    std::vector<std::vector<cv::Point2i> > blobs;
    int img_h = src_img.height();
    int img_w = src_img.width();

    if (src_img.empty())
        return rects;
    cv::Mat img;
    if (src_img.channels() == 1)
        img = src_img._image;
    else
        cv::cvtColor(src_img._image, img, CV_BGR2GRAY);
    cv::Mat binary_img;
    cv::threshold(img, binary_img, 128.0, 1.0, cv::THRESH_BINARY);

    int label_count = 2;    //start at 2 because 0,1 are used already

    cv::Mat label_img;
    binary_img.convertTo(label_img, CV_32FC1);
    std::vector<int> labels;

    int top_margin = 4, bottom_margin = img_h-4;
    int left_margin = 4, right_margin = img_w-4;

    for (int r = 0; r < img_h; r++)
        for (int c = 0; c < img_w; c++)
        {
            //std::cout << "label img at " << r << "," << c << " = " << label_img.at<float>(r,c) <<std::endl;
            if ((int)label_img.at<float>(r,c) != 1)
            {
                continue;
            }

            cv::Rect rect;
            cv::floodFill(label_img, cv::Point(c,r), cv::Scalar(label_count),
                          &rect, cv::Scalar(0), cv::Scalar(0), 4);


            /*std::vector<cv::Point2i> blob;

            for (int i = rect.y; i < (rect.y + rect.height); i++)
                for (int j = rect.x; j < (rect.x + rect.width); j++)
                {
                    if ((int)label_img.at<float>(i,j) != label_count)
                        continue;
                    blob.push_back(cv::Point2i(j,i));
                }*/

            //blobs.push_back(blob);

            if (rect.x < left_margin || rect.x < left_margin && (rect.x+rect.width) < left_margin)
                continue;
            if (rect.x > right_margin || rect.x > right_margin && (rect.x+rect.width) > right_margin)
               continue;
            if (rect.y < top_margin && (rect.y+rect.height) < top_margin)
               continue;
            if (rect.y > bottom_margin && (rect.y+rect.height) > bottom_margin)
               continue;


            //rect too small
            if (rect.height <= 2 || rect.width <= 2 || rect.height*rect.width < 10)
                continue;

            rects.push_back(Rectangle(rect));
            labels.push_back(label_count);
            label_count++;
            //printf("label_count(%d)\n",label_count);
        }

    std::cout << "after finding, num of rectangles = " << rects.size() << std::endl;

    //check insection, merge
    Rectangle::sort_by_area(rects); //sort by area increasing

    std::vector<Rectangle> new_rects;

    std::vector<bool> isvaild(rects.size(), true);
    std::vector<int> new_labels;
    for (unsigned int i = 0; i < rects.size(); i++)
    {
        if (!isvaild.at(i))
            continue;
        for (unsigned int j = i; j < rects.size(); j++)
        {

            if ((rects.at(i) & rects.at(j)).area() > 1)
            {
                rects.at(i) |= rects.at(j);
                isvaild.at(j) = false;
            }

        }
        new_rects.push_back(rects.at(i));
    }
    std::cout << "after merging, num of rectangles = " << new_rects.size() << std::endl;

    //merge , let the labeled point in one rectangle be the same label
    label_count = 2;
    for (unsigned int i = 0; i < new_rects.size(); i++)
    {
        Rectangle rect = new_rects.at(i);
        for (int x = rect.x(); x <= rect.rx(); x++)
            for (int y = rect.y(); y <= rect.by(); y++)
                if ((int)label_img.at<float>(y,x) != 1)
                    label_img.at<float>(y,x) = label_count;
        new_labels.push_back(label_count);
        label_count++;
    }

    //optimization
    //shrink the rectangle

    std::vector<Rectangle> ret;
    for (unsigned int i = 0; i < new_rects.size(); ++i)
    {
        Rectangle rect = new_rects.at(i);
        //std::cout << "rect id = " << i << std::endl;
        //rect.print();
        //check 4 side if can shrinck
        float ocupy_threshold = 0.3;
        //left
        int x, y;
        for (x = rect.x(); x <= rect.rx(); x++)
        {
            //count left side
            int ocupy_count = 0;
            for (y = rect.y(); y <= rect.by(); y++)
            {
                //std::cout << "lable img at(y,x) = " << label_img.at<float>(y,x) << ", at new labels " << new_labels.at(i) << std::endl;
                if ((int)label_img.at<float>(y,x) == new_labels.at(i))
                    ocupy_count++;
            }
            float ocupy = (float)ocupy_count / (float)rect.height();
            if (ocupy > ocupy_threshold)
                break;
        }
        rect.setX(x);

        //right side
        for (x = rect.rx(); x >= rect.x(); x--)
        {
            int ocupy_count = 0;
            for (y = rect.y(); y < rect.by(); y++)
                if ((int)label_img.at<float>(y,x) == new_labels.at(i))
                    ocupy_count++;
            float ocupy = (float)ocupy_count / (float)rect.height();
            if (ocupy > ocupy_threshold)
                break;
        }
        rect.setW(x - rect.x() + 1);

        //top side
        for (y = rect.y(); y <= rect.by(); y++)
        {
            int ocupy_count = 0;
            for (x = rect.x(); x < rect.rx(); x++)
                if ((int)label_img.at<float>(y,x) == new_labels.at(i))
                    ocupy_count++;
            float ocupy = (float)ocupy_count / (float)rect.width();
            if (ocupy > ocupy_threshold)
                break;
        }
        rect.setY(y);

        //bottom side
        for (y = rect.by(); y >= rect.y(); y--)
        {
            int ocupy_count = 0;
            for (x = rect.x(); x <= rect.rx(); x++)
                if ((int)label_img.at<float>(y,x) == new_labels.at(i))
                    ocupy_count++;
            float ocupy = (float)ocupy_count / (float)rect.width();
            if (ocupy > ocupy_threshold)
                break;
        }
        rect.setH(y - rect.y() + 1);

        //rect.print();
        //rect too small
        if (rect.height() < sqrt(thresh_size) || rect.width() < sqrt(thresh_size) || rect.area() < thresh_size)
            continue;

        ret.push_back(rect);
    }
    std::cout << "after shrinking, num of rectangles = " << ret.size() << std::endl;
    return ret;
}

////////////////////*************** Edge ******************////////////////////////


//std::vector<LineSeg2i> ImageProcessor::find_lines(const Image &img)
//{
//    std::vector<LineSeg2i> lines;
//    Image gray = img.bgr2gray();
//    int img_w = img.width();
//    int img_h = img.height();

//    //convert lsd format
//    image_double image = new_image_double(img_w, img_h);

//    for(int i=0; i<img_h; i++)
//    {

//        for(int j=0; j<img_w; j++)
//        {
//            image->data[ j + i * img_w ] = (double)gray.pixel_gray(j,i);
//        }
//    }

//       //perform line detection
//       //set parameters
//         double scale = 0.8;       /* Scale the image by Gaussian filter to 'scale'. */
//         double sigma_scale = 0.6; /* Sigma for Gaussian filter is computed as
//                            sigma = sigma_scale/scale.                    */
//         double quant = 2.0;       /* Bound to the quantization error on the
//                            gradient norm.                                */
//         double ang_th = 22.5;     /* Gradient angle tolerance in degrees.           */
//         double eps = 0.0;         /* Detection threshold, -log10(NFA).              */
//         double density_th = 0.7;  /* Minimal density of region points in rectangle. */
//         int n_bins = 1024;        /* Number of bins in pseudo-ordering of gradient
//                            modulus.                                       */
//         double max_grad = 255.0;  /* Gradient modulus in the highest bin. The
//                            default value corresponds to the highest
//                            gradient modulus on images with gray
//                            levels in [0,255].                             */

//         //adjust parameter
//         sigma_scale = 0.7;
//         ang_th = 25.0;

//         ntuple_list line_out = LineSegmentDetection( image, scale, sigma_scale, quant, ang_th, eps, density_th, n_bins, max_grad, NULL );

//         // change line seg format into lines
//        for(int i=0;i<line_out->size;i++)
//        {
//           int p0x, p0y, p1x, p1y;
//           p0x = line_out->values[i*line_out->dim+0];
//           p0y = line_out->values[i*line_out->dim+1];
//           p1x = line_out->values[i*line_out->dim+2];
//           p1y = line_out->values[i*line_out->dim+3];

//           LineSeg2i line;

//           if (p0x<=p1x)
//           {
//               line._start.x = p0x;
//               line._start.y = p0y;
//               line._end.x = p1x;
//               line._end.y = p1y;
//           }
//           else
//           {

//               line._start.x = p1x;
//               line._start.y = p1y;
//               line._end.x = p0x;
//               line._end.y = p0y;
//           }
//           //line.length() = sqrt((p0x - p1x) * (p0x - p1x) + (p0y - p1y) * (p0y - p1y));
//           //printf("line(%d) length = %f\n", i, line._length);
//           assert(img.isInImage(line._start) && img.isInImage(line._end));
//           lines.push_back(line);
//           //for(j=0;j<line_out->dim;j++)
//           //fprintf(output,"%f ",out->values[i*out->dim+j]);
//        }
//        free_image_double_lsd(image);
//        free_ntuple_list(line_out);
//        std::cout << "--------------------ImageProcessor::find_lines total lines number = "
//                  << lines.size()
//                  << "--------------------" << std::endl;
//        return lines;
//}

std::vector<LineSeg2i> ImageProcessor::find_lines_lsd(const Image &img,
                                                      const double scale,
                                                      const double sigma_scale,
                                                      const double quant,
                                                      const double ang_th,
                                                      const double eps,
                                                      const double density_th,
                                                      const int n_bins,
                                                      const double max_grad)
{
    std::vector<LineSeg2i> lines;

    Image gray = img.bgr2gray();
    int img_w = img.width();
    int img_h = img.height();

    //convert lsd format
//    image_double image_lsd = new_image_double(img_w, img_h);

//    for(int i=0; i<img_h; i++)
//    {
//        for(int j=0; j<img_w; j++)
//        {
//            image_lsd->data[ j + i * img_w ] = (double)gray.pixel_gray(j,i);
//        }
//    }
    //image_double image_lsd = new_image_double(img_w, img_h);
    double * image_lsd = (double *) malloc( img_w * img_h * sizeof(double) );
    for(int i=0; i<img_h; i++)
    {
        for(int j=0; j<img_w; j++)
        {
            //image_lsd->data[ j + i * img_w ] = (double)gray.pixel_gray(j,i);
            image_lsd[j+i*img_w] = (double)gray.pixel_gray(j,i);
        }
    }
    int n_out;
    double log_eps = 0;
//    ntuple_list line_out = LineSegmentDetection( image_lsd, scale, sigma_scale, quant, ang_th, eps, density_th, n_bins, max_grad, NULL );
    //double* line_out = LineSegmentDetection( image_lsd, scale, sigma_scale, quant, ang_th, eps, density_th, n_bins, max_grad, NULL );
    double * line_out = LineSegmentDetection(&n_out, image_lsd, img_w, img_h, scale, sigma_scale, quant, ang_th, log_eps, density_th, n_bins, NULL, NULL, NULL);
    lines.reserve(n_out);
    //lines.reserve(line_out->size);
    // change line seg format into lines
   //for(int i=0;i<line_out->size;i++)
       for(int i=0;i<n_out;i++)
   {
      int p0x, p0y, p1x, p1y;
//      p0x = line_out->values[i*line_out->dim+0] + 0.5;
//      p0y = line_out->values[i*line_out->dim+1] + 0.5;
//      p1x = line_out->values[i*line_out->dim+2] + 0.5;
//      p1y = line_out->values[i*line_out->dim+3] + 0.5;
      p0x = line_out[i*7+0] + 0.5;
      p0y = line_out[i*7+1] + 0.5;
      p1x = line_out[i*7+2] + 0.5;
      p1y = line_out[i*7+3] + 0.5;

      LineSeg2i line;
      p0x = std::min(std::max(0,p0x), img_w-1);
      p1x = std::min(std::max(0,p1x), img_w-1);
      p0y = std::min(std::max(0,p0y), img_h-1);
      p1y = std::min(std::max(0,p1y), img_h-1);

      if (p0x<=p1x)
      {
          line._start.x = p0x;
          line._start.y = p0y;
          line._end.x = p1x;
          line._end.y = p1y;
      }
      else
      {
          line._start.x = p1x;
          line._start.y = p1y;
          line._end.x = p0x;
          line._end.y = p0y;
      }
      //line.length() = sqrt((p0x - p1x) * (p0x - p1x) + (p0y - p1y) * (p0y - p1y));
      //printf("line(%d) length = %f\n", i, line._length);
//      std::cout << "line ori (" <<  line_out->values[i*line_out->dim+0]<<","<<line_out->values[i*line_out->dim+1]
//                <<") -> (" << line_out->values[i*line_out->dim+2]<<","<<line_out->values[i*line_out->dim+3]<<")\n";
//      std::cout << "line " << line << "\n";
      assert(img.isInImage(line._start) && img.isInImage(line._end));
      lines.push_back(line);
      //for(j=0;j<line_out->dim;j++)
      //fprintf(output,"%f ",out->values[i*out->dim+j]);
   }
   //free_image_double_lsd(image_lsd);

   //free_ntuple_list(line_out);
       free( (void *) image_lsd );
       free( (void *) line_out );
   std::cout << "--------------------ImageProcessor::find_lines total lines number = "
             << lines.size()
             << "--------------------" << std::endl;
   return lines;
}

std::vector<LineSeg2i> ImageProcessor::general_lsd(const Image &img,
                                                      const double scale,
                                                      const double sigma_scale,
                                                      const double quant,
                                                      const double ang_th,
                                                      const double eps,
                                                      const double density_th,
                                                      const int n_bins,
                                                      const double max_grad)
{
    std::vector<LineSeg2i> lines;

    Image gray = img.bgr2gray();
    int img_w = img.width();
    int img_h = img.height();

    double * image_lsd = (double *) malloc( img_w * img_h * sizeof(double) );
    for(int i=0; i<img_h; i++)
    {
        for(int j=0; j<img_w; j++)
        {
            //image_lsd->data[ j + i * img_w ] = (double)gray.pixel_gray(j,i);
            image_lsd[j+i*img_w] = (double)gray.pixel_gray(j,i);
        }
    }
    int n_out;
    double log_eps = 0;
    double * line_out = GeneralLineSegmentDetection(&n_out, image_lsd, img_w, img_h, scale, sigma_scale, quant, ang_th, log_eps, density_th, n_bins, NULL, NULL, NULL);
    lines.reserve(n_out);

       for(int i=0;i<n_out;i++)
   {
      int p0x, p0y, p1x, p1y;

      p0x = line_out[i*7+0] + 0.5;
      p0y = line_out[i*7+1] + 0.5;
      p1x = line_out[i*7+2] + 0.5;
      p1y = line_out[i*7+3] + 0.5;

      LineSeg2i line;
      p0x = std::min(std::max(0,p0x), img_w-1);
      p1x = std::min(std::max(0,p1x), img_w-1);
      p0y = std::min(std::max(0,p0y), img_h-1);
      p1y = std::min(std::max(0,p1y), img_h-1);

      if (p0x<=p1x)
      {
          line._start.x = p0x;
          line._start.y = p0y;
          line._end.x = p1x;
          line._end.y = p1y;
      }
      else
      {
          line._start.x = p1x;
          line._start.y = p1y;
          line._end.x = p0x;
          line._end.y = p0y;
      }

      assert(img.isInImage(line._start) && img.isInImage(line._end));
      lines.push_back(line);

   }

       free( (double *) image_lsd );
       free( (double *) line_out );
   std::cout << "--------------------ImageProcessor::general_lsd total lines number = "
             << lines.size()
             << "--------------------" << std::endl;
   return lines;
}
//std::vector<LineSeg2i> ImageProcessor::find_lines_lsd_mask(const Image &img,
//                                                           const cv::Mat & mask,
//                                                      const double scale,
//                                                      const double sigma_scale,
//                                                      const double quant,
//                                                      const double ang_th,
//                                                      const double eps,
//                                                      const double density_th,
//                                                      const int n_bins,
//                                                      const double max_grad)
//{
//    assert(img.width() == mask.cols && img.height() == mask.rows);
//    std::vector<LineSeg2i> lines;

//    Image gray = img.bgr2gray();
//    int img_w = img.width();
//    int img_h = img.height();

//    cv::Mat mask_gray;
//    if (mask.channels() == 3)
//        cv::cvtColor(mask, mask_gray, CV_BGR2GRAY);
//    else
//        mask_gray = mask;
//    //convert lsd format
//    image_double image_lsd = new_image_double(img_w, img_h);

//    for(int i=0; i<img_h; i++)
//    {
//        for(int j=0; j<img_w; j++)
//        {
//            image_lsd->data[ j + i * img_w ] = (double)gray.pixel_gray(j,i);
//        }
//    }
//    image_double mask_lsd = new_image_double(img_w, img_h);

//    for(int i=0; i<img_h; i++)
//    {
//        for(int j=0; j<img_w; j++)
//        {
//            mask_lsd->data[ j + i * img_w ] = (double)mask_gray.at<uchar>(i,j);
//        }
//    }

//    ntuple_list line_out = LineSegmentDetectionMask( image_lsd,mask_lsd, scale, sigma_scale, quant, ang_th, eps, density_th, n_bins, max_grad, NULL );

//    lines.reserve(line_out->size);
//    // change line seg format into lines
//   for(int i=0;i<line_out->size;i++)
//   {
//      int p0x, p0y, p1x, p1y;
//      p0x = line_out->values[i*line_out->dim+0] + 0.5;
//      p0y = line_out->values[i*line_out->dim+1] + 0.5;
//      p1x = line_out->values[i*line_out->dim+2] + 0.5;
//      p1y = line_out->values[i*line_out->dim+3] + 0.5;
//      p0x = std::min(std::max(0,p0x), img_w-1);
//      p1x = std::min(std::max(0,p1x), img_w-1);
//      p0y = std::min(std::max(0,p0y), img_h-1);
//      p1y = std::min(std::max(0,p1y), img_h-1);
//      LineSeg2i line;

//      if (p0x<=p1x)
//      {
//          line._start.x = p0x;
//          line._start.y = p0y;
//          line._end.x = p1x;
//          line._end.y = p1y;
//      }
//      else
//      {

//          line._start.x = p1x;
//          line._start.y = p1y;
//          line._end.x = p0x;
//          line._end.y = p0y;
//      }
//      //line.length() = sqrt((p0x - p1x) * (p0x - p1x) + (p0y - p1y) * (p0y - p1y));
//      //printf("line(%d) length = %f\n", i, line._length);
//      lines.push_back(line);
//      //for(j=0;j<line_out->dim;j++)
//      //fprintf(output,"%f ",out->values[i*out->dim+j]);
//   }
//   free_image_double_lsd(image_lsd);
//   free_image_double_lsd(mask_lsd);

//   free_ntuple_list(line_out);
//   std::cout << "--------------------ImageProcessor::find_lines_lsd_mask total lines number = "
//             << lines.size()
//             << "\t--------------------" << std::endl;
//   return lines;
//}

Image ImageProcessor::edge_map_lsd(const Image &img)
{
    std::vector<LineSeg2i> lines = ImageProcessor::find_lines_lsd(img);

    Image gray = Image(img.width(), img.height(), 1);

    for (unsigned int i = 0; i < lines.size(); ++i)
    {
        gray.draw_line(lines.at(i), Color::white(), 1);
    }
    //gray.write("edge_map.png");
    //std::cout << "gray channels " << gray.channels() << std::endl;
    return gray;
}

void ImageProcessor::edge_ocupy_analyze(Image &img)
{
    float thresh_h_head_rate = 0.2;
    float thresh_v_head_rate = 0.1;
    float thresh_h_tail_rate = 0.9;
    float thresh_v_tail_rate = 0.5;

    Image edge_map = ImageProcessor::edge_map_lsd(img);
    std::cout << "edge map channels = " << edge_map.channels() << std::endl;
    int img_h = edge_map.height();
    int img_w = edge_map.width();

    cv::Mat binary_img;
    cv::threshold(edge_map._image, binary_img, 128.0, 1.0, cv::THRESH_BINARY);

    //******************* horizontal strip
    std::vector<std::pair<int, float> > h_ocupy;
    for (int y = 0; y < img_h; ++y)
    {
        int ocupy_count = 0;
        for (int x = 0; x < img_w; ++x)
            if (binary_img.at<int>(y,x) == 1)
                ocupy_count++;
        float ocupy = (float)ocupy_count / (float)img_w;
        std::pair<int, float> ocupy_pair = std::pair<int, float>(y, ocupy);
        h_ocupy.push_back(ocupy_pair);
    }

    std::sort(h_ocupy.begin(), h_ocupy.end(), greater_second<std::pair<int, float> >());


    //***************** vertical strip
    std::vector<std::pair<int, float> > v_ocupy;
    for (int x = 0; x < img_w; ++x)
    {
        int ocupy_count = 0;
        for (int y = 0; y < img_h; ++y)
            if (binary_img.at<int>(y,x) == 1)
                ocupy_count++;
        float ocupy = (float)ocupy_count / (float)img_h;
        std::pair<int, float> ocupy_pair = std::pair<int, float>(x, ocupy);
        v_ocupy.push_back(ocupy_pair);
    }

    std::sort(v_ocupy.begin(), v_ocupy.end(), greater_second<std::pair<int, float> >());


    //////////////output//////////////
    //horizontal strip
    /*for (unsigned int i = 0; i < h_ocupy.size(); ++i)
    {
        int y = h_ocupy.at(i).first;
        if (i < h_ocupy.size()*thresh_h_head_rate)
            img.draw_line(0,y, img_w-1, y, Color::green(), 1);
        else if (i > v_ocupy.size()*thresh_h_tail_rate)
            img.draw_line(0,y,img_w-1, y, Color::red(), 1);

    }*/

    //vertical strip
    for (unsigned int i = 0; i < v_ocupy.size(); ++i)
    {
        int x = v_ocupy.at(i).first;
        if (i < v_ocupy.size()*thresh_v_head_rate)
            img.draw_line(x,0,x, img_h-1, Color::green(), 1);
        else if (i > v_ocupy.size()*thresh_v_tail_rate)
            img.draw_line(x,0,x, img_h-1, Color::red(), 1);

    }
    img.write("edge_analize.png");

    std::ofstream fhout("h_ocupy.txt");
    for (size_t i = 0; i < h_ocupy.size(); ++i)
        fhout << h_ocupy.at(i).second << std::endl;
    fhout.close();
    std::ofstream fvout;
    fvout.open("v_ocupy.txt");
    for (size_t i = 0; i < v_ocupy.size(); ++i)
        fvout << v_ocupy.at(i).second << std::endl;
    fvout.close();

}

void ImageProcessor::edge_XY_lsd(const Image &img, Image &hori_edge, Image &vert_edge)
{
    std::vector<LineSeg2i> lines = ImageProcessor::find_lines_lsd(img);
    hori_edge = Image(img.width(), img.height(), 1);
    vert_edge = Image(img.width() ,img.height(), 1);

    Vector2<REAL> hori_dir(0,1.0);
    Vector2<REAL> vert_dir(1,0);
    REAL thresh_h = 80. / 90.0 * 0.5* MY_PI , thresh_v = 80./ 90.0 * 0.5* MY_PI;
    for (unsigned int i = 0; i < lines.size(); ++i)
    {
        if (lines.at(i).length() < 2)
            continue;
        Vector2<REAL> line_dir = lines.at(i).direction();
        REAL angle_h = hori_dir.dot(line_dir);
        angle_h = acos(fabs(angle_h));
        REAL angle_v = vert_dir.dot(line_dir);
        angle_v = acos(fabs(angle_v));
        //std::cout << "angle h = " << angle_h << ", angle_v = " << angle_v << std::endl;
        if (angle_v > thresh_v)
            vert_edge.draw_line(lines.at(i), Color::white(), 1);
        else if (angle_h > thresh_h)
            hori_edge.draw_line(lines.at(i), Color::white(), 1);
    }
}

/////////////////////////////**********Profile****************/////////////////



Signal<int> ImageProcessor::accumulate_on_X(const Image &img, const Rectangle& rect)
{
    int rect_x = rect.x();
    int rect_y = rect.y();
    int rect_rx = rect.rx();
    int rect_by = rect.by();

    Signal<int> ret;
    //rect.print();
    // horizontal profile

    for (int c = rect_x; c <= rect_rx; ++c)
    {
        int bin_c = 0;
        for (int r = rect_y; r <= rect_by; ++r)
        {
            if (img.pixel_gray(c,r) > 128)
                bin_c++;
        }
        ret.push_back(bin_c);
        //std::cout << "push h bin " << bin_c << std::endl;
    }
    return ret;
}
Signal<int> ImageProcessor::accumulate_on_X(const Image &img)
{
    int rect_x = 0;
    int rect_y = 0;
    int rect_rx = img.width()-1;
    int rect_by = img.height()-1;

    Signal<int> ret;
    //rect.print();
    // horizontal profile

    for (int c = rect_x; c <= rect_rx; ++c)
    {
        int bin_c = 0;
        for (int r = rect_y; r <= rect_by; ++r)
        {
            if (img.pixel_gray(c,r) > 128)
                bin_c++;
        }
        ret.push_back(bin_c);
        //std::cout << "push h bin " << bin_c << std::endl;
    }
    return ret;
}

Signal<int> ImageProcessor::accumulate_on_Y(const Image &img, const Rectangle& rect)
{
    int rect_x = rect.x();
    int rect_y = rect.y();
    int rect_rx = rect.rx();
    int rect_by = rect.by();

    Signal<int> ret;
    //std::cout << "vert rect by = " << rect_by << ", rect rx = " << rect_rx << std::endl;
    for (unsigned int r = rect_y; r <= rect_by; ++r)
    {
        int bin_r = 0;
        for (int c = rect_x; c <= rect_rx; ++c)
            if (img.pixel_gray(c,r) > 128)
                bin_r++;
        ret.push_back(bin_r);
        //std::cout << "vert signal r = " << r << ", binr = " << bin_r << std::endl;
    }

    return ret;
}


Signal<int> ImageProcessor::accumulate_on_Y(const Image &img)
{
    int rect_x = 0;
    int rect_y = 0;
    int rect_rx = img.width()-1;
    int rect_by = img.height()-1;

    Signal<int> ret;
    //std::cout << "vert rect by = " << rect_by << ", rect rx = " << rect_rx << std::endl;
    for (unsigned int r = rect_y; r <= rect_by; ++r)
    {
        unsigned int bin_r = 0;
        for (unsigned int c = rect_x; c <= rect_rx; ++c)
            if (img.pixel_gray(c,r) > 128)
                bin_r++;
        ret.push_back(bin_r);
        //std::cout << "vert signal r = " << r << ", binr = " << bin_r << std::endl;
    }

    return ret;
}

void ImageProcessor::draw_signal_hori(Signal<size_t> &sig, unsigned int w, unsigned int h, std::string filename)
{
    Image img(w, h);

    for (size_t bin_id = 0; bin_id < sig.size(); ++bin_id)
    {
        size_t level_full = sig.at(bin_id);
        for (size_t level = 0; level < level_full; ++level)
            img.set_pixel(bin_id, h-1-level, Color::white());
        //std::cout << "bin " << bin_id << "  = " << level_full << std::endl;
    }

    img.write(filename.c_str());
}

void ImageProcessor::draw_signal_vert(Signal<size_t> &sig, unsigned int w, unsigned int h, std::string filename)
{
    Image img(w,h);

    for (size_t y = 0; y < h; ++y)
    {
        size_t level = sig.at(y);
        for (size_t x = 0; x < level; ++x)
        {
            img.set_pixel(x,y,Color::white());
        }
    }

     img.write(filename.c_str());
}

void ImageProcessor::draw_signal_onX(Signal<int> &sig, Image &retimg, const Color color, float alpha)
{
    assert(sig.size() == retimg.width());
    Image drawImg = retimg.deep_copy();
    int w = retimg.width();
    int h = retimg.height();
    for (int bin_id = 0; bin_id < sig.size(); ++bin_id)
    {
        int level_full = sig.at(bin_id);
        for (int level = 0; level < level_full; ++level)
            drawImg.set_pixel(bin_id, h-1-level, color);
    }
    Image::alpha_blend(drawImg, alpha,retimg,1-alpha,0,retimg);
}

void ImageProcessor::draw_signal_onY(Signal<int> &sig, Image &retimg, const Color color, float alpha)
{
    assert(sig.size() == retimg.height());
    Image drawImg = retimg.deep_copy();
    int w = retimg.width();
    int h = retimg.height();

    for (int y = 0; y < h; ++y)
    {
        int level = sig.at(y);
        if (level > w)
            std::cout << "y = " << y << ", level = " << level << std::endl;
        for (int x = 0; x < level; ++x)
        {
            drawImg.set_pixel(x,y,color);
        }
    }
    Image::alpha_blend(drawImg, alpha,retimg,1-alpha,0,retimg);
}


void ImageProcessor::draw_histogram(Signal<REAL>& hist, unsigned int interval, unsigned int h, std::string filename)
{
    std::cout << "tag " << std::endl;
    REAL min_v = hist.min_value();
    REAL max_v = hist.max_value();
    REAL diff = max_v - min_v;
    size_t w = interval * hist.size();

    std::cout << "hist max = " << max_v << ", min = " << min_v << std::endl;
    Image draw_img(w,h,Color::white());
    for (size_t i = 0; i < hist.size(); ++i)
    {
        size_t x = i * interval;
        size_t rect_h = ((hist.at(i) - min_v) ) * (REAL)h;
        draw_img.draw_rect(x, h-rect_h, interval, rect_h, Color::red(), CV_FILLED);
        //std::cout << "x = " << x << ", rect_h = " << rect_h << std::endl;
    }

    draw_img.write(filename);
}

REAL ImageProcessor::NCC(const Image& block1, const Image& block2)
{

    int img_h = block1.height();
    int img_w = block1.width();
    unsigned int  num_channels = block1.channels();
    REAL ncc = 0.0;

    std::vector<REAL> mBlock1(3);
    std::vector<REAL> mBlock2(3);
    std::vector<REAL> varB1(3);
    std::vector<REAL> varB2(3);
    std::vector<REAL> numerTerm(3);
    std::vector<REAL> denomTerm(3);


    for (unsigned int k = 0; k < num_channels; ++k)
    {
        mBlock1[k] = 0;
        mBlock2[k] = 0;
        varB1[k] = 0;
        varB2[k] = 0;
        numerTerm[k] = 0;
    }

    //calculate the mean
    for (int y = 0; y < img_h; ++y)
        for (int x = 0; x < img_w; ++x)
        {
            mBlock1[0] += block1.pixel(x,y).getR();
            mBlock1[1] += block1.pixel(x,y).getG();
            mBlock1[2] += block1.pixel(x,y).getB();

            mBlock2[0] += block2.pixel(x,y).getR();
            mBlock2[1] += block2.pixel(x,y).getG();
            mBlock2[2] += block2.pixel(x,y).getB();
        }

    //make all channels block images means zero
    for (int k = 0; k < num_channels; ++k)
    {
        mBlock1[k] /= (REAL)(img_h * img_w);
        mBlock2[k] /= (REAL)(img_h * img_w);
    }

    for (int y = 0; y < img_h; ++y)
        for (int x = 0; x < img_w; ++x)
        {
            REAL b1r = (REAL)block1.pixel(x,y).getR() - mBlock1[0];
            REAL b2r = (REAL)block2.pixel(x,y).getR() - mBlock2[0];

            numerTerm[0] += fabs(b1r * b2r);
            varB1[0] += b1r * b1r;
            varB2[0] += b2r * b2r;

            REAL b1g = (REAL)block1.pixel(x,y).getG() - mBlock1[1];
            REAL b2g = (REAL)block2.pixel(x,y).getG() - mBlock2[1];
            numerTerm[1] += b1g * b2g;
            varB1[1] += fabs(b1g * b1g);
            varB2[1] += b2g * b2g;

            REAL b1b = (REAL)block1.pixel(x,y).getB() - mBlock1[2];
            REAL b2b = (REAL)block2.pixel(x,y).getB() - mBlock2[2];
            numerTerm[2] += fabs(b1b * b2b);
            varB1[2] += b1b * b1b;
            varB2[2] += b2b * b2b;
        }

    for (int k = 0; k < block1.channels(); ++k)
    {
        denomTerm[k] = sqrt(varB1[k] * varB2[k]);
        if (denomTerm[k] == 0)
            ncc += 0.;
        else
            ncc += REAL(numerTerm[k] / denomTerm[k]);
    }

    ncc /= (REAL)num_channels;
    return ncc;
}

//2 rectangle in the same image
REAL ImageProcessor::NCC(const Image &img, const Rectangle &rect1, const Rectangle &rect2)
{
    if (rect1.width() != rect2.width() || rect1.height() != rect2.height())
    {
        std::cerr << "Different size rectangles to compare NCC!" << std::endl;
        return MY_MAX_TOLLERENCE;
    }
    assert(rect1.width() == rect2.width());
    assert(rect1.height() == rect2.height());

    REAL ncc = 0.0;

    unsigned int  num_channels = img.channels();
    unsigned int block_w = rect1.width();
    unsigned int block_h = rect1.height();

    std::vector<REAL> mBlock1(3);
    std::vector<REAL> mBlock2(3);
    std::vector<REAL> varB1(3);
    std::vector<REAL> varB2(3);
    std::vector<REAL> numerTerm(3);
    std::vector<REAL> denomTerm(3);

    unsigned int lx1 = rect1.x();
    unsigned int rx1 = rect1.rx();
    unsigned int ty1 = rect1.y();
    unsigned int by1 = rect1.by();

    unsigned int lx2 = rect2.x();
    unsigned int rx2 = rect2.rx();
    unsigned int ty2 = rect2.y();
    unsigned int by2 = rect2.y();

    for (unsigned int k = 0; k < num_channels; ++k)
    {
        mBlock1[k] = 0;
        mBlock2[k] = 0;
        varB1[k] = 0;
        varB2[k] = 0;
        numerTerm[k] = 0;
    }

    //calculate the mean
    for (unsigned int y = ty1; y <= by1; ++y)
        for (unsigned int x = lx1; x <= rx1; ++x)
        {
            mBlock1[0] += img.pixel(x,y).getR();
            mBlock1[1] += img.pixel(x,y).getG();
            mBlock1[2] += img.pixel(x,y).getB();
        }

    for (unsigned int y = ty2; y <= by2; ++y)
        for (unsigned int x = lx2; x <= rx2; ++x)
        {
            mBlock2[0] += img.pixel(x,y).getR();
            mBlock2[1] += img.pixel(x,y).getG();
            mBlock2[2] += img.pixel(x,y).getB();
        }

    //make all channels block images means zero
    for (unsigned int k = 0; k < num_channels; ++k)
    {
        mBlock1[k] /= (REAL)(block_h * block_w);
        mBlock2[k] /= (REAL)(block_h * block_w);
    }

    //std::cout << "ty1 = " << ty1 << std::endl;
    for (unsigned int y = 0; y < block_h; ++y)
        for (unsigned int x = 0; x < block_w; ++x)
        {
            unsigned int y1 = ty1 + y;
            unsigned int x1 = lx1 + x;

            unsigned int y2 = ty2 + y;
            unsigned int x2 = lx2 + x;

            //std::cout << "x, y " << x1 << ", " <<y1 << ", " << x2 << ", " << y2 << std::endl;
            REAL b1r = (REAL)img.pixel(x1,y1).getR() - mBlock1[0];
            REAL b2r = (REAL)img.pixel(x2,y2).getR() - mBlock2[0];
            numerTerm[0] += fabs(b1r * b2r);
            varB1[0] += b1r * b1r;
            varB2[0] += b2r * b2r;

            REAL b1g = (REAL)img.pixel(x1,y1).getG() - mBlock1[1];
            REAL b2g = (REAL)img.pixel(x2,y2).getG() - mBlock2[1];
            numerTerm[1] += b1g * b2g;
            varB1[1] += fabs(b1g * b1g);
            varB2[1] += b2g * b2g;

            REAL b1b = (REAL)img.pixel(x1,y1).getB() - mBlock1[2];
            REAL b2b = (REAL)img.pixel(x2,y2).getB() - mBlock2[2];
            numerTerm[2] += fabs(b1b * b2b);
            varB1[2] += b1b * b1b;
            varB2[2] += b2b * b2b;
        }

    for (unsigned int k = 0; k < num_channels; ++k)
    {
        denomTerm[k] = sqrt(varB1[k] * varB2[k]);
        if (denomTerm[k] == 0)
            ncc += 0.;
        else
            ncc += REAL(numerTerm[k] / denomTerm[k]);
    }

    ncc /= (REAL)num_channels;
    return ncc;
}

Image ImageProcessor::edge_map_canny(const Image &img, const int lowThreshold, const int ratio, const int kernel_size)
{
    Image ret(img.width(), img.height());
    cv::blur(img._image, ret._image, cv::Size(3,3));

    cv::Canny(ret._image, ret._image, lowThreshold, lowThreshold*ratio, kernel_size);

    return ret;
}

void ImageProcessor::find_canny_lines(const Image &img, const int lowThreshold, const int ratio, const int kernel_size,
                                      std::vector<LineSeg2i>& lines)
{

    lines.clear();
    cv::Mat edge_img;
    cv::Canny(img._image, edge_img, lowThreshold, lowThreshold*ratio, kernel_size);
    //cv::cvtColor(edge_img, )
    std::vector<cv::Vec4i> cvlines;
    cv::HoughLinesP(edge_img, cvlines, 1, CV_PI/180.0, 80, 30, 10 );

    for (size_t i = 0; i < cvlines.size(); ++i)
    {
        lines.push_back(LineSeg2i(cvlines[i][0], cvlines[i][1], cvlines[i][2], cvlines[i][3]));
    }
}

} /// namespace wjl
