#ifndef IMAGEPROCESSOR_H
#define IMAGEPROCESSOR_H


#include <stdio.h>
#include <iostream>
#include <string>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "../Common/global.h"
#include "../Primitives/Primitives.h"
#include "../Primitives/Signal.h"
#include "./JImage.h"


namespace wjl {

class ImageProcessor
{
public:

    ImageProcessor();
    ImageProcessor(std::string filename);
    bool load_image(std::string filename);

    /* convert color space
     */
    static void rgb2hsv(Image rgb, cv::Mat &h_map, cv::Mat &s_map, cv::Mat &v_map);
    static Image hue_map(const Image& img);

    static void erode(const Image &src, Image &dst, const int erosion_type, const int erosion_size);
    static void dilate(const Image &src, Image &dst, const int dilation_type, const int dilate_size);

    /* connect components
     */
    static std::vector<Rectangle> find_connect_rect(Image img);
    static std::vector<Rectangle> find_connect_rect(cv::Mat img);
    static std::vector<Rectangle> find_rect_opt(Image img, const int thresh_size = 0);

    static std::vector<std::vector<cv::Point> > find_connect_component(cv::Mat img, const bool isErode = false);
    static void flood_fill(cv::Mat & img, std::vector<std::vector<cv::Point> > & blobs,
                      std::vector<Rectangle> & rects, const bool isErode = false);
    /* find line segments
     */
    //static std::vector<LineSeg2i> find_lines(const Image& img);
    static std::vector<LineSeg2i> find_lines_lsd(const Image& img,
                                                 const double scale = 0.8,
                                                 const double sigma_scale = 0.6,
                                                 const double quant = 2.0,
                                                 const double ang_th = 22.5,
                                                 const double eps = 0.0,
                                                 const double density_th = 0.7,
                                                 const int n_bins = 1024,
                                                 const double max_grad = 225.0
                                                 );
    static std::vector<LineSeg2i> find_lines_lsd_mask(const Image &img,
                                                      const cv::Mat & mask,
                                                      const double scale = 0.8,
                                                      const double sigma_scale = 0.6,
                                                      const double quant = 2.0,
                                                      const double ang_th = 22.5,
                                                      const double eps = 0.0,
                                                      const double density_th = 0.7,
                                                      const int n_bins = 1024,
                                                      const double max_grad = 225.0);

    static std::vector<LineSeg2i> general_lsd(const Image &img,
                                       const double scale = 0.8,
                                       const double sigma_scale = 0.6,
                                       const double quant = 2.0,
                                       const double ang_th = 22.5,
                                       const double eps = 0.0,
                                       const double density_th = 0.7,
                                       const int n_bins = 1024,
                                       const double max_grad = 225.0);
    static Image edge_map_lsd(const Image& img);
    static void edge_ocupy_analyze(Image& img);
    static void edge_XY_lsd(const Image& img, Image& hori_edge, Image& vert_edge);

    /* canny edge
     */
    static Image edge_map_canny(const Image &img, const int lowThreshold, const int ratio, const int kernel_size);
    static void find_canny_lines(const Image& img, const int lowThreshold, const int ratio, const int kernel_size,
                                 std::vector<LineSeg2i>& lines);
    /* profile
     */
    //static Profile profileH(const Rectangle rect, const Image edge_map);    //horizontal profile
    //static Profile profileV(const Rectangle rect, const Image edge_map);    //vertical profile

    //static void draw_profileH(Profile& pro, int w, int h, std::string filename);
    //static void draw_profileV(Profile& pro, int w, int h, std::string filename);

    /* accumulate to historgram, in rectangle
     */
    /* accumulate on X axis */
    static Signal<int> accumulate_on_X(const Image& img, const Rectangle& rect);
    static Signal<int> accumulate_on_X(const Image& img);

    /* accumulate on Y axis */
    static Signal<int> accumulate_on_Y(const Image& img, const Rectangle& rect);
    static Signal<int> accumulate_on_Y(const Image& img);

    static void draw_signal_hori(Signal<size_t>& sig, unsigned int w, unsigned int h, std::string filename);
    static void draw_signal_vert(Signal<size_t>& sig, unsigned int w, unsigned int h, std::string filename);

    static void draw_signal_onY(Signal<int>& sig, Image& img, const Color color, float alpha);
    static void draw_signal_onX(Signal<int>& sig, Image& img, const Color color, float alpha);

    /* draw histogram */
    static void draw_histogram(Signal<REAL>& hist, unsigned int interval, unsigned int h, std::string filename);

    /* NCC
     */
    static REAL NCC(const Image& img1, const Image& img2);
    static REAL NCC(const Image& img, const Rectangle& rect1, const Rectangle& rect2);

    //members
public:
    cv::Mat _image;
    //int _height, _width;

};

}

#endif // IMAGEPROCESSOR_H
