#include "JColor.h"

//using namespace wjl;
#include <set>
#include <stdio.h>

namespace wjl{
Color::Color()
    :_r(0), _g(0), _b(0)
{
}

//Color::Color(const uchar r, const uchar g, const uchar b)
//    : _r(r), _g(g), _b(b)
//{
//}

Color::Color(const int r, const int g, const int b)
    : _r(uchar(r)), _g(uchar(g)), _b(uchar(b))
{
}

Color::Color(double d)
{
    rainbow(d);
}

Color::Color(const cv::Scalar& scalar)
{
    if (scalar.rows * scalar.cols * scalar.channels < 3)
    {
        _r = _g = _b = 0;
    }
    else
    {
        _b = scalar[0];
        _g = scalar[1];
        _r = scalar[2];
    }
}

Color::Color(const cv::Vec3b &vec, const bool isFlip)
    : _r(vec[2]), _g(vec[1]), _b(vec[0])
{
    if (!isFlip)
    {
        _r = vec[0]; _g = vec[1]; _b = vec[2];
    }
}

Color::Color(unsigned int hex)
    : _r((hex>>16)&255), _g((hex>>8)&255), _b(hex&255)
{

}

uchar Color::to_gray() const
{
    return (uchar)(0.299*_r + 0.587*_g + 0.114*_b);
}

cv::Scalar Color::scalar(const uchar & opacity) const
{
    return cv::Scalar(_b,_g,_r, opacity);
}

Color Color::complementary() const
{
    return Color(255-_r, 255-_g, 255-_b);
}

Color Color::blue()
{
    return Color(0,0,255);
}

Color Color::red()
{
    return Color(255,0,0);
}

Color Color::green()
{
    return Color(0,255,0);
}

Color Color::white()
{
    return Color(255,255,255);
}

Color Color::black()
{
    return Color(0,0,0);
}

Color Color::yellow()
{
    return Color(255,255,0);
}

Color Color::cyan()
{
    return Color(0,255,255);
}

Color Color::magenta()
{
    return Color(255,0,255);
}

Color Color::orange()
{
    return Color(255,165,0);
}

Color Color::pink()
{
    return Color(255,128,192);
}

Color Color::orchid()
{
    return Color(128, 0,128);
}

Color Color::brown()
{
    return Color(128,64,0);
}

Color Color::dark_green()
{
    return Color(34,139,34);
}

Color Color::gold()
{
    return Color(255,215,0);
}

Color Color::gray()
{
    return Color(128,128,128);
}

Color Color::random(cv::RNG &rng)
{
    int icolor = (unsigned) rng;
    return Color( icolor&255, (icolor>>8)&255, (icolor>>16)&255 );
}

////////////////////////********access r,g,b *************////////////////////

const unsigned char &
Color::getR() const
{
    return _r;
}

const unsigned char &
Color::getG() const
{
    return _g;
}

const unsigned char &
Color::getB() const
{
    return _b;
}

double
Color::getH() const
{
    int h;
    double s, v;
    rgb2hsv(int(_r),int(_g),int(_b), h, s, v);
    return static_cast<double>(h) / 360.;
}

///////////////////////********** set r, g, b ***************/////////////////

void
Color::setR(uchar r)
{
    _r = static_cast<unsigned char>(r);
}

void
Color::setG(uchar g)
{
    _g = static_cast<unsigned char>(g);
}

void
Color::setB(uchar b)
{
    _b = static_cast<unsigned char>(b);
}

////////////////////******* Rainbow *********************/////////////////
Color& Color::rainbow_cube(double x)
{
    if(x<0)
    {
        setR(0);
        setG(0);
        setB(0);
    }

    else if(x < 0.2)
    {
        setR(255);
        setG(static_cast<uchar>(5*255*x));
        setB(0);
    }

    else if(x<0.4)
    {
        setR(static_cast<uchar>(255*(2-5*x)));
        setG(255);
        setB(0);
    }

    else if(x<0.6)
    {
        setR(0);
        setG(255);
        setB(static_cast<uchar>(255*(5*x-2)));
    }

    else if(x<0.8)
    {
        setR(0);
        setG(static_cast<uchar>(255*(4-5*x)));
        setB(255);
    }

    else if(x<=1)
    {
        setR(static_cast<uchar>(255*(5*x-4)));
        setG(0);
        setB(255);
    }

    else
    {
        setR(255);
        setG(255);
        setB(255);
    }

    return *this;
}

Color& Color::rainbow(double x)
{
    if(x<0)
    {
        setR(0);
        setG(0);
        setB(0);
    }
    else if(x>1)
    {
        setR(255);
        setG(255);
        setB(255);
    }
    else
    {
        int r,g,b;
        int h = static_cast<uchar>(x*270+0.5);
        hsv2rgb(h,240./255.,1.0,r,g,b);
        setR(r);
        setG(g);
        setB(b);
    }

    return *this;
}



////////////////////////********** Color convert RGB <-> HSV ************////

void
Color::rgb2hsv(int r, int g, int b, int& h, double& s, double& v)
{
    // r,g,b between 0 and 1
    double red = r/255.;
    double green = g/255.;
    double blue = b/255.;

    // min
    double inf = red;
    if(green<inf)
    {
        inf = green;
    }
    if(blue<inf)
    {
        inf = blue;
    }

    // max
    double sup = red;
    if(sup<green)
    {
        sup = green;
    }
    if(sup<blue)
    {
        sup = blue;
    }

    // Value
    v = sup;

    // saturation
    double c = sup-inf;
    if( sup > 0 )
    {
        s = c/sup;
    }
    else
    {
        s = 0;
    }

    // hue
    double hue = 0;
    if( red == sup ) hue = (green-blue)/c;
    else if( green == sup ) hue = 2 + (blue-red)/c;
    else hue = 4 + (red-green)/c;

    hue *= 60.;
    if( hue < 0 ) hue += 360;

    h = static_cast<int>(hue+0.5);
}

void
Color::hsv2rgb(int h, double s, double v, int& r, int& g, int& b)
{
    double red, green, blue;
    if( s == 0 )
    {
        red		= v;
        green	= v;
        blue	= v;
    }
    else
    {
        double h2 = static_cast<double>(h)/60.;
        int i = static_cast<int>( h2 );
        double f = h2 - i;
        double p = v * ( 1 - s );
        double q = v * ( 1 - s * f );
        double t = v * ( 1 - s * ( 1 - f ) );

        switch( i )
        {
            case 0:
                red		= v;
                green	= t;
                blue	= p;
                break;
            case 1:
                red		= q;
                green	= v;
                blue	= p;
                break;
            case 2:
                red		= p;
                green	= v;
                blue	= t;
                break;
            case 3:
                red		= p;
                green	= q;
                blue	= v;
                break;
            case 4:
                red		= t;
                green	= p;
                blue	= v;
                break;
            default:
                red		= v;
                green	= p;
                blue	= q;
                break;
        }
    }

    r = static_cast<int>(255*red + 0.5);
    g = static_cast<int>(255*green + 0.5);
    b = static_cast<int>(255*blue + 0.5);
}

bool Color::operator ==(const Color & c)
{
    if (&c == this)
        return true;
    if (this->_b == c._b && this->_g == c._g && this->_r == c._r)
        return true;
    else
        return false;
}

bool Color::operator !=(const Color & c)
{
//    if (this->_b == c._b && this->_g == c._g && this->_r == c._r)
//        return false;
//    else
//        return true;
    return !(this->operator ==(c));
}

std::vector<Color> Color::get_diff_colors(const int color_num)
{
    std::set<size_t> colorset;

    for (int i = 0; i < color_num; ++i)
    {
        size_t colorbits = rand() % (0xffffff) + 1;
        while(colorset.find(colorbits) != colorset.end())
            colorbits = rand() % (0xffffff) + 1;
        colorset.insert(colorbits);
    }
    std::vector<wjl::Color> colors;
    colors.reserve(color_num);
    for (std::set<size_t>::iterator itr = colorset.begin(); itr != colorset.end(); ++itr)
    {
        size_t colorbits = *itr;
        Color randcolor(colorbits % (0xff), (colorbits>>8) % (0xff), (colorbits>>16) % (0xff));
        colors.push_back(randcolor);
    }
    return colors;
}

Color Color::getNonExistColor(std::set<size_t> &colorset)
{
    size_t colorbits = rand() % (0xffffff) + 1;
    while(colorset.find(colorbits) != colorset.end())
        colorbits = rand() % (0xffffff) + 1;
    colorset.insert(colorbits);
    Color randcolor(colorbits % (0xff), (colorbits>>8) % (0xff), (colorbits>>16) % (0xff));
    return randcolor;
}

unsigned int Color::to_uint() const
{
    unsigned int bits = this->_r << 16 | this->_g << 8 |  this->_b;
    return bits;
}

cv::Vec3b Color::toCvVec() const
{
    cv::Vec3b ret(_b, _g, _r);
    //printf("%u %u %u\t", _b,_g,_r);
    //std::cout << _b << " " << _g << " " << _r << "\t";
    return ret;
}
//std::ostream & operator << (std::ostream & os, const Color & c)
//{
//    std::cout << c.red() << " " << c.green() << " " << c.blue();
//    os << c.red() << " " << c.green() << " " << c.blue();
//    return os;
//}

} /// namespace
