#ifndef JCOLOR_H
#define JCOLOR_H

#include "../Numeric/Vectors.h"
#include <iostream>
#include <set>

namespace wjl {
class Color
{
public:
    Color();
    //Color(const uchar r, const uchar g, const uchar b);
    Color(const int r, const int g, const int b);
    Color(double d);
    Color(const cv::Scalar& scalar);
    Color(const cv::Vec3b& vec, const bool isFlip = true);
    Color(unsigned int hex);

    uchar to_gray() const;
    cv::Scalar scalar(const uchar & opacity = 255) const;
    Color complementary() const;

    /* const colors
     */

    static Color black();
    static Color blue();
    static Color brown();
    static Color cyan();
    static Color dark_green();
    static Color green();
    static Color gray();
    static Color gold();
    static Color magenta();
    static Color orange();
    static Color orchid();
    static Color pink();
    static Color red();
    static Color white();
    static Color yellow();

    static Color random(cv::RNG& rng);

    static std::vector<Color> get_diff_colors(const int num);
    static Color getNonExistColor(std::set<size_t> & colorset);

    unsigned int to_uint() const;

    /* Accessor to the r,g,b component
      */
    const unsigned char & getR() const;
    const unsigned char & getG() const;
    const unsigned char & getB() const;
    double getH() const; /// h [0,1]

    /* set r,g,b component
     */
    void setR(uchar r);
    void setG(uchar g);
    void setB(uchar b);

    //! Set the color as a rainbow color, defined by a float between 0 and 1 (based on hsv, from 0 to 270°)
    //! Lower than 0 is black, greater than one is white
    Color& rainbow(double x);

    //! Set the color as a rainbow color, defined by a float between 0 and 1
    //! based on moving along the edges of the rgb cube
    //! Lower than 0 is black, greater than one is white
    Color& rainbow_cube(double x);

    /* RGB to HSV
      h [0,360]
      s [0,1]
      v [0,1]
     */
    static void rgb2hsv(int r, int g, int b, int& h, double& s, double& v);
    static void hsv2rgb(int h, double s, double v, int& r, int& g, int& b);

    /** I O
     */
    friend std::ostream & operator << (std::ostream & os, const Color & c)
    {
        os << static_cast<int>(c.getR()) << " " << static_cast<int>(c.getG()) << " " << static_cast<int>(c.getB());
        return os;
    }

    /* compare
     */
    bool operator == (const Color & c);
    bool operator != (const Color & c);


    /** format covert
     */
    cv::Vec3b toCvVec() const;

    //member
    uchar _r, _g, _b;
};
//std::ostream & operator << (std::ostream & os, const Color & c);
inline bool operator< (const Color& lhs, const Color& rhs){ return (lhs.to_uint() < rhs.to_uint()); }
inline bool operator> (const Color& lhs, const Color& rhs){return rhs < lhs;}
inline bool operator<=(const Color& lhs, const Color& rhs){return !(lhs > rhs);}
inline bool operator>=(const Color& lhs, const Color& rhs){return !(lhs < rhs);}
}

#endif // JCOLOR_H
