#include "./JImage.h"

//**************************** Image ********************//
//*******************************************************//
//using namespace wjl;
namespace wjl {
Image::Image()
{
}

Image::Image(unsigned int w, unsigned int h, bool isgray /*= false*/)
    : _width(w), _height(h)
{
    if (isgray)
        _image = cv::Mat::zeros(h,w,CV_8UC1);
    else
        _image = cv::Mat::zeros(h,w,CV_8UC3);

}

Image::Image(const cv::Mat mat)
    : _image(mat)
{
    _width = (unsigned int)(mat.cols);
    _height = (unsigned int)(mat.rows);
}

Image::Image(const Image& img)
    : _image(img._image), _width(img.width()), _height(img.height())
{

}

Image::Image(std::string filename)
{
    read(filename);
    //std::cout << "image construct by std" << std::endl;
}

Image::Image(unsigned int w, unsigned int h, Color color, bool isgray /* = false */)
    : _width(w), _height(h)
{
    if (isgray)
    {
        this->_image = cv::Mat(h, w, CV_8UC1);
        uchar gray = color.to_gray();
        for (unsigned int i = 0; i < h; ++i)
            for (unsigned int j = 0; j < w; ++j)
                this->_image.at<uchar>(i,h) = gray;
    }
    else
    {
        this->_image = cv::Mat(h,w, CV_8UC3);

        for (unsigned int i = 0; i < h; ++i)
            for (unsigned int j = 0; j < w; ++j)
            {
                this->_image.at<cv::Vec3b>(i,j)[0] = color.getB();
                this->_image.at<cv::Vec3b>(i,j)[1] = color.getG();
                this->_image.at<cv::Vec3b>(i,j)[2] = color.getR();
            }
    }

}

Image::~Image()
{
    if (!_image.empty())
        _image.release();
}

Image Image::ones(unsigned int w, unsigned int h, bool isgray /*=false*/)
{
    if (isgray)
    {
        Image ret;
        ret._image = cv::Mat::ones(h, w, CV_8UC1);
        ret._image *= 255;
        ret._width = w;
        ret._height = h;
        return ret;
    }
    else
    {
        Image ret;
        ret._image = cv::Mat::ones(h,w,CV_8UC3);
        ret._image *= 255;

        ret._width = w;
        ret._height = h;
        return ret;
    }
}

Image Image::zeros(unsigned int w, unsigned int h, bool isgray /*=false*/)
{
    if (isgray)
    {
        Image ret;
        ret._image = cv::Mat::zeros(h, w, CV_8UC1);
        ret._width = w;
        ret._height = h;
        return ret;
    }
    else
    {
        Image ret;
        ret._image = cv::Mat::zeros(h, w, CV_8UC3);
        ret._width = w;
        ret._height = h;
        return ret;
    }
}

////////////////****************** read & write *****************////
bool Image::read(const std::string filename)
{
    _image = cv::imread(filename.c_str());
    //std::cout << "in image " << filename << std::endl;
    if (_image.empty())
    {
        std::cerr << "Load error: cannot find image " << filename << std::endl;
        return false;
    }
    _width =(unsigned int) _image.cols;
    _height =(unsigned int) _image.rows;
    return true;
}

void Image::write(const std::string filename)
{
    cv::imwrite(filename.c_str(), _image);
}

////////******************* convert to IplImage* ********///////////
IplImage * Image::toIplImage()
{
    //not work, copy data
    //IplImage* ret = cvCreateImage(cvSize(this->width(), this->height()), _image.depth(), _image.channels());
    //ret->imageData = (char*)_image.data;
    IplImage* temp = new IplImage(_image);

    return temp;
}

/////////********************* convert color **********/////////////
Image Image::bgr2gray() const
{
    cv::Mat gray;

    if (_image.channels() == 1)
        gray = _image;
    else
        cv::cvtColor(_image, gray, CV_BGR2GRAY);
    Image ret(gray);
    return ret;
}

///////************************Image info ***************//////

unsigned int Image::width() const
{
    return (unsigned int)_image.cols;
}

unsigned int Image::height() const
{
    return (unsigned int)_image.rows;
}

unsigned int Image::channels() const
{
    return (unsigned int)_image.channels();
}

bool Image::empty() const
{
    return _image.empty();
}

Rectangle Image::bound_rect() const
{
    Rectangle ret(0,0,this->width(), this->height());
    return ret;
}

//////********************* operator ********************///////
Image& Image::operator =(const Image & img)
{
    this->_image = img._image;
    this->_height = img.height();
    this->_width = img.width();
    return (*this);
//    Image ret(img);
//    return ret;
}

Image& Image::operator +=(const Image & img)
{
    if (_image.channels() == img.channels())
        this->_image += img._image;
    else
        std::cerr << "operator += using different channels" << std::endl;
    return (*this);
}

Image& Image::operator -=(const Image & img)
{

    if (_image.channels() == img.channels())
        this->_image -= img._image;
    else
        std::cerr << "operator += using different channels" << std::endl;
    return (*this);
}

Image Image::operator +(const Image& img)
{
    Image ret(img.width(), img.height());
    ret._image = this->_image + img._image;
    return ret;
}

Image Image::operator -(const Image& img)
{
    Image ret(img.width(), img.height());
    ret._image = this->_image - img._image;
    return ret;
}

Image Image::deep_copy() const
{

    Image ret(this->_width, this->_height);
    ret._image = this->_image.clone();
    ret._width = this->_width;
    ret._height = this->_height;
    return ret;
}

//***************** Pixel operation ***********************//

Color Image::pixel(const unsigned int x, const unsigned int y) const
{
     Color color;
     assert(x < this->_width && x >= 0);
     assert(y < this->_height && y >= 0);

    if (this->channels() == 3)
    {

        color._b = _image.at<cv::Vec3b>(y,x)[0];
        color._g = _image.at<cv::Vec3b>(y,x)[1];
        color._r = _image.at<cv::Vec3b>(y,x)[2];
        return color;
    }
    else
    {
        color._b = color._r = color._b = _image.at<uchar>(y,x);
        //std::cerr << "channel is not 3 , please use pixel_gray() instead" << std::endl;
    }
    return color;
}

Color Image::pixel(const cv::Point &pt) const
{
    return pixel(pt.x, pt.y);
}

uchar Image::pixel_gray(const unsigned int x, const unsigned int y) const
{
    if (this->channels() == 1)
        return _image.at<uchar>(y,x);
    else
    {
        float b = float(_image.at<cv::Vec3b>(y,x)[0]);
        float g = float(_image.at<cv::Vec3b>(y,x)[1]);
        float r = float(_image.at<cv::Vec3b>(y,x)[2]);
        return (uchar)(0.299*r + 0.587*g + 0.114*b);
    }
}

uchar Image::pixel_gray(const cv::Point &pt) const
{
    return pixel_gray(pt.x, pt.y);
}

void Image::set_pixel(const unsigned int x, const unsigned int y, const Color rgb)
{
    if (x < 0 || x >= _width || y < 0 || y >= _height)
        std::cerr << "set pixel out of range, size = (" << _width << "," << _height << "), pos(" << x << ", " << y << ")"<< std::endl;
    else if (channels() == 3)
        _image.at<cv::Vec3b>(y,x) = cv::Vec3b(rgb._b, rgb._g, rgb._r);
    else
        _image.at<uchar>(y,x) = rgb.to_gray();
}

void Image::set_pixel(const unsigned int x, const unsigned int y, uchar intensity)
{
    if (x < 0 || x >= _width || y < 0 || y >= _height)
        std::cerr << "set pixel out of range, size = (" << _width << "," << _height << ")"<< std::endl;
    else if (channels() == 3)
        _image.at<cv::Vec3b>(y,x) = cv::Vec3b(intensity, intensity, intensity);
    else
        _image.at<uchar>(y,x) = intensity;
}

void Image::set_pixel(const cv::Point &pt, const Color rgb)
{
    set_pixel(pt.x, pt.y, rgb);
}

void Image::set_pixel(const cv::Point &pt, const uchar intensity)
{
    set_pixel(pt.x, pt.y, intensity);
}

////////////////////////// ROI /////////////////////////////////////

Image Image::roi(const Rectangle& rect) const
{
    if (rect.x() < 0 || rect.x() > this->_width || rect.y() < 0 || rect.y() > this->_height || rect.rx() > this->_width || rect.by() > this->_height)
    {
        std::cerr << "roi out of image range" << std::endl;
        rect.print();
        //std::cout << rect << std::endl;
    }
    Image ret(rect.width(), rect.height());
    ret._image = cv::Mat(this->_image, rect._cvrect);
    return ret;
}
////////////////////////**** statistics *******************////////////////
cv::Scalar Image::sum() const
{
    cv::Scalar sum = cv::sum(this->_image);
    return sum;
}
cv::Scalar Image::mean() const
{
    cv::Scalar ret = cv::mean(this->_image);
    return ret;
}

//////////////////*************** signal ************************//////////////
//Signal<double> Image::to_signal()
//{
//    Signal<double> sig;

//    int img_w = this->width();
//    int img_h = this->height();

//    if (this->channels() == 1)
//    {
//        for (int y = 0; y < img_h; ++y)
//            for (int x = 0; x < img_w; ++x)
//                sig.push_back((double)this->pixel_gray(x,y));
//    }

//    if (this->channels() == 3)
//    {
//        for (int y = 0; y < img_h; ++y)
//            for (int x = 0; x < img_w; ++x)
//            {
//                sig.push_back((double)this->pixel(x,y).getR());
//                sig.push_back((double)this->pixel(x,y).getG());
//                sig.push_back((double)this->pixel(x,y).getB());
//            }
//    }

//    return sig;
//}


//////////////***************** draw function ******************//////////////

void Image::draw_rect(const Rectangle &rect, const Color &color, const int thickness)
{
    cv::rectangle(_image, rect._cvrect, color.scalar(), thickness);
}

void Image::draw_rect(const int x, const int y, const int w, const int h, const Color & color, const int thickness)
{
    cv::rectangle(this->_image, cv::Rect(x,y,w,h), color.scalar(), thickness);
}

void Image::draw_rect(const cv::Rect & rect, const Color & color, const int thickness)
{
    cv::rectangle(_image, rect, color.scalar(), thickness);
}

cv::Mat& Image::cv_mat()
{
    return _image;
}

void Image::draw_line(const LineSeg2i & line, const Color & rgb, const int thickness)
{
    if (channels() == 3)
        cv::line(this->_image, line._start, line._end, rgb.scalar(), thickness);
    else
        cv::line(this->_image, line._start, line._end, rgb.scalar(), thickness);

}

void Image::draw_line(const Point2i & p0, const Point2i & p1, const Color & rgb, const int thickness)
{
    cv::line(this->_image, p0, p1, rgb.scalar(), thickness);
}

void Image::draw_line(const int x0, const int y0, const int x1, const int y1, const Color & rgb, const int thickness)
{
    cv::line(_image,cv::Point(x0,y0), cv::Point(x1,y1), rgb.scalar(), thickness);
}

void Image::draw_text(const int & x, const int & y, const std::string & str, const int & font, const double & scale, const Color & color, const int & thickness)
{
    cv::putText(this->_image, str, Point2i(x,y), font, scale, color.scalar(), thickness);
}

void Image::draw_circle(const Point2i & center, const int r, const Color & color, const int thickness)
{
    cv::circle(this->_image, center, r, color.scalar(), thickness);
}

void Image::draw_circle(const int & x, const int & y, const int r, const Color & color, const int thickness)
{
    cv::circle(this->_image, Point2i(x,y), r, color.scalar(), thickness);
}

void Image::draw_horizontal_line(const unsigned int y, const Color & color, const int thickness)
{
    cv::line(this->_image, cv::Point(0,y), cv::Point(this->_width-1,y), color.scalar(), thickness);
}

void Image::draw_verttical_line(const unsigned int x, const Color & color, const int thickness)
{
    cv::line(this->_image, cv::Point(x,0), cv::Point(x,this->_height-1), color.scalar(), thickness);
}

void Image::draw_lines(const std::vector<LineSeg2i> &lines, const Color & rgb, const int thickness)
{
    for (size_t i = 0; i < lines.size(); ++i)
        this->draw_line(lines[i], rgb, thickness);
}

void Image::draw_lines(const std::vector<LineSeg2i> &lines, const std::vector<Color> & colors, const int thickness)
{
    assert(colors.size() >= lines.size());
    for (size_t i = 0; i < lines.size(); ++i)
        this->draw_line(lines[i], colors[i], thickness);
}
void Image::draw_lines(const std::vector<LineSeg2i> &lines, const int thickness)
{
    std::vector<Color> colors = Color::get_diff_colors(lines.size());
    draw_lines(lines, colors, thickness);
}

//////////////////////////////////// blend ***************/////////////
Image Image::alpha_blend(const Image &img1, REAL alpha, const Image &img2, REAL beta, REAL gama)
{
    Image ret(img1.width(), img1.height());
    cv::addWeighted(img1._image, alpha, img2._image, beta, gama, ret._image);
    return ret;
}

void Image::alpha_blend(const Image &img1, REAL alpha, const Image &img2, REAL beta, REAL gama, Image &output)
{
    cv::addWeighted(img1._image, alpha, img2._image, beta, gama, output._image);
}

Image Image::copy_to(const Image &mask)
{
    if (this->width() != mask.width() || this->height() != mask.height())
        std::cerr << "src image and mask have different size!" << std::endl;
    Image ret(this->width(), this->height());
    this->_image.copyTo(ret._image, mask._image);
    return ret;
}

void Image::copy_to(const Image &src, const cv::Mat &mask, Image &dst)
{
    dst = Image(mask.cols, mask.rows);
    src._image.copyTo(dst._image, mask);
}

void Image::set_border(const unsigned int left, const unsigned int right, const unsigned int top, const unsigned int bottom, const Color color)
{
    if (this->channels() == 3)
    {
        for (unsigned int y = 0; y < this->height(); ++y)
            for (unsigned int x = 0; x < this->width(); ++x)
            {
                if (y < top || y > this->height() - bottom || x < left || x > this->width() - right)
                    this->set_pixel(x,y, color);
            }
    }
    else
    {
        uchar intensity = color.to_gray();
        for (unsigned int y = 0; y < this->height(); ++y)
            for (unsigned int x = 0; x < this->width(); ++x)
            {
                if (y < top || y > this->height() - bottom || x < left || x > this->width() - right)
                    this->set_pixel(x,y, intensity);
            }
    }
}

Image Image::resizeTo(const int wid, const int hei) const
{
    Image ret(wid, hei);
    cv::resize(_image, ret._image, cv::Size(wid, hei));
    return ret;
}

Image Image::resizeTo(const double factor) const
{
    size_t w = _image.cols * factor;
    size_t h = _image.rows * factor;
    Image ret(w, h);
    cv::resize(_image, ret._image, cv::Size(w,h));
    return ret;
}

} /// namespace
