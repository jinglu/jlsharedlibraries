#ifndef IMAGE_H
#define IMAGE_H

#include "../Primitives/Primitives.h"
#include "../Primitives/Signal.h"
#include "JColor.h"

namespace wjl {
class Image
{
public:
    Image();
    Image(unsigned int w, unsigned int h, bool isgray = false);
    Image(const Image& img);
    Image(const cv::Mat mat);
    Image(std::string filename);
    Image(unsigned int w, unsigned int h, Color color, bool isgray = false);
    ~Image();

    static Image ones(unsigned int w, unsigned int h, bool isgray = false);
    static Image zeros(unsigned int w, unsigned int h, bool isgray = false);

    /* read & write
     */
    bool read(const std::string filename);
    void write(const std::string filename);

    /* Image info
     */
    unsigned int width() const;
    unsigned int height() const;
    unsigned int channels() const;
    bool empty() const;
    cv::Mat& cv_mat();

    /* bound box */
    Rectangle bound_rect() const;

    /* color converting
     */

    Image bgr2gray() const;

    /* data type convert
     */
    IplImage* toIplImage();

    /* operators
     */

    Image & operator = (const Image & img);
    Image & operator += (const Image & img);
    Image & operator -= (const Image & img);

    Image operator + (const Image & img);
    Image operator - (const Image & img);

    /* copy
     */
    Image deep_copy() const;

    /* this copy to ret, only if the value of mask is non-zero
     */
    Image copy_to(const Image& mask);
    void copy_to(const Image& src, const cv::Mat & mask, Image & dst);

    /* pixel operation
     */

    Color pixel(const unsigned int x, const unsigned int y) const;
    uchar pixel_gray(const unsigned int x, const unsigned int y) const;
    Color pixel(const cv::Point & pt) const;
    uchar pixel_gray(const cv::Point & pt) const;

    /* set pixel
     */
    void set_pixel(const unsigned int x, const unsigned int y, const Color rgb);
    void set_pixel(const unsigned int x, const unsigned int y, const uchar intensity);
    void set_pixel(const cv::Point & pt, const Color rgb);
    void set_pixel(const cv::Point & pt, const uchar intensity);

    /* set region of interest
     */
    Image roi(const Rectangle& ret) const;

    /* set border
     */
    void set_border(const unsigned int left, const unsigned int right, const unsigned int top, const unsigned int bottom, const Color color);

    /* resize
     */
    Image resizeTo(const int wid, const int hei) const;
    Image resizeTo(const double factor) const;

    /* statistics
     */
    cv::Scalar sum() const;
    cv::Scalar mean() const;

    /* convert to signal
     */
    //Signal<double> to_signal();

    /* draw function
     */
    void draw_rect(const Rectangle& rect, const Color & color, const int thickness = CV_FILLED);
    void draw_rect(const int x, const int y, const int w, const int h, const Color & color, const int thickness = CV_FILLED);
    void draw_rect(const cv::Rect & rect, const Color & color, const int thickness = CV_FILLED);

    void draw_line(const LineSeg2i & line, const Color & rgb, const int thickness);
    void draw_line(const Point2i & p0, const Point2i & p1, const Color & rgb, const int thickness);
    void draw_line(const int x0, const int y0, const int x1, const int y1, const Color & rgb, const int thickness);
    void draw_text(const int & x, const int & y, const std::string & str, const int & font, const double & scale, const Color & color, const int & thickness);
    void draw_circle(const Point2i & center,const int r, const Color & color, const int thickness = CV_FILLED);
    void draw_circle(const int & x, const int & y, const int r, const Color & color, const int thickness = CV_FILLED);

    void draw_verttical_line(const unsigned int x, const Color & color, const int thickness);
    void draw_horizontal_line(const unsigned int y, const Color & color, const int thickness);

    /* draw a bunch of
     */
    void draw_lines(const std::vector<LineSeg2i>& lines, const Color & rgb, const int thickness);
    void draw_lines(const std::vector<LineSeg2i>& lines, const std::vector<Color> & colors, const int thickness);
    void draw_lines(const std::vector<LineSeg2i> & lines, const int thickness);
    /* alpha blending
     */
    static Image alpha_blend(const Image& img1, REAL alpha, const Image& img2, REAL beta, REAL gama);
    static void alpha_blend(const Image &img1, REAL alpha, const Image &img2, REAL beta, REAL gama, Image& output);
    //************************member****************//

    inline bool isInImage(const double x, const double y) const
    { return (x >=0 && y >= 0 && x < _image.cols && y < _image.rows);}
    inline bool isInImage(const cv::Point & p) const
    { return (p.x >=0 && p.y >= 0 && p.x < _image.cols && p.y < _image.rows);}

public:
    cv::Mat _image;
    unsigned int _width, _height;
};

}

#endif // IMAGE_H
