/*----------------------------------------------------------------------------

  LSD - Line Segment Detector on digital images

  This code is part of the following publication and was subject
  to peer review:

    "LSD: a Line Segment Detector" by Rafael Grompone von Gioi,
    Jeremie Jakubowicz, Jean-Michel Morel, and Gregory Randall,
    Image Processing On Line, 2012. DOI:10.5201/ipol.2012.gjmr-lsd
    http://dx.doi.org/10.5201/ipol.2012.gjmr-lsd

  Copyright (c) 2007-2011 rafael grompone von gioi <grompone@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.

  ----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/** @file lsd.h
    LSD module header
    @author rafael grompone von gioi <grompone@gmail.com>
 */
/*----------------------------------------------------------------------------*/
#ifndef LSD_HEADER
#define LSD_HEADER

namespace wjl
{

struct coorlist
{
  int x,y;
  struct coorlist * next;
};

struct point {int x,y;};

extern void error(char * msg);

#define RELATIVE_ERROR_FACTOR 100.0
static int double_equal(double a, double b);

static double dist(double x1, double y1, double x2, double y2);

typedef struct ntuple_list_s
{
  unsigned int size;
  unsigned int max_size;
  unsigned int dim;
  double * values;
} * ntuple_list;


void free_ntuple_list(ntuple_list in);
ntuple_list new_ntuple_list(unsigned int dim);
static void enlarge_ntuple_list(ntuple_list n_tuple);
static void add_5tuple( ntuple_list out, double v1, double v2,
                        double v3, double v4, double v5 );

typedef struct image_char_s
{
  unsigned char * data;
  unsigned int xsize,ysize;
} * image_char;

void free_image_char(image_char i);
image_char new_image_char(unsigned int xsize, unsigned int ysize);
image_char new_image_char_ini( unsigned int xsize, unsigned int ysize,
                               unsigned char fill_value );

typedef struct image_int_s
{
  int * data;
  unsigned int xsize,ysize;
} * image_int;

void free_image_int(image_int i);
image_int new_image_int(unsigned int xsize, unsigned int ysize);
image_int new_image_int_ini( unsigned int xsize, unsigned int ysize,
                             int fill_value );

typedef struct image_double_s
{
  double * data;
  unsigned int xsize,ysize;
} * image_double;

void free_image_double_lsd(image_double i);
image_double new_image_double(unsigned int xsize, unsigned int ysize);
image_double new_image_double_ini( unsigned int xsize, unsigned int ysize,
                                   double fill_value );
image_double new_image_double_ptr( unsigned int xsize,
                                          unsigned int ysize, double * data );
void free_image_double(image_double i);


static void gaussian_kernel(ntuple_list kernel, double sigma, double mean);
//static image_double gaussian_sampler( image_double in, double scale,
//                                      double sigma_scale );
//static image_double ll_angle( image_double in, double threshold,
//                              struct coorlist ** list_p, void ** mem_p,
//                              image_double * modgrad, unsigned int n_bins,
//                              double max_grad );
static int isaligned( int x, int y, image_double angles, double theta,
                      double prec );
static double angle_diff(double a, double b);

static double angle_diff_signed(double a, double b);

static double log_gamma_windschitl(double x);



static double log_gamma_lanczos(double x);

#define log_gamma(x) ((x)>15.0?log_gamma_windschitl(x):log_gamma_lanczos(x))


//#endif // MYLSD_H
#define TABSIZE 100000
static double nfa(int n, int k, double p, double logNT);

static void rect_copy(struct rect * in, struct rect * out);


static double inter_low(double x, double x1, double y1, double x2, double y2);

static double inter_hi(double x, double x1, double y1, double x2, double y2);

struct rect /* line segment with width */
{
  double x1,y1,x2,y2;  /* first and second point of the line segment */
  double width;        /* rectangle width */
  double x,y;          /* center of the rectangle */
  double theta;        /* angle */
  double dx,dy;        /* vector with the line segment angle */
  double prec;         /* tolerance angle */
  double p;            /* probability of a point with angle within 'prec' */
};

typedef struct
{
  double vx[4];
  double vy[4];
  double ys,ye;
  int x,y;
} rect_iter;

//static void ri_del(rect_iter * iter);

//static int ri_end(rect_iter * i);

//static void ri_inc(rect_iter * i);

//static rect_iter * ri_ini(struct rect * r);

//static double rect_nfa(struct rect * rec, image_double angles, double logNT);

//static double get_theta( struct point * reg, int reg_size, double x, double y,
//                         image_double modgrad, double reg_angle, double prec );

//static void region2rect( struct point * reg, int reg_size,
//                         image_double modgrad, double reg_angle,
//                         double prec, double p, struct rect * rec );

//static void region_grow( int x, int y, image_double angles, struct point * reg,
//                         int * reg_size, double * reg_angle, image_char used,
//                         double prec );
//static double rect_improve( struct rect * rec, image_double angles,
//                            double logNT, double eps );

//static int reduce_region_radius( struct point * reg, int * reg_size,
//                                 image_double modgrad, double reg_angle,
//                                 double prec, double p, struct rect * rec,
//                                 image_char used, image_double angles,
//                                 double density_th, double logNT, double eps );

image_double gaussian_sampler( image_double in, double scale,
                                      double sigma_scale );

void region_grow( int x, int y, image_double angles, struct point * reg,
                         int * reg_size, double * reg_angle, image_char used,
                         double prec );
void region2rect( struct point * reg, int reg_size,
                         image_double modgrad, double reg_angle,
                         double prec, double p, struct rect * rec );
double rect_improve( struct rect * rec, image_double angles,
                            double logNT, double log_eps );

int refine( struct point * reg, int * reg_size, image_double modgrad,
                   double reg_angle, double prec, double p, struct rect * rec,
                   image_char used, image_double angles, double density_th );
void add_7tuple( ntuple_list out, double v1, double v2, double v3,
                        double v4, double v5, double v6, double v7 );

/*----------------------------------------------------------------------------*/
/** LSD Full Interface

    @param n_out       Pointer to an int where LSD will store the number of
                       line segments detected.

    @param img         Pointer to input image data. It must be an array of
                       doubles of size X x Y, and the pixel at coordinates
                       (x,y) is obtained by img[x+y*X].

    @param X           X size of the image: the number of columns.

    @param Y           Y size of the image: the number of rows.

    @param scale       When different from 1.0, LSD will scale the input image
                       by 'scale' factor by Gaussian filtering, before detecting
                       line segments.
                       Example: if scale=0.8, the input image will be subsampled
                       to 80% of its size, before the line segment detector
                       is applied.
                       Suggested value: 0.8

    @param sigma_scale When scale!=1.0, the sigma of the Gaussian filter is:
                       sigma = sigma_scale / scale,   if scale <  1.0
                       sigma = sigma_scale,           if scale >= 1.0
                       Suggested value: 0.6

    @param quant       Bound to the quantization error on the gradient norm.
                       Example: if gray levels are quantized to integer steps,
                       the gradient (computed by finite differences) error
                       due to quantization will be bounded by 2.0, as the
                       worst case is when the error are 1 and -1, that
                       gives an error of 2.0.
                       Suggested value: 2.0

    @param ang_th      Gradient angle tolerance in the region growing
                       algorithm, in degrees.
                       Suggested value: 22.5

    @param log_eps     Detection threshold, accept if -log10(NFA) > log_eps.
                       The larger the value, the more strict the detector is,
                       and will result in less detections.
                       (Note that the 'minus sign' makes that this
                       behavior is opposite to the one of NFA.)
                       The value -log10(NFA) is equivalent but more
                       intuitive than NFA:
                       - -1.0 gives an average of 10 false detections on noise
                       -  0.0 gives an average of 1 false detections on noise
                       -  1.0 gives an average of 0.1 false detections on nose
                       -  2.0 gives an average of 0.01 false detections on noise
                       .
                       Suggested value: 0.0

    @param density_th  Minimal proportion of 'supporting' points in a rectangle.
                       Suggested value: 0.7

    @param n_bins      Number of bins used in the pseudo-ordering of gradient
                       modulus.
                       Suggested value: 1024

    @param reg_img     Optional output: if desired, LSD will return an
                       int image where each pixel indicates the line segment
                       to which it belongs. Unused pixels have the value '0',
                       while the used ones have the number of the line segment,
                       numbered 1,2,3,..., in the same order as in the
                       output list. If desired, a non NULL int** pointer must
                       be assigned, and LSD will make that the pointer point
                       to an int array of size reg_x x reg_y, where the pixel
                       value at (x,y) is obtained with (*reg_img)[x+y*reg_x].
                       Note that the resulting image has the size of the image
                       used for the processing, that is, the size of the input
                       image scaled by the given factor 'scale'. If scale!=1
                       this size differs from XxY and that is the reason why
                       its value is given by reg_x and reg_y.
                       Suggested value: NULL

    @param reg_x       Pointer to an int where LSD will put the X size
                       'reg_img' image, when asked for.
                       Suggested value: NULL

    @param reg_y       Pointer to an int where LSD will put the Y size
                       'reg_img' image, when asked for.
                       Suggested value: NULL

    @return            A double array of size 7 x n_out, containing the list
                       of line segments detected. The array contains first
                       7 values of line segment number 1, then the 7 values
                       of line segment number 2, and so on, and it finish
                       by the 7 values of line segment number n_out.
                       The seven values are:
                       - x1,y1,x2,y2,width,p,-log10(NFA)
                       .
                       for a line segment from coordinates (x1,y1) to (x2,y2),
                       a width 'width', an angle precision of p in (0,1) given
                       by angle_tolerance/180 degree, and NFA value 'NFA'.
                       If 'out' is the returned pointer, the 7 values of
                       line segment number 'n+1' are obtained with
                       'out[7*n+0]' to 'out[7*n+6]'.
 */
double * LineSegmentDetection( int * n_out,
                               double * img, int X, int Y,
                               double scale, double sigma_scale, double quant,
                               double ang_th, double log_eps, double density_th,
                               int n_bins,
                               int ** reg_img, int * reg_x, int * reg_y );

/*----------------------------------------------------------------------------*/
/** LSD Simple Interface with Scale and Region output.

    @param n_out       Pointer to an int where LSD will store the number of
                       line segments detected.

    @param img         Pointer to input image data. It must be an array of
                       doubles of size X x Y, and the pixel at coordinates
                       (x,y) is obtained by img[x+y*X].

    @param X           X size of the image: the number of columns.

    @param Y           Y size of the image: the number of rows.

    @param scale       When different from 1.0, LSD will scale the input image
                       by 'scale' factor by Gaussian filtering, before detecting
                       line segments.
                       Example: if scale=0.8, the input image will be subsampled
                       to 80% of its size, before the line segment detector
                       is applied.
                       Suggested value: 0.8

    @param reg_img     Optional output: if desired, LSD will return an
                       int image where each pixel indicates the line segment
                       to which it belongs. Unused pixels have the value '0',
                       while the used ones have the number of the line segment,
                       numbered 1,2,3,..., in the same order as in the
                       output list. If desired, a non NULL int** pointer must
                       be assigned, and LSD will make that the pointer point
                       to an int array of size reg_x x reg_y, where the pixel
                       value at (x,y) is obtained with (*reg_img)[x+y*reg_x].
                       Note that the resulting image has the size of the image
                       used for the processing, that is, the size of the input
                       image scaled by the given factor 'scale'. If scale!=1
                       this size differs from XxY and that is the reason why
                       its value is given by reg_x and reg_y.
                       Suggested value: NULL

    @param reg_x       Pointer to an int where LSD will put the X size
                       'reg_img' image, when asked for.
                       Suggested value: NULL

    @param reg_y       Pointer to an int where LSD will put the Y size
                       'reg_img' image, when asked for.
                       Suggested value: NULL

    @return            A double array of size 7 x n_out, containing the list
                       of line segments detected. The array contains first
                       7 values of line segment number 1, then the 7 values
                       of line segment number 2, and so on, and it finish
                       by the 7 values of line segment number n_out.
                       The seven values are:
                       - x1,y1,x2,y2,width,p,-log10(NFA)
                       .
                       for a line segment from coordinates (x1,y1) to (x2,y2),
                       a width 'width', an angle precision of p in (0,1) given
                       by angle_tolerance/180 degree, and NFA value 'NFA'.
                       If 'out' is the returned pointer, the 7 values of
                       line segment number 'n+1' are obtained with
                       'out[7*n+0]' to 'out[7*n+6]'.
 */
double * lsd_scale_region( int * n_out,
                           double * img, int X, int Y, double scale,
                           int ** reg_img, int * reg_x, int * reg_y );

/*----------------------------------------------------------------------------*/
/** LSD Simple Interface with Scale

    @param n_out       Pointer to an int where LSD will store the number of
                       line segments detected.

    @param img         Pointer to input image data. It must be an array of
                       doubles of size X x Y, and the pixel at coordinates
                       (x,y) is obtained by img[x+y*X].

    @param X           X size of the image: the number of columns.

    @param Y           Y size of the image: the number of rows.

    @param scale       When different from 1.0, LSD will scale the input image
                       by 'scale' factor by Gaussian filtering, before detecting
                       line segments.
                       Example: if scale=0.8, the input image will be subsampled
                       to 80% of its size, before the line segment detector
                       is applied.
                       Suggested value: 0.8

    @return            A double array of size 7 x n_out, containing the list
                       of line segments detected. The array contains first
                       7 values of line segment number 1, then the 7 values
                       of line segment number 2, and so on, and it finish
                       by the 7 values of line segment number n_out.
                       The seven values are:
                       - x1,y1,x2,y2,width,p,-log10(NFA)
                       .
                       for a line segment from coordinates (x1,y1) to (x2,y2),
                       a width 'width', an angle precision of p in (0,1) given
                       by angle_tolerance/180 degree, and NFA value 'NFA'.
                       If 'out' is the returned pointer, the 7 values of
                       line segment number 'n+1' are obtained with
                       'out[7*n+0]' to 'out[7*n+6]'.
 */
double * lsd_scale(int * n_out, double * img, int X, int Y, double scale);

/*----------------------------------------------------------------------------*/
/** LSD Simple Interface

    @param n_out       Pointer to an int where LSD will store the number of
                       line segments detected.

    @param img         Pointer to input image data. It must be an array of
                       doubles of size X x Y, and the pixel at coordinates
                       (x,y) is obtained by img[x+y*X].

    @param X           X size of the image: the number of columns.

    @param Y           Y size of the image: the number of rows.

    @return            A double array of size 7 x n_out, containing the list
                       of line segments detected. The array contains first
                       7 values of line segment number 1, then the 7 values
                       of line segment number 2, and so on, and it finish
                       by the 7 values of line segment number n_out.
                       The seven values are:
                       - x1,y1,x2,y2,width,p,-log10(NFA)
                       .
                       for a line segment from coordinates (x1,y1) to (x2,y2),
                       a width 'width', an angle precision of p in (0,1) given
                       by angle_tolerance/180 degree, and NFA value 'NFA'.
                       If 'out' is the returned pointer, the 7 values of
                       line segment number 'n+1' are obtained with
                       'out[7*n+0]' to 'out[7*n+6]'.
 */
double * lsd(int * n_out, double * img, int X, int Y);

/** add mask */
ntuple_list LineSegmentDetectionMask( image_double image, image_double mask, double scale,
                                  double sigma_scale, double quant,
                                  double ang_th, double eps, double density_th,
                                  int n_bins, double max_grad,
                                  image_int * region );

/*----------------------------------------------------------------------------*/
double * GeneralLineSegmentDetection( int * n_out,
                               double * img, int X, int Y,
                               double scale, double sigma_scale, double quant,
                               double ang_th, double log_eps, double density_th,
                               int n_bins,
                               int ** reg_img, int * reg_x, int * reg_y );
}
#endif /* !LSD_HEADER */

