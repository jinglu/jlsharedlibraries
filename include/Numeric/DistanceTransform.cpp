#include "DistanceTransform.h"
#include <iostream>

DistanceTransform::DistanceTransform()
    : _INF(1e20)
{
}

void DistanceTransform::setInput(const cv::Mat &input)
{
    _input = input;
    _output = input.clone();
}

float * DistanceTransform::dist_transf(float *f, int n)
{
    float *d = new float[n];
    int *v = new int[n];
    float *z = new float[n+1];
    int k = 0;
    v[0] = 0;
    z[0] = -_INF;
    z[1] = +_INF;
    for (int q = 1; q <= n-1; q++) {
        float s  = ((f[q]+(q*q))-(f[v[k]]+(v[k] * v[k])))/(2*q-2*v[k]);
      while (s <= z[k]) {
        k--;
        s  = ((f[q]+q*q)-(f[v[k]]+v[k]*v[k]))/(2*q-2*v[k]);

      }
      k++;
      v[k] = q;
      z[k] = s;
      z[k+1] = +_INF;
    }

    k = 0;
    for (int q = 0; q <= n-1; q++) {
      while (z[k+1] < q)
        k++;
      d[q] = (q-v[k]) * (q-v[k]) + f[v[k]];
    }

    delete [] v;
    delete [] z;
    return d;
}

void DistanceTransform::dist_transf_mat(cv::Mat &im)
{
    int width = im.cols;
    int height = im.rows;
    float *f = new float[std::max(width,height)];

    // transform along columns
    for (int x = 0; x < width; x++) {
      for (int y = 0; y < height; y++) {
          f[y] = im.at<float>(y,x);
      }
      float *d = dist_transf(f, height);
      for (int y = 0; y < height; y++) {
          im.at<float>(y,x) = d[y];
      }
      delete [] d;
    }

    // transform along rows
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
          f[x] = im.at<float>(y,x);
      }
      float *d = dist_transf(f, width);
      for (int x = 0; x < width; x++) {
          im.at<float>(y,x) = d[x];
      }
      delete [] d;
    }

   delete f;
}

void DistanceTransform::visualize(const std::string name)
{
    double minv, maxv;
    cv::minMaxIdx(_output, &minv, &maxv);
    std::cout << "Dist trans ["<< minv << ", " << maxv << "]" << std::endl;
    cv::Mat im;
    cv::sqrt(_output, im);

    cv::Mat im2 = im * (255.0 / (maxv-minv));

    cv::imwrite(name, im2);
}

void DistanceTransform::mainTransform(const cv::Mat& input, cv::Mat& output)
{
    setInput(input);

    dist_transf_mat(_output);

    std::string name = "class_output.png";
    visualize(name);
    output = _output;
}
