#ifndef DISTANCETRANSFORM_H
#define DISTANCETRANSFORM_H


#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

class DistanceTransform
{
public:
    DistanceTransform();

    void setInput(const cv::Mat& input);

    float* dist_transf(float *f, int n);
    void dist_transf_mat(cv::Mat& im);

    void visualize(const std::string name);

    void mainTransform(const cv::Mat& input, cv::Mat& output);

    const float _INF;
    cv::Mat _input;
    cv::Mat _output;
};

#endif // DISTANCETRANSFORM_H
