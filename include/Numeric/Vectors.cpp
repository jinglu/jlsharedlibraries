#include "Vectors.h"

using namespace wjl;

template <typename _Tp>
Vector3<_Tp>::Vector3()
    : _vec(0,0,0)
{
}

template <typename _Tp>
Vector3<_Tp>::Vector3(_Tp v0, _Tp v1, _Tp v2)
    : _vec(v0, v1, v2)
{
}

template <typename _Tp>
Vector3<_Tp>::Vector3(const Vector3<_Tp> &v)
    : _vec(v[0],v[1],v[2])
{
}

template <typename _Tp>
 _Tp Vector3<_Tp>::operator [](int i) const
 {
     return this->_vec[i];
 }

template <typename _Tp>
_Tp& Vector3<_Tp>::operator[](int i)
{
    return this->_vec[i];
}

template <typename _Tp>
_Tp Vector3<_Tp>::dot(const Vector3<_Tp> & v) const
{
    return _vec.dot(v._vec);
}

template <typename _Tp>
Vector3<_Tp> Vector3<_Tp>::cross(const Vector3<_Tp> & v) const
{
    Vector3<_Tp> ret;
    ret._vec = _vec.cross(v._vec);
    return ret;
}

/////////////////////////////
template <typename _Tp>
REAL Vector3<_Tp>::L2norm()
{
    REAL l2 = (_vec[0] * _vec[0]) + (_vec[1]*_vec[1]) + (_vec[2]*_vec[2]);
    return sqrt(l2);
}

template <typename _Tp>
_Tp Vector3<_Tp>::L1norm()
{
    _Tp l1 = _vec[0] + _vec[1] + _vec[2];
    return l1;
}

template <typename _Tp>
void Vector3<_Tp>::L1normalize()
{
    _Tp l1 = this->L1norm();
    if (l1 < MY_ZERO_TOLLERENCE)
        return;
    else
    {
        for (size_t i = 0; i < 3; ++i)
            _vec[i] /= l1;
    }
}
template <typename _Tp>
void Vector3<_Tp>::L2normalize()
{
    _Tp l2 = this->L2norm();
    if (l2 < MY_ZERO_TOLLERENCE)
        return;
    else
    {
        for (size_t i = 0; i < 3; ++i)
            _vec[i] /= l2;
    }
}

template <typename _Tp>
Vector3<REAL> Vector3<_Tp>::L1normalizeD()
{
    REAL l1 = this->L1norm();
    Vector3<REAL> ret(0., 0., 0.);
    if (l1 < MY_ZERO_TOLLERENCE)
        return ret;
    else
    {
        for (size_t i = 0; i < 3; ++i)
            ret[i] = (REAL)_vec[i] / l1;
        return ret;
    }
}


template <typename _Tp>
Vector3<REAL> Vector3<_Tp>::L2normalizeD()
{
    REAL l2 = this->L2norm();
    Vector3<REAL> ret(0., 0., 0.);
    if (l2 < MY_ZERO_TOLLERENCE)
        return ret;
    else
    {
        for (size_t i = 0; i < 3; ++i)
            ret[i] = (REAL)_vec[i] / l2;
        return ret;
    }
}

//////////////////////////////////////////////////
template <typename _Tp>
Vector3<_Tp> & Vector3<_Tp>::operator =(const Vector3<_Tp>& v)
{
    //_vec = cv::Vec<_Tp,3>(v[0],v[1],v[2]);
    _vec = v._vec;
    return (*this);
}

template <typename _Tp>
Vector3<_Tp> & Vector3<_Tp>::operator +=(const Vector3<_Tp>& v)
{
    _vec += v._vec;
    return (*this);
}

template <typename _Tp>
Vector3<_Tp> & Vector3<_Tp>::operator -=(const Vector3<_Tp>& v)
{
    _vec -= v._vec;
    return (*this);
}
template <typename _Tp>
Vector3<_Tp> & Vector3<_Tp>::operator *=(const Vector3<_Tp>& v)
{

    for (unsigned int i = 0; i < 3; ++i)
        _vec[i] *= v._vec[i];
    return (*this);
}

template <typename _Tp>
Vector3<_Tp> & Vector3<_Tp>::operator /=(const Vector3<_Tp>& v)
{
    for (size_t i = 0; i < 3; ++i)
        _vec[i] /= v._vec[i];
    return (*this);
}


template <typename _Tp>
Vector3<_Tp> Vector3<_Tp>::operator + (const Vector3<_Tp> & v)
{
    Vector3<_Tp> ret;
    ret._vec = this->_vec + v._vec;
    return ret;
}
template <typename _Tp>
Vector3<_Tp> Vector3<_Tp>::operator - (const Vector3<_Tp> & v)
{
    Vector3<_Tp> ret;
    ret._vec = this->_vec - v._vec;
    return ret;
}

template <typename _Tp>
void Vector3<_Tp>::print()
{
    std::cout << "(" << _vec[0] << "," << _vec[1] << "," << _vec[2] << ")" << std::endl;
}

template <typename _Tp>
Vector3<_Tp> Vector3<_Tp>::operator + (const _Tp v)
{
    Vector3<_Tp> ret;
    for (size_t i = 0; i < 3; ++i)
        ret._vec[i] = this->_vec[i] + v;
    return ret;
}

template <typename _Tp>
Vector3<_Tp> Vector3<_Tp>::operator - (const _Tp v)
{
    Vector3<_Tp> ret;
    for (size_t i = 0; i < 3; ++i)
        ret._vec[i] = this->_vec[i] - v;
    return ret;
}

template <typename _Tp>
Vector3<_Tp> Vector3<_Tp>::operator / (const _Tp v)
{
    Vector3<_Tp> ret;
    ret._vec = this->_vec / v;
    return ret;
}

template <typename _Tp>
Vector3<_Tp> Vector3<_Tp>::operator * (const _Tp v)
{
    Vector3<_Tp> ret;
    ret._vec = this->_vec * v;
    return ret;
}

template class Vector3<double>;
template class Vector3<float>;
template class Vector3<int>;


/************************ Vector2 ***************************************/

template <typename _Tp>
Vector2<_Tp>::Vector2()
    : _vec(0,0)
{
}

template <typename _Tp>
Vector2<_Tp>::Vector2(_Tp v0, _Tp v1)
    : _vec(v0, v1)
{
}

template <typename _Tp>
Vector2<_Tp>::Vector2(const Vector2<_Tp> &v)
    : _vec(v[0],v[1])
{
}

template <typename _Tp>
 _Tp Vector2<_Tp>::operator [](int i) const
 {
     return this->_vec[i];
 }

template <typename _Tp>
_Tp& Vector2<_Tp>::operator[](int i)
{
    return this->_vec[i];
}

template <typename _Tp>
_Tp Vector2<_Tp>::dot(const Vector2<_Tp> & v) const
{
    return _vec.dot(v._vec);
}

template <typename _Tp>
Vector2<_Tp> Vector2<_Tp>::cross(const Vector2<_Tp> & v) const
{
    Vector2<_Tp> ret;
    ret._vec = _vec.cross(v._vec);
    return ret;
}
template <typename _Tp>
Vector2<_Tp> & Vector2<_Tp>::operator =(const Vector2<_Tp>& v)
{
    _vec = v._vec;
    return (*this);
}

template <typename _Tp>
Vector2<_Tp> & Vector2<_Tp>::operator +=(const Vector2<_Tp>& v)
{
    _vec += v._vec;
    return (*this);
}

template <typename _Tp>
Vector2<_Tp> & Vector2<_Tp>::operator -=(const Vector2<_Tp>& v)
{
    _vec -= v._vec;
    return (*this);
}
template <typename _Tp>
Vector2<_Tp> & Vector2<_Tp>::operator *=(const Vector2<_Tp>& v)
{
    for (unsigned int i = 0; i < 2; ++i)
        _vec[i] += v._vec[i];
    return (*this);
}

template <typename _Tp>
Vector2<_Tp> & Vector2<_Tp>::operator /=(const Vector2<_Tp>& v)
{
    for (size_t i = 0; i < 2; ++i)
        _vec[i] /= v._vec[i];
    return (*this);
}
//////////////////////////////////////
template <typename _Tp>
REAL Vector2<_Tp>::L2norm()
{
    REAL l2 = (_vec[0] * _vec[0]) + (_vec[1]*_vec[1]);
    return sqrt(l2);
}

template <typename _Tp>
_Tp Vector2<_Tp>::L1norm()
{
    _Tp l1 = _vec[0] + _vec[1];
    return l1;
}

template <typename _Tp>
void Vector2<_Tp>::L1normalize()
{
    _Tp l1 = this->L1norm();
    if (l1 < MY_ZERO_TOLLERENCE)
        return;
    else
    {
        for (size_t i = 0; i < 2; ++i)
            _vec[i] /= l1;
    }
}
template <typename _Tp>
void Vector2<_Tp>::L2normalize()
{
    _Tp l2 = this->L2norm();
    if (l2 < MY_ZERO_TOLLERENCE)
        return;
    else
    {
        for (size_t i = 0; i < 2; ++i)
            _vec[i] /= l2;
    }
}

template <typename _Tp>
Vector2<REAL> Vector2<_Tp>::L1normalizeD()
{
    REAL l1 = this->L1norm();
    Vector2<REAL> ret(0., 0.);
    if (l1 < MY_ZERO_TOLLERENCE)
        return ret;
    else
    {
        for (size_t i = 0; i < 2; ++i)
            ret[i] = (REAL)_vec[i] / l1;
        return ret;
    }
}


template <typename _Tp>
Vector2<REAL> Vector2<_Tp>::L2normalizeD()
{
    REAL l2 = this->L2norm();
    Vector2<REAL> ret(0., 0.);
    if (l2 < MY_ZERO_TOLLERENCE)
        return ret;
    else
    {
        for (size_t i = 0; i < 2; ++i)
            ret[i] = (REAL)_vec[i] / l2;
        return ret;
    }
}
//////////////////////////
template <typename _Tp>
Vector2<_Tp> Vector2<_Tp>::operator + (const Vector2<_Tp> & v)
{
    Vector2<_Tp> ret;
    ret._vec = this->_vec + v._vec;
    return ret;
}
template <typename _Tp>
Vector2<_Tp> Vector2<_Tp>::operator - (const Vector2<_Tp> & v)
{
    Vector2<_Tp> ret;
    ret._vec = this->_vec - v._vec;
    return ret;
}

template <typename _Tp>
void Vector2<_Tp>::print()
{
    std::cout << "(" << _vec[0] << "," << _vec[1] << ")" << std::endl;
}

template <typename _Tp>
Vector2<_Tp> Vector2<_Tp>::operator + (const _Tp v)
{
    Vector2<_Tp> ret;
    for (size_t i = 0; i < 2; ++i)
        ret._vec[i] = this->_vec[i] + v;
    return ret;
}

template <typename _Tp>
Vector2<_Tp> Vector2<_Tp>::operator - (const _Tp v)
{
    Vector2<_Tp> ret;
    for (size_t i = 0; i < 2; ++i)
        ret._vec[i] = this->_vec[i] - v;
    return ret;
}

template <typename _Tp>
Vector2<_Tp> Vector2<_Tp>::operator / (const _Tp v)
{
    Vector2<_Tp> ret;
    ret._vec = this->_vec / v;
    return ret;
}

template <typename _Tp>
Vector2<_Tp> Vector2<_Tp>::operator * (const _Tp v)
{
    Vector2<_Tp> ret;
    ret._vec = this->_vec * v;
    return ret;
}

template class Vector2<double>;
template class Vector2<float>;
template class Vector2<int>;

/************************ Vector4 ***************************************/

template <typename _Tp>
Vector4<_Tp>::Vector4()
    : _vec(0,0,0,0)
{
}

template <typename _Tp>
Vector4<_Tp>::Vector4(_Tp v0, _Tp v1, _Tp v2, _Tp v3)
    : _vec(v0, v1, v2, v3)
{
}

template <typename _Tp>
Vector4<_Tp>::Vector4(const Vector4<_Tp> &v)
    : _vec(v[0],v[1],v[2],v[3])
{
}

template <typename _Tp>
 _Tp Vector4<_Tp>::operator [](int i) const
 {
     return this->_vec[i];
 }

template <typename _Tp>
_Tp& Vector4<_Tp>::operator[](int i)
{
    return this->_vec[i];
}

template <typename _Tp>
_Tp Vector4<_Tp>::dot(const Vector4<_Tp> & v) const
{
    return _vec.dot(v._vec);
}

template <typename _Tp>
Vector4<_Tp> Vector4<_Tp>::cross(const Vector4<_Tp> & v) const
{
    Vector4<_Tp> ret;
    ret._vec = _vec.cross(v._vec);
    return ret;
}
template <typename _Tp>
Vector4<_Tp> & Vector4<_Tp>::operator =(const Vector4<_Tp>& v)
{
    _vec = v._vec;
    return (*this);
}

template <typename _Tp>
Vector4<_Tp> & Vector4<_Tp>::operator +=(const Vector4<_Tp>& v)
{
    _vec += v._vec;
    return (*this);
}

template <typename _Tp>
Vector4<_Tp> & Vector4<_Tp>::operator -=(const Vector4<_Tp>& v)
{
    _vec -= v._vec;
    return (*this);
}
template <typename _Tp>
Vector4<_Tp> & Vector4<_Tp>::operator *=(const Vector4<_Tp>& v)
{
    for (unsigned int i = 0; i < 4; ++i)
        _vec[i] += v._vec[i];
    return (*this);
}

template <typename _Tp>
Vector4<_Tp> & Vector4<_Tp>::operator /=(const Vector4<_Tp>& v)
{
    for (size_t i = 0; i < 4; ++i)
        _vec[i] /= v._vec[i];
    return (*this);
}


template <typename _Tp>
Vector4<_Tp> Vector4<_Tp>::operator + (const Vector4<_Tp> & v)
{
    Vector4<_Tp> ret;
    ret._vec = this->_vec + v._vec;
    return ret;
}
template <typename _Tp>
Vector4<_Tp> Vector4<_Tp>::operator - (const Vector4<_Tp> & v)
{
    Vector4<_Tp> ret;
    ret._vec = this->_vec - v._vec;
    return ret;
}

template <typename _Tp>
void Vector4<_Tp>::print()
{
    std::cout << "(" << _vec[0] << "," << _vec[1] << "," << _vec[2] << ","<<_vec[3]<< ")" << std::endl;
}

template <typename _Tp>
Vector4<_Tp> Vector4<_Tp>::operator + (const _Tp v)
{
    Vector4<_Tp> ret;
    for (size_t i = 0; i < 4; ++i)
        ret._vec[i] = this->_vec[i] + v;
    return ret;
}

template <typename _Tp>
Vector4<_Tp> Vector4<_Tp>::operator - (const _Tp v)
{
    Vector4<_Tp> ret;
    for (size_t i = 0; i < 4; ++i)
        ret._vec[i] = this->_vec[i] - v;
    return ret;
}

template <typename _Tp>
Vector4<_Tp> Vector4<_Tp>::operator / (const _Tp v)
{
    Vector4<_Tp> ret;
    ret._vec = this->_vec / v;
    return ret;
}

template <typename _Tp>
Vector4<_Tp> Vector4<_Tp>::operator * (const _Tp v)
{
    Vector4<_Tp> ret;
    ret._vec = this->_vec * v;
    return ret;
}


/////////////////////////////
template <typename _Tp>
REAL Vector4<_Tp>::L2norm()
{
    REAL l2 = (_vec[0] * _vec[0]) + (_vec[1]*_vec[1]) + (_vec[2]*_vec[2]) + (_vec[3] *_vec[3]);
    return sqrt(l2);
}

template <typename _Tp>
_Tp Vector4<_Tp>::L1norm()
{
    _Tp l1 = _vec[0] + _vec[1] + _vec[2] + _vec[3];
    return l1;
}

template <typename _Tp>
void Vector4<_Tp>::L1normalize()
{
    _Tp l1 = this->L1norm();
    if (l1 < MY_ZERO_TOLLERENCE)
        return;
    else
    {
        for (size_t i = 0; i < 4; ++i)
            _vec[i] /= l1;
    }
}
template <typename _Tp>
void Vector4<_Tp>::L2normalize()
{
    _Tp l2 = this->L2norm();
    if (l2 < MY_ZERO_TOLLERENCE)
        return;
    else
    {
        for (size_t i = 0; i < 4; ++i)
            _vec[i] /= l2;
    }
}

template <typename _Tp>
Vector4<REAL> Vector4<_Tp>::L1normalizeD()
{
    REAL l1 = this->L1norm();
    Vector4<REAL> ret(0., 0., 0.,0.);
    if (l1 < MY_ZERO_TOLLERENCE)
        return ret;
    else
    {
        for (size_t i = 0; i < 4; ++i)
            ret[i] = (REAL)_vec[i] / l1;
        return ret;
    }
}


template <typename _Tp>
Vector4<REAL> Vector4<_Tp>::L2normalizeD()
{
    REAL l2 = this->L2norm();
    Vector4<REAL> ret(0., 0., 0.,0.);
    if (l2 < MY_ZERO_TOLLERENCE)
        return ret;
    else
    {
        for (size_t i = 0; i < 4; ++i)
            ret[i] = (REAL)_vec[i] / l2;
        return ret;
    }
}

template class Vector4<double>;
template class Vector4<float>;
template class Vector4<int>;
