#ifndef VECTORS_H
#define VECTORS_H

#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "../Common/global.h"

namespace wjl {
template<typename _Tp>
class Vector3
{
public:
    Vector3();
    Vector3(_Tp v0, _Tp v1, _Tp v2);
    Vector3(const Vector3<_Tp>& v);

    _Tp dot(const Vector3<_Tp>& v) const;
    Vector3<_Tp> cross(const Vector3<_Tp>& v) const;

    template<typename T2> operator Vector3<T2>() const;
    _Tp operator [](int i) const;
    _Tp& operator[](int i);

    REAL L2norm();
    _Tp L1norm();

    void L1normalize();
    void L2normalize();
    Vector3<REAL> L1normalizeD();
    Vector3<REAL> L2normalizeD();

    Vector3<_Tp> & operator =(const Vector3<_Tp>& v);
    Vector3<_Tp> & operator +=(const Vector3<_Tp>& v);
    Vector3<_Tp> & operator -=(const Vector3<_Tp>& v);
    Vector3<_Tp> & operator *=(const Vector3<_Tp>& v);
    Vector3<_Tp> & operator /=(const Vector3<_Tp>& v);


    Vector3<_Tp> operator + (const Vector3<_Tp> & v);
    Vector3<_Tp> operator - (const Vector3<_Tp> & v);


    Vector3<_Tp> operator + (const _Tp v);
    Vector3<_Tp> operator - (const _Tp v);

    Vector3<_Tp> operator * (const _Tp v);
    Vector3<_Tp> operator / (const _Tp v);


    void print();

    //member
    cv::Vec<_Tp,3> _vec;
};

typedef Vector3<int> Vector3i;
typedef Vector3<float> Vector3f;
typedef Vector3<double> Vector3d;

/************************ Vector2 ************************/

template<typename _Tp>
class Vector2
{
public:
    Vector2();
    Vector2(_Tp v0, _Tp v1);
    Vector2(const Vector2<_Tp>& v);

    _Tp dot(const Vector2<_Tp>& v) const;

    Vector2<_Tp> cross(const Vector2<_Tp>& v) const;

    template<typename T2> operator Vector2<T2>() const;
    _Tp operator [](int i) const;
    _Tp& operator[](int i);
    REAL L2norm();
    _Tp L1norm();

    void L1normalize();
    void L2normalize();
    Vector2<REAL> L1normalizeD();
    Vector2<REAL> L2normalizeD();

    Vector2<_Tp> & operator =(const Vector2<_Tp>& v);
    Vector2<_Tp> & operator +=(const Vector2<_Tp>& v);
    Vector2<_Tp> & operator -=(const Vector2<_Tp>& v);
    Vector2<_Tp> & operator *=(const Vector2<_Tp>& v);
    Vector2<_Tp> & operator /=(const Vector2<_Tp>& v);


    Vector2<_Tp> operator + (const Vector2<_Tp> & v);
    Vector2<_Tp> operator - (const Vector2<_Tp> & v);


    Vector2<_Tp> operator + (const _Tp v);
    Vector2<_Tp> operator - (const _Tp v);

    Vector2<_Tp> operator * (const _Tp v);
    Vector2<_Tp> operator / (const _Tp v);


    void print();

    //member
    cv::Vec<_Tp,2> _vec;
};

typedef Vector2<int> Vector2i;
typedef Vector2<float> Vector2f;
typedef Vector2<double> Vector2d;

/***************************Vector4************************/

template<typename _Tp>
class Vector4
{
public:
    Vector4();
    Vector4(_Tp v0, _Tp v1, _Tp v2, _Tp v3);
    Vector4(const Vector4<_Tp>& v);

    _Tp dot(const Vector4<_Tp>& v) const;
    Vector4<_Tp> cross(const Vector4<_Tp>& v) const;

    template<typename T2> operator Vector4<T2>() const;
    _Tp operator [](int i) const;
    _Tp& operator[](int i);

    REAL L2norm();
    _Tp L1norm();

    void L1normalize();
    void L2normalize();
    Vector4<REAL> L1normalizeD();
    Vector4<REAL> L2normalizeD();

    Vector4<_Tp> & operator =(const Vector4<_Tp>& v);
    Vector4<_Tp> & operator +=(const Vector4<_Tp>& v);
    Vector4<_Tp> & operator -=(const Vector4<_Tp>& v);
    Vector4<_Tp> & operator *=(const Vector4<_Tp>& v);
    Vector4<_Tp> & operator /=(const Vector4<_Tp>& v);


    Vector4<_Tp> operator + (const Vector4<_Tp> & v);
    Vector4<_Tp> operator - (const Vector4<_Tp> & v);


    Vector4<_Tp> operator + (const _Tp v);
    Vector4<_Tp> operator - (const _Tp v);

    Vector4<_Tp> operator * (const _Tp v);
    Vector4<_Tp> operator / (const _Tp v);


    void print();

    //member
    cv::Vec<_Tp,4> _vec;
};

typedef Vector4<int> Vector4i;
typedef Vector4<float> Vector4f;
typedef Vector4<double> Vector4d;

}
#endif // VECTORS_H
