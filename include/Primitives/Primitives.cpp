#include "Primitives.h"
#include <algorithm>
#include <fstream>
#include <set>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

using namespace wjl;

LineSeg2i::LineSeg2i()
{

}

LineSeg2i::LineSeg2i(const Point2i & start, const Point2i & end)
    : _start(start), _end(end)
{
}

LineSeg2i::LineSeg2i(int x0, int y0, int x1, int y1)
    : _start(x0, y0), _end(x1, y1)
{

}

LineSeg2i::LineSeg2i(const LineSeg2i &line)
    : _start(line._start), _end(line._end)
{

}

LineSeg2i::~LineSeg2i()
{

}

int LineSeg2i::length2() const
{
    return ((_end.x -_start.x) * (_end.x - _start.x) + (_end.y - _start.y) * (_end.y - _start.y));
}

LineSeg2i & LineSeg2i::operator =(const LineSeg2i & rhs)
{
    assert (this != &rhs);
    {
        _start = rhs._start;
        _end = rhs._end;
        return *this;
    }
}

REAL LineSeg2i::length() const
{
    REAL l2 = (REAL)length2();
    return sqrt(l2);
}

Vector2i LineSeg2i::toVec2i() const
{
    Vector2i ret(this->_end.x - this->_start.x, this->_end.y - this->_start.y);
    return ret;
}
/// normalized direction
Vector2<REAL> LineSeg2i::direction() const
{
    REAL x = this->_end.x - this->_start.x;
    REAL y = this->_end.y - this->_start.y;
    Vector2<REAL> ret(x,y);
    ret.L2normalize();
    return ret;
}

REAL LineSeg2i::angleX() const
{
    wjl::Vector2<REAL> dir = this->direction();
    return acos(dir[0]);
}

REAL LineSeg2i::angleDegree() const
{
    REAL angle = this->angleX();
    return (180.0 * angle / 3.14159265);
}
REAL LineSeg2i::point_distance(const LineSeg2i &line) const
{
    double ret = 0;
    cv::Point ps = line.start() - this->start();
    ret += sqrt(ps.ddot(ps));
    ps = line.end() - this->end();
    ret += sqrt(ps.ddot(ps));
    return ret;
}

double LineSeg2i::min_point_distance(const LineSeg2i &s1, const LineSeg2i &s2)
{
    std::vector<double> dists;
    dists.reserve(4);
    cv::Point ps = s1.start() - s2.start();
    dists.push_back(sqrt(ps.ddot(ps)));
    ps = s1.start() - s2.end();
    dists.push_back(sqrt(ps.ddot(ps)));

    ps = s1.end() - s2.start();
    dists.push_back(sqrt(ps.ddot(ps)));

    ps = s1.end() - s2.end();
    dists.push_back(sqrt(ps.ddot(ps)));

    return *std::min_element(dists.begin(), dists.end());
}

void LineSeg2i::scale(const double factor)
{
    _start.x = int(_start.x * factor);
    _start.y = int(_start.y * factor);
    _end.x = int(_end.x * factor);
    _end.y = int(_end.y * factor);
}
void LineSeg2i::scale(const LineSeg2i &src, const double factor, LineSeg2i &dst)
{
    dst._start = src._start * factor;
    dst._end = src._end * factor;
}

void LineSeg2i::print(std::ostream& str) const
{
    str << "Line: (" << _start.x << "," << _start.y << ") --> (" << _end.x << "," << _end.y << ")\n";
}
void LineSeg2i::simple_print(std::ostream &os) const
{
    os << _start.x << " " << _start.y << " "<< _end.x << " " << _end.y;
}
//std::ostream& operator << (std::ostream& str, const LineSeg2i& line)
//{
//    return str << "Line: (" << line._start.x << "," << line._start.y << ") --> (" << line._end.x << "," << line._end.y << ")\n";
//}
const cv::Point & LineSeg2i::start() const
{
    return _start;
}
const cv::Point & LineSeg2i::end() const
{
    return _end;
}
cv::Point & LineSeg2i::start()
{
    return _start;
}
cv::Point & LineSeg2i::end()
{
    return _end;
}

bool LineSeg2i::read_line2d_txt(const std::string & filename, std::vector<wjl::LineSeg2i> & lines_2d)
{
    std::ifstream ifs(filename.c_str());
    if (!ifs.is_open())
        return false;
//    int n;
//    ifs>>n;

    char temp[1024];
    ifs.getline(temp, 1024);
    int n = boost::lexical_cast<int>(temp);
    lines_2d.clear();
    lines_2d.reserve(n);
    for(int i = 0; i < n; i++){
        ifs.getline(temp, 1024);
        std::string textLine(temp);
        std::vector<std::string> textElement;

        boost::split(textElement, textLine, boost::is_any_of(" ,\t"), boost::token_compress_on);
        int x0, y0, x1, y1, r, g, b;

        if (textElement.size() >= 4)
        {
            x0 = boost::lexical_cast<int>(textElement[0]);
            y0 = boost::lexical_cast<int>(textElement[1]);
            x1 = boost::lexical_cast<int>(textElement[2]);
            y1 = boost::lexical_cast<int>(textElement[3]);

        }
//        else if (textElement.size() >= 6)
//        {
//            r = boost::lexical_cast<int>(textElement[4]);
//            g = boost::lexical_cast<int>(textElement[5]);
//            b = boost::lexical_cast<int>(textElement[6]);
//        }
        lines_2d.push_back(wjl::LineSeg2i(x0,y0,x1,y1));
        //std::cout << "read line " << lines_2d.back()<<"\n";
        //tupleset.insert(std::make_tuple(x0,y0,x1,y1));
    }

    std::cout << "load 2d lines # " << lines_2d.size()<<"\n";
    ifs.close();
    return true;
}

bool LineSeg2i::write_line2d_txt(const std::string & filename, std::vector<wjl::LineSeg2i> & lines_2d)
{
    std::ofstream ofs(filename.c_str());
    if (!ofs.is_open())
        return false;
    ofs<<lines_2d.size()<<"\n";
    for(int i = 0; i < lines_2d.size(); i++){
        ofs<<lines_2d[i].start().x<<" "<<lines_2d[i].start().y<<" "
          <<lines_2d[i].end().x<<" "<<lines_2d[i].end().y<<"\n";
    }
    ofs.close();
    std::cout << "save 2d lines # " << lines_2d.size()<<"\n";
    return true;
}

bool LineSeg2i::is_intersect_polygon(const cv::Mat & img, const std::vector<cv::Point> &vertices) const
{
    cv::LineIterator line_it(img, start(), end(), 8);
    //std::cout << "line pt # " << line_it.count<<"\n";
    for (int i = 0; i < line_it.count; ++i, ++line_it)
    {
        if (cv::pointPolygonTest(vertices, line_it.pos(), false) >= 0)
            return true;
    }
    return false;
}

//LineSeg2i LineSeg2i::ptsFitLine(const std::vector<cv::Point> &select_pts)
//{
//    cv::Mat input_data(select_pts.size(), 2, CV_32FC1);
//    for (size_t pi = 0; pi < select_pts.size(); ++pi)
//    {
//        input_data.at<float>(pi,0) = select_pts[pi].x;
//        input_data.at<float>(pi,1) = select_pts[pi].y;
//    }
//    int numComponent = 2;
//    cv::PCA pca(input_data, cv::Mat(), CV_PCA_DATA_AS_ROW, numComponent);

//}

LineSeg2i LineSeg2i::linesFitLine(const cv::Mat & img, const std::vector<LineSeg2i> &lines)
{
    std::vector<cv::Point> select_pts;
    cv::Mat end_data(lines.size() * 2, 2, CV_32FC1);

    for (int li = 0; li < lines.size(); ++li)
    {
        const wjl::LineSeg2i & line =  lines[li];
        cv::LineIterator litr = cv::LineIterator(img,
                                                 line.start(),
                                                 line.end(), 8);
        for (int j = 0; j < litr.count; ++j, ++litr)
        {
            select_pts.push_back(litr.pos());
        }
        end_data.at<float>(li*2,0) = line.x0();
        end_data.at<float>(li*2,1) = line.y0();
        end_data.at<float>(li*2+1,0) = line.x1();
        end_data.at<float>(li*2+1,1) = line.y1();
    }
    cv::Mat input_data(select_pts.size(), 2, CV_32FC1);
    for (size_t pi = 0; pi < select_pts.size(); ++pi)
    {
        input_data.at<float>(pi,0) = select_pts[pi].x;
        input_data.at<float>(pi,1) = select_pts[pi].y;
    }
    int numComponent = 2;
    cv::PCA pca(input_data, cv::Mat(), CV_PCA_DATA_AS_ROW, numComponent);

    cv::Mat project_end_points = pca.project(end_data);
    float max_pc = std::numeric_limits<float>::min();
    float min_pc = std::numeric_limits<float>::max();
    for (size_t pi = 0; pi < project_end_points.rows; ++pi)
    {
        max_pc = std::max(max_pc, project_end_points.at<float>(pi,0));
        min_pc = std::min(min_pc, project_end_points.at<float>(pi,0));
    }
    cv::Mat startmat = min_pc * pca.eigenvectors.row(0) + pca.mean;
    cv::Mat endmat= max_pc * pca.eigenvectors.row(0) + pca.mean;
    wjl::LineSeg2i ft_line;
    ft_line._start = cv::Point(startmat.at<float>(0,0), startmat.at<float>(0,1));
    ft_line._end = cv::Point(endmat.at<float>(0,0), endmat.at<float>(0,1));

    return ft_line;
}

bool lineIntersection(
double Ax, double Ay,
double Bx, double By,
double Cx, double Cy,
double Dx, double Dy,
double & X, double & Y)
{

  double  distAB, theCos, theSin, newX, ABpos ;

  //  Fail if either line is undefined.
  if (Ax==Bx && Ay==By || Cx==Dx && Cy==Dy) return false;

  //  (1) Translate the system so that point A is on the origin.
  Bx-=Ax; By-=Ay;
  Cx-=Ax; Cy-=Ay;
  Dx-=Ax; Dy-=Ay;

  //  Discover the length of segment A-B.
  distAB=sqrt(Bx*Bx+By*By);

  //  (2) Rotate the system so that point B is on the positive X axis.
  theCos=Bx/distAB;
  theSin=By/distAB;
  newX=Cx*theCos+Cy*theSin;
  Cy  =Cy*theCos-Cx*theSin; Cx=newX;
  newX=Dx*theCos+Dy*theSin;
  Dy  =Dy*theCos-Dx*theSin; Dx=newX;

  //  Fail if the lines are parallel.
  if (Cy == Dy)
  {
//      if (Cy != 0) return false;
//      else
//      {
//          if ((Cx > Bx && Dx > Bx) || (Cx < 0 && Dx < 0))
//              return false;
//          else
//              return true;
//      }
      return false;
  }

  //  (3) Discover the position of the intersection point along line A-B.
  ABpos=Dx+(Cx-Dx)*Dy/(Dy-Cy);

  //  (4) Apply the discovered position to line A-B in the original coordinate system.
  X=Ax+ABpos*theCos;
  Y=Ay+ABpos*theSin;

  //  Success.
  return true;
}

bool LineSeg2i::is_intersect(const wjl::LineSeg2i & line, double & at_x, double & at_y) const
{
    return lineIntersection(start().x, start().y, end().x, end().y,
                     line.start().x, line.start().y, line.end().x, line.end().y,
                     at_x, at_y);

}
double
LineSeg2i::dist2D_Segment_to_Segment( const LineSeg2i & S1, const LineSeg2i & S2)
{
    const double SMALL_NUM =  0.00000001; // anything that avoids division overflow

    Vector2d u = S1.direction();
    Vector2d v = S2.direction();
    cv::Point wp = S1._start - S2._start;
    Vector2d w = Vector2d(wp.x, wp.y);

    double a = u.dot(u);
    double b = u.dot(v);
    double c = v.dot(v);
    double d = u.dot(w);
    double e = v.dot(w);
    double D = a*c - b*b;
    double    sc, sN, sD = D;       // sc = sN / sD, default sD = D >= 0
    double    tc, tN, tD = D;

    // compute the line parameters of the two closest points
    if (D < SMALL_NUM) { // the lines are almost parallel
        sN = 0.0;         // force using point P0 on segment S1
        sD = 1.0;         // to prevent possible division by 0.0 later
        tN = e;
        tD = c;
    }
    else {                 // get the closest points on the infinite lines
        sN = (b*e - c*d);
        tN = (a*e - b*d);
        if (sN < 0.0) {        // sc < 0 => the s=0 edge is visible
            sN = 0.0;
            tN = e;
            tD = c;
        }
        else if (sN > sD) {  // sc > 1  => the s=1 edge is visible
            sN = sD;
            tN = e + b;
            tD = c;
        }
    }

    if (tN < 0.0) {            // tc < 0 => the t=0 edge is visible
        tN = 0.0;
        // recompute sc for this edge
        if (-d < 0.0)
            sN = 0.0;
        else if (-d > a)
            sN = sD;
        else {
            sN = -d;
            sD = a;
        }
    }
    else if (tN > tD) {      // tc > 1  => the t=1 edge is visible
        tN = tD;
        // recompute sc for this edge
        if ((-d + b) < 0.0)
            sN = 0;
        else if ((-d + b) > a)
            sN = sD;
        else {
            sN = (-d +  b);
            sD = a;
        }
    }
    // finally do the division to get sc and tc
    sc = (fabs(sN) < SMALL_NUM ? 0.0 : sN / sD);
    tc = (fabs(tN) < SMALL_NUM ? 0.0 : tN / tD);

    // get the difference of the two closest points
    //Vector   dP = w + (sc * u) - (tc * v);  // =  S1(sc) - S2(tc)
    Vector2d dP = w + ( u * sc) - (v * tc);  // =  S1(sc) - S2(tc)

    return dP.L2norm();   // return the closest distance
}


///////////////////*********** SizeI *************//////////////////
SizeI::SizeI()
    : _w(0), _h(0)
{
}

SizeI::SizeI(int w, int h)
    : _w(w), _h(h)
{
}

int SizeI::width() const
{
    return _w;
}

int SizeI::height() const
{
    return _h;
}

/***************************** Rectangle ******************/

Rectangle::Rectangle()
{

}

Rectangle::Rectangle(Point2i pos, int w, int h)
    : _cvrect(cv::Rect(pos.x,pos.y,w,h))
{
}

Rectangle::Rectangle(int x, int y, int w, int h)
    : _cvrect(cv::Rect(x,y,w,h))
{
}

Rectangle::Rectangle(Point2i tl, Point2i dr)
    : _cvrect(cv::Rect(tl,dr))
{
}

Rectangle::Rectangle(const cv::Rect &cvrect)
    : _cvrect(cvrect)
{

}

Rectangle::~Rectangle()
{
    //std::cout << "Rectangle destructor" << std::endl;
}

Rectangle& Rectangle::operator =(const Rectangle & rect)
{
    _cvrect = rect._cvrect;
    return (*this);
}

Rectangle& Rectangle::operator &=(const Rectangle & rect)
{
    _cvrect &= rect._cvrect;
    return (*this);
}
Rectangle& Rectangle::operator |=(const Rectangle & rect)
{
    _cvrect |= rect._cvrect;
    return (*this);
}

Rectangle& Rectangle::operator +=(const Point2i& p)
{
    _cvrect += p;
    return (*this);
}

Rectangle& Rectangle::operator -=(const Point2i& p)
{
    _cvrect -= p;
    return (*this);
}

////////////////******************get info********************///////////////////////

int Rectangle::x() const
{
    return _cvrect.x;
}

int Rectangle::y() const
{
    return _cvrect.y;
}

int Rectangle::width() const
{
    return _cvrect.width;
}

int Rectangle::height() const
{
    return _cvrect.height;
}

//right x
int Rectangle::rx() const
{
    return (_cvrect.x + _cvrect.width - 1);
}

//bottom y
int Rectangle::by() const
{
    return (_cvrect.y + _cvrect.height - 1);
}

//top left point
Point2i Rectangle::tl() const
{
    return _cvrect.tl();
}

//bottom right point
Point2i Rectangle::br() const
{
    return _cvrect.br();
}

Point2i Rectangle::center() const
{
    Point2i ret = (_cvrect.tl() + _cvrect.br()) * 0.5;
    return ret;
}

//top right point
Point2i Rectangle::tr() const
{
    return Point2i(this->rx(), this->y());
}

//bottom left point
Point2i Rectangle::bl() const
{
    return Point2i(this->x(), this->by());
}

int Rectangle::area() const
{
    return _cvrect.area();
}

////////////////////////*************Set member **************************////////////////////

void Rectangle::setX(int x)
{
    _cvrect.x = x;
}

void Rectangle::setY(int y)
{
    _cvrect.y = y;
}

void Rectangle::setW(int w)
{
    _cvrect.width = w;
}

void Rectangle::setH(int h)
{
    _cvrect.height = h;
}

//////////////////////////////////////////////////////////////////////

Rectangle Rectangle::shift(int x, int y)
{
    Rectangle ret;
    ret._cvrect = cv::Rect(this->_cvrect.x+x, this->_cvrect.y+y, this->_cvrect.width, this->_cvrect.height);
    return ret;
}

Rectangle Rectangle::operator &(const Rectangle & rect)
{
    Rectangle ret;
    ret._cvrect = this->_cvrect & rect._cvrect;
    return ret;
}

Rectangle Rectangle::operator |(const Rectangle & rect)
{
    Rectangle ret;
    ret._cvrect = this->_cvrect | rect._cvrect;
    return ret;
}

Rectangle Rectangle::operator +(const Point2i& p)
{
    Rectangle ret;
    ret._cvrect = this->_cvrect + p;
    return ret;
}

Rectangle Rectangle::operator -(const Point2i& p)
{
    Rectangle ret;
    ret._cvrect = this->_cvrect - p;
    return ret;
}

bool Rectangle::operator ==(const Rectangle & rect)
{
    if (this->_cvrect == rect._cvrect)
        return true;
    else
        return false;
}

bool Rectangle::operator !=(const Rectangle & rect)
{
    if (this->_cvrect != rect._cvrect)
        return true;
    else
        return false;
}

////////////************ Relationship ************/////////

bool Rectangle::contains(const Point2i &pt) const
{
    return _cvrect.contains(pt);
}

bool Rectangle::contains(const Rectangle &rect) const
{
    return (_cvrect.contains(rect.tl()) && _cvrect.contains(rect.br()));
}

int Rectangle::insect_area(const Rectangle &rect) const
{
    cv::Rect insect = (this->_cvrect) & (rect._cvrect);
    return insect.area();
}

bool Rectangle::is_insect(const Rectangle &rect) const
{
    cv::Rect insect = (this->_cvrect) & (rect._cvrect);
    if (insect.area() > 0)
        return true;
    else
        return false;
}

void Rectangle::print() const
{
    std::cout << "Rect: pos(" << x() << "," << y() << "), size(" << width() << "," << height() << ")\n";

}

std::ostream& operator << (std::ostream& str, Rectangle & rect)
{
    str << "Rect: pos (" << rect.x() << "," << rect.y() << "), size(" << rect.width() << "," << rect.height() << ")\n";
    return str;
}

void Rectangle::sort_by_area(std::vector<Rectangle>& rects)
{
    std::sort(rects.begin(), rects.end(), area_greater);
}

bool wjl::area_greater(Rectangle a, Rectangle b)
{
    return (a.area() > b.area());
}

/////////////************** Open operation **********/////////

Rectangle Rectangle::dilate(int x, int y) const
{

    Point2i ret_tl = this->tl() - Point2i(x,y);
    Point2i ret_br = this->br() + Point2i(x,y);
    Rectangle ret(ret_tl, ret_br);
    return ret;
}

Rectangle Rectangle::erode(int x, int y) const
{
    Point2i ret_tl = this->tl() + Point2i(x,y);
    Point2i ret_br = this->br() - Point2i(x,y);
    Rectangle ret(ret_tl, ret_br);
    return ret;
}


Rectangle Rectangle::resize(int new_w, int new_h) const
{

    Point2i center = this->center();

    Rectangle ret(center.x - new_w / 2, center.y - new_h / 2, new_w, new_h);
    return ret;
}

Rectangle Rectangle::scale(double rate_w, double rate_h) const
{
    Point2i center = this->center();
    int new_w = this->width() * rate_w;
    int new_h = this->height() * rate_h;
    Rectangle ret(center.x - new_w / 2, center.y - new_h / 2, new_w, new_h);
    return ret;
}

///////*************** dist ****************////////////////
int Rectangle::shape_dist2(const Rectangle r1, const Rectangle r2)
{
    Point2i shape1(r1.width(), r1.height());
    Point2i shape2(r2.width(), r2.height());
    shape1 = shape1 - shape2;
    int ret = shape1.dot(shape1);
    return ret;
}

Rectangle::RelationType Rectangle::relation(const Rectangle r, const int img_w, const int img_h) const
{
    Rectangle left_rect(Point2i(1,this->y()), this->bl());

    if (r.is_insect(left_rect))
    {
        return Rectangle::LEFT;
    }
    else
    {
        Rectangle right_rect(this->tr(), Point2i(img_w-1, this->by()));
        if (r.is_insect(right_rect))
        {
            return Rectangle::RIGHT;
        }
        else
        {
            Rectangle above_rect(Point2i(this->x(), 1), this->tr());
            if (r.is_insect(above_rect))
            {
                return Rectangle::ABOVE;
            }
            else
            {
                Rectangle below_rect(this->bl(), Point2i(this->rx(), img_h-1));
                if (r.is_insect(below_rect))
                {
                    return Rectangle::BELOW;
                }
                else
                    return Rectangle::NONE;
            }

        }
    }

}


int Rectangle::min_dist2(const Rectangle r1, const Rectangle r2)
{
    std::vector<Point2i> r1pts;
    std::vector<Point2i> r2pts;

    r1pts.push_back(r1.tl());
    r1pts.push_back(r1.tr());
    r1pts.push_back(r1.bl());
    r1pts.push_back(r1.br());
    r1pts.push_back(r1.center());

    r2pts.push_back(r2.tl());
    r2pts.push_back(r2.tr());
    r2pts.push_back(r2.bl());
    r2pts.push_back(r2.br());
    r2pts.push_back(r2.center());

    int min_dist2;

    for (unsigned int i = 0; i < r1pts.size(); ++i)
        for (unsigned int j = 0; j < r2pts.size(); ++j)
        {
            Point2i diff = r1pts.at(i) - r2pts.at(j);
            int dist = diff.dot(diff);
            if (i == 0 && j == 0)
                min_dist2 = dist;
            else
                if (dist < min_dist2)
                    min_dist2 = dist;
        }
    return min_dist2;
}

void Rectangle::getBoundBox(std::vector<cv::Point> pts, Rectangle &boundBox)
{
    assert(!pts.empty());
    int left, right, top, bottom;

    for (int i = 0; i < pts.size(); ++i)
    {
        if (i == 0)
        {
            left = pts.at(i).x;
            right = pts.at(i).x;
            top = pts.at(i).y;
            bottom = pts.at(i).y;
        }
        else
        {
            if (pts.at(i).x < left)
                left = pts.at(i).x;
            if (pts.at(i).x > right)
                right = pts.at(i).x;
            if (pts.at(i).y < top)
                top = pts.at(i).y;
            if (pts.at(i).y > top)
                bottom = pts.at(i).y;
        }
    }
    boundBox = Rectangle(left, top, right-left+1, bottom-right+1);
}
