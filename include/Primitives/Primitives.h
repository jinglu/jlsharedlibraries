#ifndef PRIMITIVES_H
#define PRIMITIVES_H

#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv/ml.h>
#include "../Numeric/Vectors.h"
#include "../Common/compare.h"
#include "../Common/global.h"
#include <iostream>


namespace wjl{
typedef cv::Point2i Point2i;
typedef cv::Point2d Point2d;
typedef cv::Size Size;

//class Point2i
//{
//public:
//    Point2i();
//    Point2i(int x, int y);
//    Point2i(const Point2i &p);

//    Point2i & operator =(const Point2i & p);
//    Point2i & operator +=(const Point2i & p);
//    Point2i & operator -=(const Point2i & p);

//    Point2i & operator + (const Point2i & p);
//    Point2i & operator - (const Point2i & p);

//    cv::Point2i _cvp2i;
//};


class LineSeg2i
{
public:
    LineSeg2i();
    LineSeg2i(const Point2i & start, const Point2i & end);
    LineSeg2i(int x0, int y0, int x1, int y1);
    LineSeg2i(const LineSeg2i & line);
    virtual ~LineSeg2i();

    LineSeg2i & operator = (const LineSeg2i & rhs);

    REAL length() const;
    int length2() const;

    Vector2i toVec2i() const;
    Vector2<REAL> direction() const;
    REAL angleX() const;
    REAL angleDegree() const;

    void scale(const double factor);
    static void scale(const LineSeg2i & src, const double factor,LineSeg2i & dst);

    const cv::Point & start() const;
    const cv::Point & end() const;

    cv::Point & start();
    cv::Point & end();

    inline int x0() const { return start().x; }
    inline int y0() const { return start().y; }
    inline int x1() const { return end().x; }
    inline int y1() const { return end().y; }

    /** I O
     */
    void print(std::ostream& str) const;
    void simple_print(std::ostream & os) const;
    friend std::ostream& operator << (std::ostream& str, const LineSeg2i& line)
    {
        str << line._start.x << " " << line._start.y << " " << line._end.x << " " << line._end.y;
        return str;
    }
    static bool read_line2d_txt(const std::string & filename, std::vector<wjl::LineSeg2i> & lines_2d);
    static bool write_line2d_txt(const std::string & filename, std::vector<wjl::LineSeg2i> & lines_2d);

    bool is_intersect_polygon(const cv::Mat & img, const std::vector<cv::Point> & vertices) const ;
    bool is_intersect(const wjl::LineSeg2i & line, double & at_x, double & at_y) const;

    /** distance
     */
    REAL point_distance(const LineSeg2i & line) const;

    static double dist2D_Segment_to_Segment( const LineSeg2i & S1, const LineSeg2i & S2);
    static double min_point_distance(const LineSeg2i & s1, const LineSeg2i & s2);
    //static wjl::LineSeg2i ptsFitLine(const std::vector<cv::Point> & pts);
    static LineSeg2i linesFitLine(const cv::Mat & img, const std::vector<LineSeg2i> &lines);

public:
    //members
    Point2i _start, _end;
};
//std::ostream& operator << (std::ostream& str, const LineSeg2i& line);

class SizeI
{
public:
    SizeI();
    SizeI(int w, int h);

    int width() const;
    int height() const;

    int _w, _h;
};


class Rectangle
{
public:
    enum RelationType{ LEFT , RIGHT, ABOVE, BELOW, NONE};
    Rectangle();
    Rectangle(Point2i pos, int w, int h);
    Rectangle(int x, int y, int w, int h);
    Rectangle(Point2i tl, Point2i dr);
    Rectangle(const cv::Rect& cvrect);
    virtual ~Rectangle();

    Rectangle & operator = (const Rectangle & rect);
    Rectangle& operator &= (const Rectangle& rect);
    Rectangle& operator |= (const Rectangle& rect);
    Rectangle& operator += (const Point2i& p );
    Rectangle& operator -= (const Point2i& p );

    Rectangle operator & (const Rectangle & rect);
    Rectangle operator | (const Rectangle & rect);
    Rectangle operator + (const Point2i & p);
    Rectangle operator - (const Point2i & p);


    bool operator == (const Rectangle & rect);
    bool operator != (const Rectangle & rect);

    /* get info
     */
    Point2i tl() const;
    Point2i br() const;
    Point2i center() const;
    Point2i tr() const;
    Point2i bl() const;
    int x() const;
    int y() const;
    int width() const;
    int height() const;
    int rx() const;
    int by() const;
    int area() const;

    /* set member
     */
    void setX(int x);
    void setY(int y);
    void setW(int w);
    void setH(int h);



    Rectangle shift(int x, int y);

    /* relationship
     */
    bool contains(const Point2i & pt) const;
    bool contains(const Rectangle& rect) const;
    int insect_area(const Rectangle& rect) const;
    bool is_insect(const Rectangle& rect) const;

    /* sort Rectangle vectors
     * by area in increasing order
     */
    static void sort_by_area(std::vector<Rectangle>& rects);

    /* open operation
     */
    Rectangle dilate(int x, int y) const;
    Rectangle erode(int x, int y) const;
    Rectangle resize(int new_w, int new_h) const;
    Rectangle scale(double rate_w, double rate_h) const;

    /* distance
     */
    static int shape_dist2(const Rectangle r1, const Rectangle r2);

    static int min_dist2(const Rectangle r1, const Rectangle r2);

    /* check 2 rectangle's position relations
     * on the left, right, above, below
     */
    RelationType relation(const Rectangle r, const int img_w, const int img_h) const;

    static void getBoundBox(std::vector<cv::Point> points, Rectangle& boundBox);

    /* I O
     */
    void print() const;

    friend std::ostream& operator << (std::ostream& str, Rectangle & rect);

    //cv::Rect _rect;
    //Point2i _topleft;
    //int _x, _y;
    //int _width, _height;
    cv::Rect _cvrect;
};
bool area_greater(Rectangle a, Rectangle b);
}





#endif // PRIMITIVES_H
