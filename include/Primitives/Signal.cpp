#include "Signal.h"
#include <algorithm>
#include <math.h>
#include <assert.h>
#include "../Common/filter.h"


using namespace wjl;

template <typename Tp>
Signal<Tp>::Signal()
{
}

template <typename Tp>
Signal<Tp>::Signal(const size_t num)
{
    this->_vec = std::vector<Tp>(num);
}

template <typename Tp>
Signal<Tp>::Signal(const size_t num, const Tp a)
{
    this->_vec = std::vector<Tp>(num, a);
}

template <typename Tp>
Signal<Tp>::Signal(const std::vector<Tp> &vec)
    : _vec(vec)
{

}

//template <typename Tp>
//Signal<Tp>::Signal(const Signal<Tp> &sig)
//{
//    this->_vec = sig._vec;
//}

//template <typename Tp>
//Signal<Tp>::~Signal()
//{
//    std::cout << "Destuctor Signal" << std::endl;
//}

//template <typename Tp>
//Signal<Tp> & Signal<Tp>::operator = (const Signal<Tp> & sig)
//{
//    this->_vec = sig._vec;
//    return (*this);
//}

template <typename Tp>
Signal<Tp> & Signal<Tp>::operator += (const Signal<Tp> & sig)
{
    size_t min_num = std::min(this->_vec.size(), sig._vec.size());

    for (size_t i = 0; i < min_num; ++i)
        this->_vec[i] += sig._vec[i];
    return (*this);
}

template <typename Tp>
Signal<Tp> & Signal<Tp>::operator -= (const Signal<Tp> & sig)
{
    size_t min_num = std::min(this->_vec.size(), sig._vec.size());

    for (size_t i = 0; i < min_num; ++i)
        this->_vec[i] -= sig._vec[i];
    return (*this);
}

template <typename Tp>
Signal<Tp> & Signal<Tp>::operator *= (const Signal<Tp> & sig)
{
    size_t min_num = std::min(this->_vec.size(), sig._vec.size());

    for (size_t i = 0; i < min_num; ++i)
        this->_vec[i] *= sig._vec[i];
    return (*this);
}

template <typename Tp>
Signal<Tp> & Signal<Tp>::operator /= (const Signal<Tp> & sig)
{
    size_t min_num = std::min(this->_vec.size(), sig._vec.size());

    for (size_t i = 0; i < min_num; ++i)
        this->_vec[i] /= sig._vec[i];
    return (*this);
}

template <typename Tp>
Signal<Tp> & Signal<Tp>::operator += (const Tp a)
{
    size_t num = this->_vec.size();

    for (size_t i = 0; i < num; ++i)
        this->_vec[i] += a;
    return (*this);
}

template <typename Tp>
Signal<Tp> & Signal<Tp>::operator -= (const Tp a)
{
    size_t num = this->_vec.size();

    for (size_t i = 0; i < num; ++i)
        this->_vec[i] -= a;
    return (*this);
}

template <typename Tp>
Signal<Tp> & Signal<Tp>::operator *= (const Tp a)
{
    size_t num = this->_vec.size();

    for (size_t i = 0; i < num; ++i)
        this->_vec[i] *= a;
    return (*this);
}

template <typename Tp>
Signal<Tp> & Signal<Tp>::operator /= (const Tp a)
{
    size_t num = this->_vec.size();

    for (size_t i = 0; i < num; ++i)
        this->_vec[i] /= a;
    return (*this);
}

template <typename Tp>
Signal<Tp> Signal<Tp>::operator + (const Signal<Tp> & sig)
{
    if (this->size() != sig.size())
        std::cerr << "Different Signal size!" << std::endl;
    Signal<Tp> ret(this->size());
    size_t num = this->size();
    for (size_t i = 0; i < num; ++i)
        ret._vec[i] = this->_vec[i] + sig._vec[i];
    return ret;
}

template <typename Tp>
Signal<Tp> Signal<Tp>::operator - (const Signal<Tp> & sig)
{
    if (this->size() != sig.size())
        std::cerr << "Different Signal size!" << std::endl;
    Signal<Tp> ret(this->size());
    size_t num = this->size();
    for (size_t i = 0; i < num; ++i)
        ret._vec[i] = this->_vec[i] - sig._vec[i];
    return ret;
}

template <typename Tp>
Signal<Tp> Signal<Tp>::operator * (const Signal<Tp> & sig)
{
    if (this->size() != sig.size())
        std::cerr << "Different Signal size!" << std::endl;
    Signal<Tp> ret(this->size());
    size_t num = this->size();
    for (size_t i = 0; i < num; ++i)
        ret._vec[i] = this->_vec[i] * sig._vec[i];

    return ret;
}

template <typename Tp>
Signal<Tp> Signal<Tp>::operator / (const Signal<Tp> & sig)
{
    if (this->size() != sig.size())
        std::cerr << "Different Signal size!" << std::endl;
    Signal<Tp> ret(this->size());
    size_t num = this->size();
    for (size_t i = 0; i < num; ++i)
        ret._vec[i] = this->_vec[i] / sig._vec[i];
    return ret;
}

template <typename Tp>
Signal<Tp> Signal<Tp>::operator + (const Tp a)
{
    Signal<Tp> ret(this->size());
    for (size_t i = 0; i < this->size(); ++i)
        ret._vec[i] = this->_vec[i] + a;
    return ret;
}

template <typename Tp>
Signal<Tp> Signal<Tp>::operator - (const Tp a)
{
    Signal<Tp> ret(this->size());
    for (size_t i = 0; i < this->size(); ++i)
        ret._vec[i] = this->_vec[i] - a;
    return ret;
}

template <typename Tp>
Signal<Tp> Signal<Tp>::operator * (const Tp a)
{
    Signal<Tp> ret(this->size());
    for (size_t i = 0; i < this->size(); ++i)
        ret._vec[i] = this->_vec[i] * a;
    return ret;
}

template <typename Tp>
Signal<Tp> Signal<Tp>::operator / (const Tp a)
{
    Signal<Tp> ret(this->size());
    for (size_t i = 0; i < this->size(); ++i)
        ret._vec[i] = this->_vec[i] / a;
    return ret;
}

template <typename Tp>
Tp Signal<Tp>::operator [] (const size_t i) const
{
    assert(i>=0);
    assert(i<this->size());
    return this->_vec[i];
}

template <typename Tp>
Tp& Signal<Tp>::operator [] (const size_t i)
{
    assert(i>=0);
    assert(i<this->size());
    Tp& ret = this->_vec[i];
    return ret;
}

///////////////////////////////////////

template <typename Tp>
Tp Signal<Tp>::at(const size_t i) const
{
    assert(i>=0);
    assert(i<this->size());
    return this->_vec[i];
}
template <typename Tp>
Tp & Signal<Tp>::at(const size_t i)
{
    assert(i >= 0);
    assert( i< this->size());

    Tp& ret = this->_vec[i];
    return ret;
}

template <typename Tp>
void Signal<Tp>::push_back(const Tp a)
{
    this->_vec.push_back(a);
}

template <typename Tp>
void Signal<Tp>::clear()
{
    this->_vec.clear();
}

template <typename Tp>
Signal<Tp> Signal<Tp>::addWeighted(const Signal<Tp>& sig1, const REAL w1, const Signal<Tp>& sig2, const REAL w2)
{
    assert(sig1.size() == sig2.size());
    Signal<Tp> ret(sig1.size());
    for (size_t i = 0; i < sig1.size(); ++i)
    {
        REAL sum = static_cast<REAL>(sig1[i]) * w1 + static_cast<REAL>(sig2[i]) * w2;
        ret[i] = static_cast<Tp>(sum);
    }
    return ret;
}
template <typename Tp>
Signal<Tp> Signal<Tp>::addWeightedUnsigned(const Signal<Tp>& sig1, const REAL w1, const Signal<Tp>& sig2, const REAL w2)
{
    assert(sig1.size() == sig2.size());
    Signal<Tp> ret(sig1.size());
    for (size_t i = 0; i < sig1.size(); ++i)
    {
        REAL sum = static_cast<REAL>(sig1[i]) * w1 + static_cast<REAL>(sig2[i]) * w2;
        sum = (sum > 0 ? sum: 0);
        ret[i] = static_cast<Tp>(sum);
    }
    return ret;
}

template <typename Tp>
size_t Signal<Tp>::size() const
{
    return this->_vec.size();
}

template <typename Tp>
Tp Signal<Tp>::min_value() const
{
    Tp min = *(std::min_element(this->_vec.begin(), this->_vec.end()));
    return min;
}

template <typename Tp>
Tp Signal<Tp>::max_value() const
{
    Tp max = *(std::max_element(this->_vec.begin(), this->_vec.end()));
    return max;
}

template <typename Tp>
Tp Signal<Tp>::min_value(size_t lid, size_t rid) const
{
    assert(lid <= rid);

    Tp max = *(std::max_element(this->_vec.begin()+lid, this->_vec.begin()+rid));
    return max;
}

template <typename Tp>
Tp Signal<Tp>::max_value(size_t lid, size_t rid) const
{
    assert(lid <= rid);

    Tp min = *(std::min_element(this->_vec.begin()+lid, this->_vec.begin()+rid));
    return min;
}

template <typename Tp>
size_t Signal<Tp>::min_index() const
{
    size_t id = std::min_element(this->_vec.begin(), this->_vec.end()) - this->_vec.begin();
    return id;
}

template <typename Tp>
size_t Signal<Tp>::max_index() const
{
    size_t id = std::max_element(this->_vec.begin(), this->_vec.end()) - this->_vec.begin();
    return id;
}

template <typename Tp>
size_t Signal<Tp>::min_index(size_t lid, size_t rid) const
{
    assert(lid <= rid);
    size_t id = std::min_element(this->_vec.begin()+lid, this->_vec.begin()+rid) - this->_vec.begin();
    return id;
}

template <typename Tp>
size_t Signal<Tp>::max_index(size_t lid, size_t rid) const
{
    assert(lid <= rid);
    size_t id = std::max_element(this->_vec.begin()+lid, this->_vec.begin()+rid) - this->_vec.begin();
    return id;
}

template <typename Tp>
Tp Signal<Tp>::sum() const
{
    Tp sum = static_cast<Tp>(0);

    for (size_t i = 0; i < this->_vec.size(); ++i)
        sum += this->_vec[i];
    return sum;
}

template <typename Tp>
REAL Signal<Tp>::stdev() const
{
    REAL ret = 0.0;

    REAL mean = this->mean();
    size_t num = this->size();
    for (size_t i = 0; i < num; ++i)
    {
        REAL diff = static_cast<REAL>(this->at(i)) - mean;
        ret += diff * diff;
    }

    ret = sqrt(ret / (REAL)num);
    return ret;
}

template <typename Tp>
REAL Signal<Tp>::mean() const
{
    if (this->size() == 0)
        return 0.0;

    REAL sum = static_cast<REAL>(this->sum());

    return (sum / (REAL)this->size());
}

template <typename Tp>
Tp Signal<Tp>::norm2() const
{
    REAL ret = 0.0;
    for (size_t i = 0; i < this->size(); ++i)
    {
        ret += this->_vec[i] * this->_vec[i];
    }
    return ret;
}

template <typename Tp>
REAL Signal<Tp>::norm() const
{
    REAL ret = static_cast<REAL>(this->norm2());
    return sqrt(ret);
}

template <typename Tp>
Tp Signal<Tp>::dot(Signal<Tp>& s) const
{
    if (this->size() != s.size())
    {
        std::cerr << "Signal sizes are not same!" << std::endl;
        return -1;
    }

    size_t size = this->size();
    Tp ret = static_cast<Tp>(0);
    for (size_t i = 0; i < size; ++i)
        ret += this->_vec[i] * s._vec[i];
    return ret;
}

template <typename Tp>
Tp Signal<Tp>::dot_loop(Signal<Tp>& s) const
{
    Tp max_sum = static_cast<Tp>(0);

    if (this->size() >= s.size())
    {
        //double this profile & dot p
        //find the maximum dot value & return
        Signal<Tp> this2 = this->extend(2);
        size_t start_last = 2 * this->size() - s.size();
        for (size_t start = 0; start < start_last; ++start)
        {
            Tp sum = static_cast<Tp>(0);
            for (size_t i = 0; i < s.size(); ++i)
                sum += this2.at(start+i) * s.at(i);
            if (sum > max_sum)
                max_sum = sum;
        }

        return max_sum;
    }
    else
    {
        Signal<Tp> s2 = s.extend(2);
        size_t start_last = 2 * s.size() - this->size();

        for (size_t start = 0; start < start_last; ++start)
        {
            Tp sum = static_cast<Tp>(0);
            for (size_t i = 0; i < this->size(); ++i)
                sum += this->at(i) * s2.at(i);
            if (sum > max_sum)
                max_sum = sum;
        }
        return max_sum;
    }
}

template <typename Tp>
REAL Signal<Tp>::dist(Signal<Tp> &s) const
{
    if (this->size() != s.size())
    {
        std::cerr << "Signal sizes are not same!" << std::endl;
        return 65536;
    }

    REAL ret = 0.0;

    for (size_t i = 0; i < this->size(); ++i)
    {
        REAL diff = this->_vec[i] - s._vec[i];
        ret += diff * diff;
    }
    return ret;
}

template <typename Tp>
REAL Signal<Tp>::dist_loop(Signal<Tp> &s) const
{
    Tp min_sum = static_cast<Tp>(65536);

    if (this->size() >= s.size())
    {
        //double this profile & dot p
        //find the maximum dot value & return
        Signal<Tp> this2 = this->extend(2);
        size_t start_last = 2 * this->size() - s.size();
        for (size_t start = 0; start < start_last; ++start)
        {
            Tp sum = static_cast<Tp>(0);
            for (size_t i = 0; i < s.size(); ++i)
            {
                Tp diff = this2.at(start+i) - s.at(i);
                sum += diff * diff;
            }
            if (sum < min_sum)
                min_sum = sum;
        }

        return min_sum;
    }
    else
    {
        Signal<Tp> s2 = s.extend(2);
        size_t start_last = 2 * s.size() - this->size();

        for (size_t start = 0; start < start_last; ++start)
        {
            Tp sum = static_cast<Tp>(0);
            for (size_t i = 0; i < this->size(); ++i)
            {
                Tp diff = this->at(i) - s2.at(i);
                sum += diff * diff;
            }
            if (sum < min_sum)
                min_sum = sum;
        }
        return min_sum;
    }
}

//dist := (this^2 + sig^2) / (this dot_loop sig)
template <typename Tp>
REAL Signal<Tp>::dist_sum_over_dot(Signal<Tp>& sig) const
{
    REAL dot_v = this->dot_loop(sig);
    REAL this_2 = this->norm2();
    REAL sig_2 = sig.norm2();

    if (this_2 < EPSILON)
        this_2 += EPSILON;
    if (sig_2 < EPSILON)
        sig_2 += EPSILON;

    REAL ret = dot_v * 2.0 / (this_2 + sig_2);

    return (1.0 / ret);
}

//dist := consin angle,
template <typename Tp>
REAL Signal<Tp>::dist_angle(Signal<Tp>& sig) const
{
    REAL dot_v = this->dot(sig);
    REAL this_norm = this->norm();
    REAL sig_norm = sig.norm();
    if (this_norm < EPSILON)
        this_norm += EPSILON;
    if (sig_norm < EPSILON)
        sig_norm += EPSILON;

    REAL ret = dot_v / (this_norm * sig_norm);
    return ret;
}

template <typename Tp>
Signal<Tp> Signal<Tp>::extend(size_t times) const
{
    Signal<Tp> ret;
    for (size_t t = 0; t < times; ++t)
        for (size_t i = 0; i < this->size(); ++i)
            ret.push_back(this->at(i));
    return ret;
}

template <typename Tp>
void Signal<Tp>::print() const
{
    for (size_t i = 0; i < this->size(); ++i)
        std::cout <<this->at(i) << " ";
    std::cout << "\nsignal length: " << this->size() << std::endl;
}

///////////////////// ********** filter **********************///////////////

template <typename Tp>
void Signal<Tp>::filter(const Signal<Tp>& src, const std::vector<REAL>& filt, Signal<Tp>& response)
{
    size_t half_filt_size = filt.size() / 2;
    response = Signal<Tp>(src.size());
    for (size_t i = 0; i < src.size(); ++i)
    {
        if (i < half_filt_size)
            response[i] = src.at(i);
        else if (i > src.size() - half_filt_size - 1)
            response[i] = src[i];
        else
        {
            REAL sum = 0.;

            for (size_t k = 0, src_id = i - half_filt_size; k < filt.size(); ++k, ++src_id)
            {
                sum += static_cast<REAL>(src[src_id]) * filt[k];
            }
            response[i] = static_cast<Tp>(sum);
        }
    }
}

template <typename Tp>
void Signal<Tp>::filter(const Signal<Tp>& src, const Signal<REAL>& filt, Signal<Tp>& response)
{
    Signal<Tp>::filter(src, filt.toStdVec(), response);
}

template <typename Tp>
void Signal<Tp>::filter_half(const Signal<Tp>& src, const std::vector<REAL>& mask, Signal<Tp>& response)
{
    size_t len = mask.size();
    size_t half = len -1;
    for (size_t i = 0; i < src.size(); ++i)
    {
        if (i < half)
            response[i] = src[i];
        else if (i > src.size() - half - 1)
            response[i] = src[i];
        else
        {
            REAL sum = 0.;
            for (size_t k = 0; k < len; ++k)
                sum += mask[k] * static_cast<REAL>(src[i-k]);
            for (size_t k = 1; k < len; ++k)
                sum += mask[k] * static_cast<REAL>(src[i+k]);
            response[i] = static_cast<Tp>(sum);
        }
    }
}

template <typename Tp>
void Signal<Tp>::fgauss(const Signal<Tp>& src, REAL sigma, int width, Signal<Tp>& response)
{
    std::vector<REAL> mask = make_fgauss(sigma, width);
//    for (size_t i = 0; i < mask.size(); ++i)
//        std::cout << "mask " << i << ", " << mask[i] << "\t";

    Signal<Tp>::filter_half(src, mask, response);
}

template <typename Tp>
Signal<REAL> Signal<Tp>::to_histogram(const size_t n_bins)
{
    REAL min_v = static_cast<REAL>(this->min_value());
    REAL max_v = static_cast<REAL>(this->max_value());
    std::cout << "Signal [" << min_v <<","<<max_v<<"]" << std::endl;

    Signal<REAL> hist(n_bins);
    hist = to_histogram(min_v, max_v, n_bins);
    return hist;
}

template <typename Tp>
Signal<REAL> Signal<Tp>::to_histogram(const Tp & minv, const Tp & maxv, const size_t n_bins)
{
    Signal<REAL> hist(n_bins, 0.0);

    REAL min_v = static_cast<REAL>(minv);
    REAL max_v = static_cast<REAL>(maxv);

    REAL interval = (max_v - min_v) / static_cast<REAL>(n_bins);
    min_v = min_v - 0.5 * interval;
    max_v = max_v + 0.5 * interval;

    for (size_t i = 0; i < this->size(); ++i)
    {
        int idx = ((REAL)this->at(i) - min_v) / interval;
        if (idx >= 0 && idx < n_bins)
            hist[idx] += 1.0;
    }

    for (size_t i = 0; i < n_bins; ++i)
    {
        hist[i] /= (REAL)(this->size());
        //std::cout << "hist " << i << ", = " << hist[i] << std::endl;
    }
    REAL sum = hist.sum();
    std::cout << "hist sum " << sum << std::endl;
    return hist;
}

template <typename Tp>
std::vector<std::vector<Tp> > Signal<Tp>::to_histogram_vecs(const size_t n_bins)
{
    REAL min_v = (REAL)this->min_value() - 0.5;
    REAL max_v = (REAL)this->max_value() + 0.5;
    REAL interval = (max_v - min_v) / static_cast<REAL>(n_bins);

    std::vector<std::vector<Tp> > ret;
    for (size_t i = 0; i < n_bins; ++i)
    {
        std::vector<Tp> vec;
        ret.push_back(vec);

    }

    for (size_t i = 0; i < this->size(); ++i)
    {
        int idx = ((REAL)this->at(i) - min_v) / interval;
        size_t idx_u = (size_t)idx;
        ret.at(idx_u).push_back(this->at(i));
    }
    return ret;
}

//template <typename Tp>
//std::vector<Tp>& Signal<Tp>::stdVec()
//{
//    return this->_vec;
//}

//////////////////////********* Local max & min *****************///////////////

template <typename Tp>
std::vector<size_t> Signal<Tp>::local_strict_maxIds(const Tp & thresh)
{
    std::vector<size_t> ret;
    if (this->size() == 0)
        return ret;
    else if (this->size() <= 2)
    {
        ret.push_back(this->max_index());
        return ret;
    }

    for (size_t i = 1; i < this->size()-1; ++i)
    {
        if (_vec[i] < thresh)
            continue;
        REAL diff_pre = (REAL)this->_vec[i] - (REAL)this->_vec[i-1];
        REAL diff_cur = (REAL)this->_vec[i+1] - (REAL)this->_vec[i];
        if (diff_pre >0 && diff_cur < 0)
            ret.push_back(i);
    }
    return ret;
}

template <typename Tp>
std::vector<size_t> Signal<Tp>::local_strict_maxIds(const size_t window_size, const Tp & thresh)
{
    assert(window_size % 2 == 1); //window_size should be odd
    std::vector<size_t> ret;
    size_t half = window_size / 2;
    if (half == 0)
    {
        ret = this->local_strict_maxIds(thresh);
        return ret;
    }
    if (window_size >= this->size())
    {
        size_t maxid = this->max_index();
        ret.push_back(maxid);
        return ret;
    }
    size_t startid = half * 2;
    size_t endid = this->size()-1 - half*2;
    //std::cout << "start from " << startid << ", to " << endid <<std::endl;
    for (size_t i = startid; i <= endid; ++i)
    {
        if (_vec[i] < thresh)
            continue;
        REAL diff_pre = (REAL)this->_vec[i] - (REAL)this->_vec[i-1];
        REAL diff_cur = (REAL)this->_vec[i+1] - (REAL)this->_vec[i];
        if (diff_pre > 0 && diff_cur < 0)  //local max
        {
            size_t left = i - half;
            size_t right = i + half;
            bool isincrease = true;
            bool leftvalid = false;
            //std::cout << "gradient valid " << i << std::endl;
            //check left if is all increase
            for (size_t k = left; k < i; ++k)
            {
                REAL d_pre = (REAL)this->_vec[k] - (REAL)this->_vec[k-1];
                REAL d_cur = (REAL)this->_vec[k+1] - (REAL)this->_vec[k];
                if(d_pre >= 0 && d_cur >= 0)
                   ;
                else
                   isincrease = false;
            }
            if (isincrease)
            {
                leftvalid = true;
                std::cout << "left valid " << i << std::endl;
            }

            //check right if is all decrease
            bool isdecrease = true;
            bool rightvalid = false;
            for (size_t k = i+1; k <= right; ++k)
            {
                REAL d_pre = (REAL)this->_vec[k] - (REAL)this->_vec[k-1];
                REAL d_cur = (REAL)this->_vec[k+1] - (REAL)this->_vec[k];
                if (d_pre <= 0 && d_cur <= 0)
                    ;
                else
                    isdecrease = false;
            }
            if (isdecrease)
            {
                rightvalid = true;
                std::cout << "right valid " << i << std::endl;
            }
            if (leftvalid & rightvalid)
                ret.push_back(i);
        }
    }
    return ret;
}


template <typename Tp>
std::vector<size_t> Signal<Tp>::local_strict_minIds(const Tp & thresh)
{
    Tp maxv = this->max_value();
    Signal<Tp> complement = maxv - (*this);
    std::vector<size_t> ret = complement.local_strict_maxIds(maxv-thresh);
    return ret;
}

template <typename Tp>
std::vector<size_t> Signal<Tp>::local_strict_minIds(const size_t window_size, const Tp & thresh)
{
    Tp maxv = this->max_value();
    Signal<Tp> complement = maxv - (*this);
    std::vector<size_t> ret = complement.local_strict_maxIds(window_size, maxv-thresh);
    return ret;
}

template <typename Tp>
std::vector<size_t> Signal<Tp>::local_maxIds(const size_t window_size, const Tp & thresh)
{
    assert(window_size % 2 == 1); //window_size should be odd
    std::vector<size_t> ret;
    size_t half = window_size / 2;
    if (half == 0)
    {
        ret = this->local_strict_maxIds(thresh);
        return ret;
    }
    if (window_size >= this->size())
    {
        size_t maxid = this->max_index();
        ret.push_back(maxid);
        return ret;
    }
    size_t startid = half * 2;
    size_t endid = this->size()-1 - half*2;

    for (size_t i = startid; i <= endid; ++i)
    {
        if (_vec[i] < thresh)
            continue;
        size_t left = i - half;
        size_t right = i + half;
        if (this->max_index(left,right) == i)
        {
            ret.push_back(i);
//            for (size_t k = left; k <= right; ++k)
//                std::cout << k <<"("<<this->_vec[k]<<")\t";
//            std::cout << "local max id " << i << std::endl;
        }
    }
    return ret;
}

template <typename Tp>
std::vector<size_t> Signal<Tp>::local_minIds(const size_t window_size, const Tp & thresh)
{
    Tp maxv = this->max_value();
    Signal<Tp> complement = maxv - (*this);
    std::vector<size_t> ret = complement.local_maxIds(window_size, maxv - thresh);
    return ret;
}


template <typename Tp>
void Signal<Tp>::toInt(Signal<Tp>& src, Signal<int>& dst)
{
    dst.clear();
    for (size_t i = 0; i < src.size(); ++i)
    {
        int v = (int)src._vec[i];
        dst.push_back(v);
    }
}

template <typename Tp>
void Signal<Tp>::toReal(Signal<Tp>& src, Signal<REAL>& dst)
{
    dst.clear();
    for (size_t i = 0; i < src.size(); ++i)
    {
        REAL v = (REAL)src._vec[i];
        dst.push_back(v);
    }
}

template <typename Tp>
Signal<int> Signal<Tp>::toInt()
{
    Signal<int> ret;
    for (size_t i = 0; i < this->size(); ++i)
        ret.push_back((int)_vec[i]);
    return ret;
}

template <typename Tp>
Signal<REAL> Signal<Tp>::toReal()
{
    Signal<REAL> ret;
    for (size_t i = 0; i < this->size(); ++i)
        ret.push_back((REAL)_vec[i]);
    return ret;
}
template <typename Tp>
const std::vector<Tp> Signal<Tp>::toStdVec() const
{
    return _vec;
}

template <typename Tp>
std::vector<Tp> & Signal<Tp>::toStdVec()
{
    return _vec;
}

template class Signal<size_t>;
template class Signal<unsigned char>;
template class Signal<int>;
template class Signal<float>;
template class Signal<double>;

