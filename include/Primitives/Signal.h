#ifndef SIGNAL_H
#define SIGNAL_H

#include <iostream>
#include <vector>
#include "../Common/global.h"

namespace wjl{

#define EPSILON 0.00001

template<typename Tp>
class Signal
{
public:


    Signal();
    Signal(const size_t num);
    Signal(const size_t num, const Tp a);
    Signal(const std::vector<Tp>& vec);
    //Signal(const Signal& sig);
    virtual ~Signal(){}

    //Signal<Tp> & operator = (const Signal<Tp> & sig);

    Signal<Tp> & operator += (const Signal<Tp> & sig);
    Signal<Tp> & operator -= (const Signal<Tp> & sig);
    Signal<Tp> & operator *= (const Signal<Tp> & sig);
    Signal<Tp> & operator /= (const Signal<Tp> & sig);

    Signal<Tp> & operator += (const Tp a);
    Signal<Tp> & operator -= (const Tp a);
    Signal<Tp> & operator *= (const Tp a);
    Signal<Tp> & operator /= (const Tp a);

    Signal<Tp> operator + (const Signal<Tp> & sig);
    Signal<Tp> operator - (const Signal<Tp> & sig);
    Signal<Tp> operator * (const Signal<Tp> & sig);
    Signal<Tp> operator / (const Signal<Tp> & sig);

    Signal<Tp> operator + (const Tp a);
    Signal<Tp> operator - (const Tp a);
    Signal<Tp> operator * (const Tp a);
    Signal<Tp> operator / (const Tp a);


    Tp operator [] (const size_t i ) const;
    Tp & operator [] (const size_t i);
    Tp at(const size_t i) const;
    Tp & at(const size_t i);

    void push_back(const Tp a);
    void clear();
    static Signal<Tp> addWeighted(const Signal<Tp>& sig1, const REAL w1, const Signal<Tp>& sig2, const REAL w2);
    static Signal<Tp> addWeightedUnsigned(const Signal<Tp> &sig1, const REAL w1, const Signal<Tp> &sig2, const REAL w2);
    /* get signal info
     */

    size_t size() const;
    Tp min_value() const;
    Tp max_value() const;

    Tp min_value(size_t lid, size_t rid) const;
    Tp max_value(size_t lid, size_t rid) const;

    size_t min_index() const;
    size_t max_index() const;

    size_t min_index(size_t lid, size_t rid) const;
    size_t max_index(size_t lid, size_t rid) const;

    Tp sum() const;
    Tp norm2() const;
    REAL norm() const;

    /* staticstic value
     */
    REAL stdev() const;
    REAL mean() const;

    Tp dot(Signal<Tp> & sig) const;
    Tp dot_loop(Signal<Tp>& sig) const;

    /* distance */
    REAL dist(Signal<Tp>& s) const;
    REAL dist_loop(Signal<Tp> &s) const;

    REAL dist_sum_over_dot(Signal<Tp>& s) const;
    REAL dist_angle(Signal<Tp>& s) const;

    Signal<Tp> extend(size_t times) const;

    /* filter
     */
    static void filter(const Signal<Tp>& src, const Signal<REAL>& filt, Signal<Tp>& response);
    static void filter(const Signal<Tp>& src, const std::vector<REAL>& filt, Signal<Tp>& response);
    static void filter_half(const Signal<Tp>& src, const std::vector<REAL>& mask, Signal<Tp>& response);
    static void fgauss(const Signal<Tp>& src, REAL sigma, int width, Signal<Tp>& response);


    /* histogram
     */
    Signal<REAL> to_histogram(const size_t n_bins);
    Signal<REAL> to_histogram(const Tp & minv, const Tp & maxv, const size_t n_bins);

    std::vector<std::vector<Tp> > to_histogram_vecs(const size_t n_bins);

    void print() const;

    /* find local max */
    std::vector<size_t> local_strict_maxIds(const Tp & thresh);
    std::vector<size_t> local_strict_maxIds(const size_t window_size, const Tp & thresh);
    std::vector<size_t> local_strict_minIds(const Tp & thresh);
    std::vector<size_t> local_strict_minIds(const size_t window_size, const Tp & thresh);

    std::vector<size_t> local_maxIds(const size_t window_size, const Tp & thresh);
    std::vector<size_t> local_minIds(const size_t window_size, const Tp & thresh);

    /* type switch */
    static void toInt(Signal<Tp>& src, Signal<int>& dst);
    static void toReal(Signal<Tp>& src, Signal<REAL>& dst);

    Signal<int> toInt();
    Signal<REAL> toReal();
    const std::vector<Tp> toStdVec() const;
    std::vector<Tp> & toStdVec();

    friend std::ostream & operator << (std::ostream & os, const Signal<Tp> & sig)
    {
        os << sig.size() << "\n";
        for (size_t i = 0; i < sig.size(); ++i)
            os << i << "("<<sig[i] << ") ";
        os << "\n";
        return os;
    }

    //member
private:
    std::vector<Tp> _vec;
};

template<typename Tp>
Signal<Tp> operator - (const Tp a, const Signal<Tp>& sig)
{
    Signal<Tp> ret(sig.size());
    for (size_t i = 0; i < sig.size(); ++i)
        ret[i] = a - sig[i];
    return ret;
}

}

#endif // SIGNAL_H
