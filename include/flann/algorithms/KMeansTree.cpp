/*
 * KMeansTree.cpp
 *
 *  Created on: 2009-8-18
 *      Author: fangtian
 */
#include "KMeansTree.h"

#include "constants.h"
#include "util/common.h"
#include "util/Heap.h"
#include "util/Allocator.h"
#include "util/Dataset.h"
#include "util/ResultSet.h"
#include "util/Random.h"
#include "NNIndex.h"
#include "util/Timer.h"

#include <fstream>
#include <queue>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <string>
#include <vector>
#include <set>
//#ifdef _MSC_VER
// #include <float.h>  // for _isnan() on VC++
// #define isnan(x) _isnan(x)  // VC++ uses _isnan() instead of isnan()
//#else
//    #include <math.h>  // for isnan() everywhere else
//#endif
#ifdef __GNUC__
    #define _isnan(x) std::isnan(x)  //
#else
// ... handle this for other compilers
#endif
//#define DEBUGCLUSTERING
//#ifdef __cplusplus
//extern "C" {
//#endif
void chooseCentersRandom(int k, Dataset<float>& vecs, int* indices, int indices_length, float** centers, int& centers_length)
{
    UniqueRandom r(indices_length);

    int index;
    for (index=0;index<k;++index) {
        bool duplicate = true;
        int rnd;
        while (duplicate) {
            duplicate = false;
            rnd = r.next();
            if (rnd<0) {
                centers_length = index;
                return;
            }

            centers[index] = vecs[indices[rnd]];

            for (int j=0;j<index;++j) {
                float sq = flann_dist(centers[index],centers[index]+vecs.cols,centers[j]);
                if (sq<1e-16) {
                    duplicate = true;
                }
            }
        }
    }

    centers_length = index;
};

void chooseCentersGonzales(int k, Dataset<float>& vecs, int* indices, int indices_length, float** centers, int& centers_length)
{
    int n = indices_length;


    int rnd = rand_int(n);
    assert(rnd >=0 && rnd < n);

    centers[0] = vecs[indices[rnd]];

    int index;
    for (index=1; index<k; ++index) {

        int best_index = -1;
        float best_val = 0;
        for (int j=0;j<n;++j) {
            float dist = flann_dist(centers[0],centers[0]+vecs.cols,vecs[indices[j]]);
            for (int i=1;i<index;++i) {
                    float tmp_dist = flann_dist(centers[i],centers[i]+vecs.cols,vecs[indices[j]]);
                if (tmp_dist<dist) {
                    dist = tmp_dist;
                }
            }
            if (dist>best_val) {
                best_val = dist;
                best_index = j;
            }
        }
        if (best_index!=-1) {
            centers[index] = vecs[indices[best_index]];
        }
        else {
            break;
        }
    }
    centers_length = index;
};

void chooseCentersKMeanspp(int k, Dataset<float>& vecs, int* indices, int indices_length, float** centers, int& centers_length)
{
    int n = indices_length;

    double currentPot = 0;
    double* closestDistSq = new double[n];

    std::set<int> selectedIndices;

    // Choose one random center and set the closestDistSq values
    int index = rand_int(n);

    assert(index >=0 && index < n);
    centers[0] = vecs[indices[index]];
    selectedIndices.insert(index);

    for (int i = 0; i < n; i++) {
        closestDistSq[i] = flann_dist(vecs[indices[i]], vecs[indices[i]] + vecs.cols, vecs[indices[index]]);
#ifdef DEBUGCLUSTERING
        if (_isnan(closestDistSq[i]))
        {
        	cerr << "Nan when initialzing centers"<< i << "...!\n";
        	for (int j = 0; j < vecs.cols; ++j)
        	{
        		cerr << vecs[indices[i]][j] << " ";
        	}
        	cerr << "\n";
        }
#endif
        currentPot += closestDistSq[i];

    }


    const int numLocalTries = 3;

    // Choose each center
    int centerCount;
    for (centerCount = 1; centerCount < k; centerCount++) {

        // Repeat several trials
        double bestNewPot = -1;
        int bestNewIndex = -1;
        bool foundNewIndex = false;
        for (int localTrial = 0; localTrial < numLocalTries && !foundNewIndex; localTrial++)
        {

            // Choose our center - have to be slightly careful to return a valid answer even accounting
            // for possible rounding errors
        	double randVal = rand_double(currentPot);
            for (index = 0; index < n-1; index++) {
                if (randVal <= closestDistSq[index])
                    break;
                else
                    randVal -= closestDistSq[index];
            }

            // Compute the new potential
            double newPot = 0;
            for (int i = 0; i < n; i++)
                newPot += min( flann_dist(vecs[indices[i]], vecs[indices[i]] + vecs.cols, vecs[indices[index]]), closestDistSq[i] );

            // Store the best result
            if (bestNewPot < 0 || newPot < bestNewPot)
            {
            	std::set<int>::iterator foundIter = selectedIndices.find(index);
            	if (foundIter == selectedIndices.end())
            	{
            		bestNewPot = newPot;
            		bestNewIndex = index;
            		foundNewIndex = true;
            		selectedIndices.insert(index);
            	}
            }
        }

        // Add the appropriate center
        if (foundNewIndex)
        {
        	centers[selectedIndices.size()-1] = vecs[indices[bestNewIndex]];
        	currentPot = bestNewPot;
        	for (int i = 0; i < n; i++)
        		closestDistSq[i] = min( flann_dist(vecs[indices[i]], vecs[indices[i]]+vecs.cols, vecs[indices[bestNewIndex]]), closestDistSq[i] );
        }
    }

   // centers_length = centerCount;
    centers_length = selectedIndices.size();

	delete[] closestDistSq;
};

//#ifdef __cplusplus
//}
//#endif

//namespace {
//
//
//    /**
//    * Associative array with functions to use for choosing the cluster centers.
//    */
//    map<flann_centers_init_t,centersAlgFunction> centerAlgs;
//    /**
//    * Static initializer. Performs initialization befor the program starts.
//    */
//
//
//
//    struct Init {
//        Init() { centers_init(); }
//    };
//    Init __init;
//}


KMeansTree::KMeansTree(Dataset<float>& inputData, Params params)
: cachedNodeNum(-1)
, cachedTreeDepth(-1)
, cachedLeavesNodeNum(-1)
, center_initStorage(-1)
, isForClassificationOnly(false)
, branching(0)
, max_iter(0)
, cb_index(0)
, dataset(inputData)
, size_(0)
, veclen_(0)
, heap(NULL)
, root(NULL)
, indices(NULL)
, memoryCounter(0)
, domain_distances(NULL)
, pDummyDataSet(NULL)
{
	centers_init();
	memoryCounter = 0;

    size_ = dataset.rows;
    veclen_ = dataset.cols;

	// get algorithm parameters
	branching = (int)params["branching"];
	if (branching<2) {
		throw FLANNException("Branching factor must be at least 2");
	}
	int iterations = (int)params["max-iterations"];
	if (iterations<0) {
		iterations =  numeric_limits<int>::max();
	}
	max_iter = iterations;

	flann_centers_init_t centersInit = (flann_centers_init_t)(int)params["centers-init"];
	center_initStorage = static_cast<int>(centersInit);
	if ( centerAlgs.find(centersInit) != centerAlgs.end() ) {
		chooseCenters = centerAlgs[centersInit];
	}
	else {
		throw FLANNException("Unknown algorithm for choosing initial centers.");
	}
    cb_index = 0.4;

	domain_distances = new float[branching];
		heap = new Heap<BranchSt>(dataset.rows);
}

KMeansTree::KMeansTree(std::ifstream & inputStream)
: cachedNodeNum(-1)
, cachedTreeDepth(-1)
, cachedLeavesNodeNum(-1)
, center_initStorage(-1)
, isForClassificationOnly(true)
, branching(0)
, max_iter(0)
, cb_index(0)
, dataset(*(new Dataset<float>(1, 1)))
, size_(0)
, veclen_(0)
, heap(NULL)
, root(NULL)
, indices(NULL)
, memoryCounter(0)
, domain_distances(NULL)
, pDummyDataSet(NULL)
{
	centers_init();
	pDummyDataSet = &dataset;
	LoadFromFile(inputStream);
	domain_distances = new float[branching];
	heap = new Heap<BranchSt>(size_);
}

KMeansTree::KMeansTree(std::ifstream & inputStream, Dataset<float>& inputData)
: cachedNodeNum(-1)
, cachedTreeDepth(-1)
, cachedLeavesNodeNum(-1)
, center_initStorage(-1)
, isForClassificationOnly(false)
, branching(0)
, max_iter(0)
, cb_index(0)
, dataset(inputData)
, size_(0)
, veclen_(0)
, heap(NULL)
, root(NULL)
, indices(NULL)
, memoryCounter(0)
, domain_distances(NULL)
, pDummyDataSet(NULL)
{
	centers_init();
	LoadFromFile(inputStream);
	domain_distances = new float[branching];
	heap = new Heap<BranchSt>(size_);
};

static bool isEmptyString(const std::string & str)
{
	for (size_t i = 0; i < str.length(); ++i)
	{
		if (!isspace(str[i]))
			return false;
	}
	return true;
}

template < class charType >
static void SkipBlankSpace(std::basic_istream<charType> & inputStream)
{
	while (isspace(inputStream.peek()))
	{
		inputStream.get();
	}
};

void KMeansTree::LoadFromFile(std::ifstream  & inputStream)
{
	inputStream >> size_ >> veclen_ >> branching
				>> max_iter >> center_initStorage >> cb_index >> isForClassificationOnly;

	flann_centers_init_t centersInit = (flann_centers_init_t)center_initStorage;
	center_initStorage = static_cast<int>(centersInit);
	if ( centerAlgs.find(centersInit) != centerAlgs.end() ) {
		chooseCenters = centerAlgs[centersInit];
	}
	else {
		throw FLANNException("Unknown algorithm for choosing initial centers.");
	}

//	if (isForClassificationOnly)
//	{
//		dataset.SetDimension(size_, veclen_, NULL);
//	}

	std::string tmpString;
	KMeansNode pNode = NULL;
	int readNodeNum = 0;
	std::queue<KMeansNode> nodesQueue;
	int currentChildNum = 0;

	while (!inputStream.eof() && !inputStream.bad())
	{
		std::getline(inputStream, tmpString);
		if (tmpString.empty())
			continue;


		std::stringstream tmpStrString;
		tmpStrString << tmpString;
		std::string token;
		tmpStrString >> token;

		if (token == "Node")
		{

			int nodeID = 0;
			tmpStrString >> nodeID;
			// it is a node begin
			pNode = pool.allocate<KMeansNodeSt>();

			if (readNodeNum%1031 == 0)
			{
				printf("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\
\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\
%d nodes is read.", readNodeNum);
			}

			if (pNode == NULL)
			{
				cerr << "NULL pNode!!!\n";
			}

			pNode->ID = nodeID;

			KMeansNode pFrontNode = NULL;
			if (!nodesQueue.empty())
			{
//				cerr << "setting child clusters!!!\n";
				pFrontNode = nodesQueue.front();
				pFrontNode->childs[currentChildNum] = pNode;
				currentChildNum += 1;
//				cerr << "about to updating new front clusters!!!\n";
				if (currentChildNum == branching)
				{
//					cerr << "updating new front clusters!!!\n";
					currentChildNum = 0;
					bool foundNextNonLeaveCluster = false;
					nodesQueue.pop();
					while (!nodesQueue.empty() && !foundNextNonLeaveCluster)
					{
						KMeansNode pNewFront = nodesQueue.front();
						foundNextNonLeaveCluster = (NULL != pNewFront->childs);
						if (!foundNextNonLeaveCluster)
							nodesQueue.pop();
//						cerr << "skipping leave clusters!!!\n";
					}
				}
				nodesQueue.push(pNode);

			}
			else
			{
				root = pNode;
				nodesQueue.push(pNode);
			}

			int childNum = 0;
			inputStream >> pNode->level >> pNode->mean_radius
			 >> pNode->radius >> pNode->variance >> pNode->size >> childNum;
			SkipBlankSpace(inputStream);

			pNode->pivot = new float[veclen_];
			// Skip the node pivot line
			std::getline(inputStream, tmpString);

			while(isEmptyString(tmpString))
			{
				std::getline(inputStream, tmpString);
			}
			for (int pi = 0; pi < veclen_; ++pi)
			{
				inputStream >> pNode->pivot[pi];
			}
//			cerr << " Pivit read!!!\n" ;

			// Try to get the leaves if any
			std::getline(inputStream, tmpString);
			while(isEmptyString(tmpString))
			{
				std::getline(inputStream, tmpString);
			}
			std::stringstream indicesStream;
			indicesStream << tmpString;
			std::string indicesToken;
			indicesStream >> indicesToken;
			int indicesNum = 0;
			if (indicesToken == "NodeIndices:")
			{
				indicesStream >> indicesNum;
			}
			else
			{
				cerr << "\nKMeans Tree file error when reading indices!!!\n";
			}

//			if (!isForClassificationOnly)
//			{
				if (indicesNum > 0
						&& indicesNum == pNode->size)
				{
					pNode->indices = new int[indicesNum];
//					pNode->childs = NULL;
				}
				else if (indicesNum == 0)
				{
					pNode->indices = NULL;
//					pNode->childs = pool.allocate<KMeansNode>(branching);
//					memset(pNode->childs, 0, branching*sizeof(KMeansNode));
				}
				else
				{
					cerr << "Unexpected case!!\n";
				}

				if (childNum > 0)
				{
					if (childNum != branching)
					{
						cerr << "Unexpected case in the number of children!!!\n";
					}
					pNode->childs = pool.allocate<KMeansNode>(childNum);
					memset(pNode->childs, 0, childNum*sizeof(KMeansNode));
				}
				else
				{
					pNode->childs = NULL;
				}
//			}
//			else
//			{
//				pNode->indices = NULL;
//				pNode->childs = NULL;
//			}

			SkipBlankSpace(inputStream);

			for (int indicesI = 0
					; indicesI < indicesNum
					; ++indicesI)
			{
				int tmpIndex = 0;
				inputStream >> tmpIndex;
				pNode->indices[indicesI] = tmpIndex;
			}

			SkipBlankSpace(inputStream);

			readNodeNum += 1;
		}

	}

	printf("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\
\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\
%d nodes is read.", readNodeNum);

	cerr << "\n";
};

void KMeansTree::WriteOnlyClusterToFile(std::ofstream & outputStream)
{
	bool oldValue = isForClassificationOnly;
	isForClassificationOnly = true;
	WriteToFile(outputStream);
	isForClassificationOnly = oldValue;
};

int KMeansTree::WriteOnlyClusterToFile(std::ofstream & outputStream, const int clusterNum)
{
	bool oldValue = isForClassificationOnly;
	isForClassificationOnly = true;

	// Get cluster center
    if (clusterNum < 1) {
        throw FLANNException("Number of clusters must be at least 1");
    }

    float variance;
    KMeansNode* clusters = new KMeansNode[clusterNum];

    int clusterCount = getMinVarianceClusters(root, clusters, clusterNum, variance);

//    logger.info("Clusters requested: %d, returning %d\n",numClusters, clusterCount);


    // Write cluster and stop at the required clusters
    WriteToFile(outputStream, clusters, clusterCount);




	delete[] clusters;

	isForClassificationOnly = oldValue;

	return clusterCount;
};

void KMeansTree::WriteToFile(std::ofstream & outputStream)
{
	WriteToFile(outputStream, NULL, -1);
}

void KMeansTree::WriteToFile(std::ofstream & outputStream, KMeansNode * pStopNodes, const int stopNodeNum)
{
	bool isStopNodeEnable = (NULL != pStopNodes);

	std::vector<bool> stopNodeVector;

	if (isStopNodeEnable)
	{
		stopNodeVector.resize(GetNodeNum(), false);
		for (int i = 0; i < stopNodeNum; ++i)
		{
			stopNodeVector[pStopNodes[i]->ID] = true;
		}
	}


	if (isStopNodeEnable)
	{
		outputStream << stopNodeNum << " " ;
	}
	else
	{
		outputStream << size_ << " ";
	}

	outputStream << veclen_ << " " << branching << "\n";
	outputStream << max_iter << " " << center_initStorage << "\n";
	outputStream << cb_index << " " << isForClassificationOnly << "\n";

	std::queue<KMeansNode> nodesQueue;
	//nodesQueue.(size_);
	nodesQueue.push(root);
	int nodeID = 0;
	int clusterNum = 0;
	int leavesNum = 0;
	while (!nodesQueue.empty())
	{
		KMeansNode node = nodesQueue.front();
		nodesQueue.pop();

		if (node == NULL)
		{
			cerr << "Empty node!!!\n";
		}

//		cerr << "Writing node " << node->ID << node << "\n";

		if (!isStopNodeEnable)
		{
			outputStream << "Node " << node->ID << "\n";
		}
		else
		{
			outputStream << "Node " << nodeID << "\n";
		}
		outputStream << node->level << " ";
        if (_isnan(node->mean_radius))
		{
			cerr << "Mean radius nan problem!!!\n";
		}
		outputStream << node->mean_radius << " ";
		outputStream << node->radius << " ";
		outputStream << node->variance << " ";
		outputStream << node->size << " ";

		bool isStopNode = false;

		if (isStopNodeEnable)
		{
			isStopNode = stopNodeVector[node->ID];
		}

		int childNum = 0;
		if (!isStopNode)
		{
			if (NULL != node->childs)
			{
				for (int i = 0; i < branching && i < node->size; ++i)
				{
					if (NULL != node->childs[i])
					{
						childNum ++;
					}
				}
			}
		}
		outputStream << childNum << " ";

		outputStream << "\n";
		outputStream << "NodePivot:\n";
		if (NULL != node->pivot)
		{
//			cerr << "Writing privot.\n";
			for (int i = 0; i < veclen_; ++i)
			{
				outputStream << node->pivot[i] << " ";
			}
		}
		outputStream << "\n";

		if (NULL != node->indices && !isForClassificationOnly)
		{
//			cerr << "Writing indices.\n";
			leavesNum += node->size;
			outputStream << "NodeIndices: " << node->size << "\n";

			for (int i = 0; i < node->size; ++i)
			{
				if (NULL != node->indices && !isForClassificationOnly)
				{
					outputStream << node->indices[i] << " ";
				}
			}
		}
		else
		{
//			cerr << "Writing indices.\n";
			outputStream << "NodeIndices: 0\n";
		}

		outputStream << "\n\n";

		if (NULL != node->childs && !isStopNode)
		{
//			cerr << "Inserting queue.\n";
			int childNum = 0;
			for (int i = 0; i < branching && i < node->size; ++i)
			{
				if (NULL != node->childs[i])
				{

					nodesQueue.push(node->childs[i]);
					childNum ++;
				}
			}
			if (childNum != branching)
			{
				cerr << childNum << " children are found. It is unexpected case!\n";
			}
		}
		else
		{
			clusterNum += 1;
		}

		if (nodeID%371 == 0)
		{
			printf("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\
\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\
%d nodes is written.", nodeID);
		}

		nodeID += 1;

	}


	printf("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\
\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\
%d nodes is written.", nodeID);
	cerr << endl;
	cerr << leavesNum << " leaves are written!\n";
	cerr << clusterNum << " leaves clusters are written!\n";
};

int KMeansTree::GetNodeNum()
{
	if (cachedNodeNum < 0)
	{
		std::queue<KMeansNode> nodesQueue;
		if (root == NULL)
		{
			cachedNodeNum = 0;
		}
		else
		{
			//cachedNodeNum = 0;
			int id = 0;
			nodesQueue.push(root);
			while (!nodesQueue.empty())
			{
				KMeansNode pNode = nodesQueue.front();
				pNode->ID = id;
				id += 1;

				if (pNode->childs != NULL)
				{
					for (int i = 0; i < branching; ++i)
					{
						if (NULL != pNode->childs[i])
						{
							nodesQueue.push(pNode->childs[i]);
						}
					}
				}
				nodesQueue.pop();
			}
			cachedNodeNum = id;
		}
	}

	return cachedNodeNum;
};

int KMeansTree::GetDepth()
{
	if (cachedTreeDepth < 0)
	{
		std::queue<KMeansNode> nodesQueue;
		if (root == NULL)
		{
			cachedTreeDepth = 0;
		}
		else
		{
			//cachedNodeNum = 0;
			int id = 0;
			nodesQueue.push(root);
			while (!nodesQueue.empty())
			{
				KMeansNode pNode = nodesQueue.front();
				id += 1;

				if (pNode->level > cachedTreeDepth)
				{
					cachedTreeDepth = pNode->level;
				}

				if (pNode->childs != NULL)
				{
					for (int i = 0; i < branching; ++i)
					{
						if (NULL != pNode->childs[i])
						{
							nodesQueue.push(pNode->childs[i]);
						}
					}
				}
				nodesQueue.pop();
			}

			cachedTreeDepth += 1;
		}
	}
	return cachedTreeDepth;
}

int KMeansTree::GetLeavesClusterNum()
{
	if (cachedLeavesNodeNum < 0)
	{
		std::queue<KMeansNode> nodesQueue;
		if (root == NULL)
		{
			cachedLeavesNodeNum = 0;
		}
		else
		{
			//cachedNodeNum = 0;
//			int id = 0;
			nodesQueue.push(root);
			while (!nodesQueue.empty())
			{
				KMeansNode pNode = nodesQueue.front();
//				id += 1;

				if (pNode->childs != NULL)
				{
					for (int i = 0; i < branching; ++i)
					{
						if (NULL != pNode->childs[i])
						{
							nodesQueue.push(pNode->childs[i]);
						}
					}
				}
				else
				{
					cachedLeavesNodeNum += 1;
				}
				nodesQueue.pop();
			}
		}
	}
	return cachedLeavesNodeNum;
}

void KMeansTree::InitializeNodeIDs()
{
	std::queue<KMeansNode> nodesQueue;
	if (root == NULL)
		return;

	//root->ID = 0;
	int id = 0;
	nodesQueue.push(root);
	while (!nodesQueue.empty())
	{
		KMeansNode pNode = nodesQueue.front();
		pNode->ID = id;
		id += 1;

		if (pNode->childs != NULL)
		{
			for (int i = 0; i < branching; ++i)
			{
				if (NULL != pNode->childs[i])
				{
					nodesQueue.push(pNode->childs[i]);
				}
			}
		}
		nodesQueue.pop();
	}
}

int KMeansTree::FindCluster(float * vec,const int veclen, const int maxCheck, std::vector<int> * visitedNodeID)
{
	std::vector<KMeansNode> visitedNode;
	KMeansNode pNode = FindCluster(vec, veclen, maxCheck, &visitedNode);
//	KMeansNode pNode = FindCluster(vec, veclen, &visitedNode);
	if (NULL != pNode)
	{
		if (NULL != visitedNodeID)
		{
			visitedNodeID->reserve(visitedNode.size());
			for (size_t i = 0; i < visitedNode.size(); ++i)
			{
				visitedNodeID->push_back(visitedNode[i]->ID);
			}
		}
		return pNode->ID;
	}
	else
	{
		return -1;
	}
};

KMeansTree::KMeansNode KMeansTree::FindClusterUnit(KMeansTree::KMeansNode node
		, float* vec
		, int veclen
		, float & minDis
		, float & maxDis
		, std::vector<KMeansTree::KMeansNode> & visitedNode
		, std::vector<KMeansTree::KMeansNode> & track)
{
	// Ignore those clusters that are too far away
	float bsq = flann_dist(vec, vec+veclen, node->pivot);
	float rsq = node->radius;
	float wsq = maxDis;

	float val = bsq-rsq-wsq;
	float val2 = val*val-4*rsq*wsq;


	if (val>0 && val2>0 && minDis != std::numeric_limits<float>::max())
	{
		return NULL;
	}

	visitedNode.push_back(node);
	KMeansNode pNode = NULL;

	if (node->childs==NULL) {
//		for (int i=0;i<node->size;++i) {
			//result.addPoint(dataset[node->indices[i]], node->indices[i]);
			if (bsq < minDis)
			{
				minDis = bsq;
				track = visitedNode;
				pNode = node;
			}

			if (bsq > maxDis)
			{
				maxDis = bsq;
			}
//		}
	}
	else {
//		int* sort_indices = new int[branching];
////
//		getCenterOrdering(node, vec, sort_indices);

		for (int i=0; i<branching; ++i)
		{
//			KMeansNode tmpNode = FindClusterUnit(node->childs[sort_indices[i]],vec, veclen, minDis, maxDis, visitedNode, track);
			KMeansNode tmpNode = FindClusterUnit(node->childs[i],vec, veclen, minDis, maxDis, visitedNode, track);
			if (NULL != tmpNode)
			{
				pNode = tmpNode;
			}
		}

//		delete[] sort_indices;
	}
	visitedNode.pop_back();
	return pNode;
}

KMeansTree::KMeansNode KMeansTree::FindCluster(float * vec, const int veclen, std::vector<KMeansNode> * visitedNode)
{
	if (NULL == vec)
	{
		cerr << "NULL input vec for cluster finding!!!\n";
		return NULL;
	}
	if (veclen != veclen_)
	{
		cerr << "The length of the input vector doesn't match the length of the internal data in the KMeans Tree!!!\n";
		return NULL;
	}
	if (NULL != visitedNode)
	{
		visitedNode->resize(GetDepth(), NULL);
	}

	KMeansNode pNode = root;
	if (NULL == pNode)
	{
		cerr << "Invalid KMeans Tree!!!\n";
		return NULL;
	}
//	if (NULL != visitedNode)
//	{
//		(*visitedNode)[pNode->level] = pNode;
//	}

//	while (NULL != pNode->childs)
//	{
////		KMeansNode pNextNode = NULL;
//		int minIdx = -1;
//		float minDis = std::numeric_limits<float>::max();
//		for (int i = 0; i < branching; ++i)
//		{
//			float dis = flann_dist(vec, vec+veclen, pNode->childs[i]->pivot);
//
//			if (dis < minDis)
//			{
//				minDis = dis;
//				minIdx = i;
//			}
//		}
//
//		if (minIdx >= 0)
//		{
//			pNode = pNode->childs[minIdx];
//			if (NULL != visitedNode)
//			{
//				(*visitedNode)[pNode->level] = pNode;
//			}
//		}
//	}
	// New not recursive
//	typedef std::pair<KMeansNode, int> StackItem;
//	std::vector<StackItem> visitedStack;
//	visitedStack.reserve(GetDepth());
//	visitedStack.push_back(StackItem(root, 0));
//
//	float nearestDis = std::numeric_limits<float>::max();
//	float farthestDis = std::numeric_limits<float>::min();
//
//	std::vector<KMeansNode> track;
//	track.reserve(GetDepth());
//	track.push_back(root);
//
//
//	while (!visitedStack.empty())
//	{
//		StackItem top = visitedStack.back();
//		const float dis = flann_dist(vec, vec+veclen, top.first->pivot);
//
//		if (NULL != top.first->childs)
//		{
//			// push next level
//			if (top.second < branching)
//			{
//				KMeansNode nextLevelNode = top.first->childs[top.second];
//
//				// Ignore those clusters that are too far away
//
////				float bsq = flann_dist(vec, vec+veclen, nextLevelNode->pivot);
////				float rsq = nextLevelNode->radius;
////				float wsq = farthestDis;
////
////				float val = bsq-rsq-wsq;
////				float val2 = val*val-4*rsq*wsq;
////
////		//  		if (val>0) {
////				if (val>0 && val2>0)
////				{
//////		//			if (NULL != top.first->childs)
//////		//			{
//////		//				if (top.second < branching - 1)
//////		//				{
//////		//					visitedStack.back().second += 1;
//////		//				}
//////		//				else
//////		//				{
//////		//					visitedStack.pop_back();
//////		//					track.pop_back();
//////		//				}
//////		//			}
//////		//			else
//////					{
//////						visitedStack.pop_back();
//////						track.pop_back();
//////					}
////					visitedStack.back().second += 1;
//////					continue;
////				}
////				else
//				{
//					StackItem nextLevelItem = StackItem(nextLevelNode, 0);
//					visitedStack.back().second += 1;
//					visitedStack.push_back(nextLevelItem);
//					track.push_back(nextLevelNode);
//				}
//			}
//			else
//			{
//				if (!visitedStack.empty())
//				{
//					visitedStack.pop_back();
//					track.pop_back();
//				}
//				else
//				{
//					cerr << "Error case when popping visitedStack!!!\n";
//					return NULL;
//				}
//			}
//		}
//		else
//		{
//			// Compute the distance
//
//
//			if (dis < nearestDis)
//			{
//				nearestDis = dis;
//				if (NULL != visitedNode)
//				{
//					(*visitedNode) = track;
//				}
//				pNode = top.first;
//			}
//
//			if (dis > farthestDis)
//			{
//				farthestDis = dis;
//			}
//
//			visitedStack.pop_back();
//			track.pop_back();
//		}
//	}

	// new recursive
//	float rootDis = flann_dist(vec, vec+veclen, root->pivot);
	float minDis = std::numeric_limits<float>::max();
	float maxDis = std::numeric_limits<float>::min();

	std::vector<KMeansNode> track;
	std::vector<KMeansNode> tmpVisitedNode;
	track.reserve(GetDepth());
	tmpVisitedNode.reserve(GetDepth());
	pNode = FindClusterUnit(root, vec, veclen, minDis, maxDis, tmpVisitedNode, track);
	if (NULL != pNode)
	{
		if (static_cast<int>(track.size()) != pNode->level + 1)
		{
			cerr << "The searching of cluster met some problems, because \
the path of traversal ("<< track.size() << ") does not match the depth of the cluster("<< pNode->level <<")!!!\n";
			return NULL;
		}
	}

	if (NULL != pNode)
	{
		if (NULL != visitedNode)
		{
			(*visitedNode) = track;
		}
	}

	return pNode;
};

KMeansTree::~KMeansTree()
{
	if (root != NULL)
	{
		free_centers(root);
	}
	if (NULL != heap)
	{
		delete heap;
	}
    if (indices!=NULL)
    {
	  delete[] indices;
    }
    if (NULL != domain_distances)
    {
    	delete[] domain_distances;
    }
	if (NULL != pDummyDataSet)
	{
		delete pDummyDataSet;
	}
}

void KMeansTree::buildIndex()
{
	indices = new int[size_];
	for (int i=0;i<size_;++i) {
		indices[i] = i;
	}

	root = pool.allocate<KMeansNodeSt>();
	root->mean_radius = 0;
	cerr << "Maximum iteration: " << max_iter << "\n";
	computeNodeStatistics(root, indices, size_);
	computeClustering(root, indices, size_, branching,0);
	InitializeNodeIDs();
}

void KMeansTree::findNeighbors(ResultSet& result, float* vec, Params searchParams)
{
    int maxChecks;
    float cb_index;
    if (searchParams.find("checks") == searchParams.end()) {
        maxChecks = -1;
    }
    else {
        maxChecks = (int)searchParams["checks"];
    }

    if (searchParams.find("cb_index") != searchParams.end()) {
        cb_index = (float)searchParams["cb_index"];
    }


    if (maxChecks<0) {
        findExactNN(root, result, vec);
    }
    else {
        heap->clear();
        int checks = 0;

        findNN(root, result, vec, checks, maxChecks);

        BranchSt branch;
        while (heap->popMin(branch) && (checks<maxChecks || !result.full())) {
            KMeansNode node = branch.node;
            findNN(node, result, vec, checks, maxChecks);
        }
        assert(result.full());
    }
}
KMeansTree::KMeansNode KMeansTree::FindCluster(float * vec, const int veclen, const int maxChecks, std::vector<KMeansTree::KMeansNode> * visitedNode)
{
//	int maxChecks = maxCheck;


	if (maxChecks<0)
	{
		return FindCluster(vec, veclen, visitedNode);
	}
	else {
		KMeansNode pMinNode = NULL;
		heap->clear();
		int checks = 0;
		float minDis = std::numeric_limits<float>::max();
		float maxDis = std::numeric_limits<float>::min();



		std::vector<KMeansNode> tmpVisitedNodes;
		std::vector<KMeansNode> tracks;
		pMinNode = ApproFindClusterUnit(root, vec, veclen, minDis, maxDis, tmpVisitedNodes, tracks, checks, maxChecks);
//
		BranchSt branch;
		while (heap->popMin(branch) && (checks<maxChecks)) {
			KMeansNode node = branch.node;
			KMeansNode pNode = NULL;
			pNode = ApproFindClusterUnit(node, vec, veclen, minDis, maxDis, branch.visitedNodes, tracks, checks, maxChecks);
			if (NULL != pNode)
			{
				pMinNode = pNode;
			}
		}
		if (NULL != pMinNode)
		{
			if (static_cast<int>(tracks.size()) != pMinNode->level + 1)
			{
				cerr << "The searching of cluster met some problems, because \
the path of traversal ("<< tracks.size() << ") does not match the depth of the cluster("<< pMinNode->level <<")!!!\n";
				return NULL;
			}
		}
		if (NULL != visitedNode && NULL != pMinNode)
		{
			(*visitedNode) = tracks;
		}
		return pMinNode;
	}
};


KMeansTree::KMeansNode KMeansTree::ApproFindClusterUnit(KMeansTree::KMeansNode node
		, float* vec
		, int veclen
		, float & minDis
		, float & maxDis
		, std::vector<KMeansTree::KMeansNode> & visitedNodes
		, std::vector<KMeansTree::KMeansNode> & tracks
		, int& checks, const int maxChecks)
{
	// Ignore those clusters that are too far away

	float bsq = flann_dist(vec, vec+veclen, node->pivot);
	float rsq = node->radius;
	float wsq = maxDis;

	float val = bsq-rsq-wsq;
	float val2 = val*val-4*rsq*wsq;

	//if (val>0) {
	if (val>0 && val2>0 && minDis != std::numeric_limits<float>::max()) {
		return NULL;
	}

	visitedNodes.push_back(node);
	KMeansNode pNode = NULL;


	if (node->childs==NULL)
	{
        if (checks>=maxChecks)
        {
            return NULL;
        }
        checks += 1;

		if (bsq < minDis)
		{
			minDis = bsq;
			tracks = visitedNodes;
			pNode = node;
		}

		if (bsq > maxDis)
		{
			maxDis = bsq;
		}


	}
	else {

		int closest_center = exploreNodeBranches(node, vec, visitedNodes);

		pNode = ApproFindClusterUnit(node->childs[closest_center], vec, veclen, minDis, maxDis, visitedNodes, tracks, checks, maxChecks);
	}
	visitedNodes.pop_back();
	return pNode;
};

int KMeansTree::getClusterCenters(int numClusters, float* centers)
{
    if (numClusters<1) {
        throw FLANNException("Number of clusters must be at least 1");
    }

    float variance;
    KMeansNode* clusters = new KMeansNode[numClusters];

    int clusterCount = getMinVarianceClusters(root, clusters, numClusters, variance);

//         logger.info("Clusters requested: %d, returning %d\n",numClusters, clusterCount);


    for (int i=0;i<clusterCount;++i) {
        float* center = clusters[i]->pivot;
        for (int j=0;j<veclen_;++j) {
            centers[j] = center[j];
        }
        centers += veclen_;
    }
	delete[] clusters;

    return clusterCount;
}

Params KMeansTree::estimateSearchParams(float precision, Dataset<float>* testset)
{
    Params params;

    return params;
}

void KMeansTree::free_centers(KMeansNode node)
{
    delete[] node->pivot;
    if (node->childs!=NULL) {
        for (int k=0;k<branching;++k) {
            free_centers(node->childs[k]);
        }
    }
}

void KMeansTree::computeNodeStatistics(KMeansNode node, int* indices, int indices_length) {

	float radius = 0;
	float variance = 0;
	float mean_radius = 0;
	float* mean = new float[veclen_];
	memoryCounter += veclen_*sizeof(float);

    memset(mean,0,veclen_*sizeof(float));

	for (int i=0;i<size_;++i) {
		float* vec = dataset[indices[i]];
        for (int j=0;j<veclen_;++j) {
            mean[j] += vec[j];
        }
		variance += flann_dist(vec,vec+veclen_,zero);
	}
	for (int j=0;j<veclen_;++j) {
		mean[j] /= size_;
	}
	variance /= size_;
	variance -= flann_dist(mean,mean+veclen_,zero);

	float tmp = 0;
	for (int i=0;i<indices_length;++i) {
		tmp = flann_dist(mean, mean + veclen_, dataset[indices[i]]);
		mean_radius += tmp;
		if (tmp>radius) {
			radius = tmp;
		}
	}

	node->variance = variance;
	node->radius = radius;
	node->pivot = mean;
	node->mean_radius = mean_radius / static_cast<float>(indices_length);
}

//extern flann_distance_t flann_distance_type;
void KMeansTree::computeClustering(KMeansNode node, int* indices, int indices_length, int branching, int level)
{
	node->size = indices_length;
	node->level = level;
	node->indices = NULL;
	node->childs = NULL;

#ifdef DEBUGCLUSTERING
	cerr << "level: " << level << "\n";
#endif

	if (indices_length < branching) {
		node->indices = indices;
        sort(node->indices,node->indices+indices_length);
        node->childs = NULL;
		return;
	}

#ifdef DEBUGCLUSTERING
	StartStopTimer timer;
	timer.start();
	cerr << "Computing initializing cluster...";
#endif

	float** initial_centers = new float*[branching];
	memset(initial_centers, 0,sizeof(float*)*branching); //Added by Tian
    int centers_length;
	chooseCenters(branching, dataset, indices, indices_length, initial_centers, centers_length);


	if (centers_length < branching && indices_length > branching)
	{
#ifdef DEBUGCLUSTERING
		cerr << "The predefined cluster choosing algorithm doesn't work. "
			 <<	"A random cluster choosing algorithm is used instead.";
#endif
		memset(initial_centers, 0, sizeof(float*)*branching);
		chooseCentersRandom(branching, dataset, indices, indices_length, initial_centers, centers_length);
	}

	if (centers_length<branching && indices_length == branching) {
        node->indices = indices;
        sort(node->indices,node->indices+indices_length);
        node->childs = NULL;
		return;
	}


#ifdef DEBUGCLUSTERING
	timer.stop();
	cerr << "Takes " << timer.value << " second(s)\n";
	cerr << "Assigning initial cluster...";
	cerr << "Distance function " << flann_distance_type;
	timer.reset();
	timer.start();
#endif

#ifdef DEBUGCLUSTERING
    cerr << "\nCopy centers...\n";
#endif
    Dataset<double> dcenters(branching,veclen_);
    for (int i=0; i<centers_length; ++i) {
        for (int k=0; k<veclen_; ++k) {
            dcenters[i][k] = double(initial_centers[i][k]);
        }
    }
	delete[] initial_centers;


 	float* radiuses = new float[branching];
	int* count = new int[branching];
    for (int i=0;i<branching;++i) {
        radiuses[i] = 0;
        count[i] = 0;
    }

    //	assign points to clusters
	int* belongs_to = new int[indices_length];
	memset(belongs_to,0,sizeof(int)*indices_length); //Added by Tian
	for (int i=0;i<indices_length;++i) {

		float sq_dist = flann_dist(dataset[indices[i]], dataset[indices[i]] + veclen_ ,dcenters[0]);
		belongs_to[i] = 0;
		for (int j=1;j<branching;++j) {
			float new_sq_dist = flann_dist(dataset[indices[i]], dataset[indices[i]]+veclen_, dcenters[j]);
			if (sq_dist>new_sq_dist) {
				belongs_to[i] = j;
				sq_dist = new_sq_dist;
			}
		}
        if (sq_dist>radiuses[belongs_to[i]]) {
            radiuses[belongs_to[i]] = sq_dist;
        }
		count[belongs_to[i]]++;
	}


	for (int i=0;i<branching;++i)
	{
		// if one cluster converges to an empty cluster,
		// move an element into that cluster
		if (count[i]==0) {
			int j = (i+1)%branching;
			int trial = 0;
			while (count[j]<=1) {
				j = (j+1)%branching;
				trial += 1;
				if (trial > branching)
				{
					cerr << "Trapped in infinite loops!!!\n";
				}
			}

			for (int k=0;k<indices_length;++k)
			{
				if (belongs_to[k]==j)
				{
					belongs_to[k] = i;
					count[j]--;
					count[i]++;
					break;
				}
			}
		}
	}

	bool converged = false;
	int iteration = 0;

#ifdef DEBUGCLUSTERING
	timer.stop();
	cerr << "Takes " << timer.value << " second(s)\n";
	cerr << "Begin to refine cluster...\n";
#endif

	while (!converged && iteration<max_iter) {
		converged = true;
		iteration++;

#ifdef DEBUGCLUSTERING
		cerr << "Current cluster: \n";
		for (int bi = 0; bi < branching; ++bi)
		{
			cerr << count[bi] << " ";
//			for (int ci = 0; ci < veclen_; ++ci)
//			{
//				cerr << dcenters[bi][ci] << " ";
//			}
		}
		cerr << "\n";
		cerr << "Computing new cluster center...";
		timer.reset();
		timer.start();
#endif
		// compute the new cluster centers
		for (int i=0;i<branching;++i) {
            memset(dcenters[i],0,sizeof(double)*veclen_);
            radiuses[i] = 0;
		}
        for (int i=0;i<indices_length;++i) {
			float* vec = dataset[indices[i]];
			double* center = dcenters[belongs_to[i]];
			for (int k=0;k<veclen_;++k) {
				center[k] += vec[k];
				}
		}
		for (int i=0;i<branching;++i) {
            int cnt = count[i];
            for (int k=0;k<veclen_;++k) {
                dcenters[i][k] /= cnt;
            }
		}

#ifdef DEBUGCLUSTERING
		timer.stop();
		cerr << "Takes " << timer.value << " second(s)\n";
		cerr << "Reassign clusters...";
		cerr << "Distance function " << flann_distance_type;
		timer.reset();
		timer.start();
#endif
		// reassign points to clusters
		for (int i=0;i<indices_length;++i) {
			float sq_dist = flann_dist(dataset[indices[i]], dataset[indices[i]]+veclen_ ,dcenters[0]);
			if (_isnan(sq_dist))
			{
				cerr << "NaN error!\n";
			}
			int new_centroid = 0;
			for (int j=1;j<branching;++j) {
				float new_sq_dist = flann_dist(dataset[indices[i]], dataset[indices[i]]+veclen_,dcenters[j]);
				if (_isnan(new_sq_dist))
				{
					cerr << "NaN error!\n";
				}
				if (sq_dist>new_sq_dist) {
					new_centroid = j;
					sq_dist = new_sq_dist;
				}
			}

			if (sq_dist>radiuses[new_centroid]) {
				radiuses[new_centroid] = sq_dist;
			}
			if (new_centroid != belongs_to[i]) {
				count[belongs_to[i]]--;
				count[new_centroid]++;
				belongs_to[i] = new_centroid;

				converged = false;
			}
		}
#ifdef DEBUGCLUSTERING
		timer.stop();
		cerr << "Takes " << timer.value << " second(s)\n";
		cerr << "Handle empty cluster...";
		timer.reset();
		timer.start();
#endif
		for (int i=0;i<branching;++i)
		{
			// if one cluster converges to an empty cluster,
			// move an element into that cluster
			if (count[i]==0) {
				int j = (i+1)%branching;
				int trial = 0;
				while (count[j]<=1) {
					j = (j+1)%branching;
					trial += 1;
					if (trial > branching)
					{
						cerr << "Trapped in infinite loops!!!\n";
					}
				}

				for (int k=0;k<indices_length;++k)
				{
					if (belongs_to[k]==j)
					{
						belongs_to[k] = i;
						count[j]--;
						count[i]++;
						break;
					}
				}
				converged = false;
			}
		}
#ifdef DEBUGCLUSTERING
		timer.stop();
		cerr << "Takes " << timer.value << " second(s)\n";
#endif

	}

    float** centers = new float*[branching];

    for (int i=0; i<branching; ++i) {
			centers[i] = new float[veclen_];
			memoryCounter += veclen_*sizeof(float);
        for (int k=0; k<veclen_; ++k) {
            centers[i][k] = dcenters[i][k];
        }
		}

#ifdef DEBUGCLUSTERING
	cerr << "Finalizing cluster...\n";
#endif

	// compute kmeans clustering for each of the resulting clusters
	node->childs = pool.allocate<KMeansNode>(branching);
	memset(node->childs, 0, branching*sizeof(KMeansNode));
	int start = 0;
	int end = start;
	for (int c=0;c<branching;++c) {
		int s = count[c];

		float variance = 0;
		float mean_radius =0;
		for (int i=0;i<indices_length;++i) {
			if (belongs_to[i]==c) {
				float d = flann_dist(dataset[indices[i]],dataset[indices[i]]+veclen_,zero);
				variance += d;
				mean_radius += sqrt(d);
				swap(indices[i],indices[end]);
				swap(belongs_to[i],belongs_to[end]);
				end++;
			}
		}
		variance /= s;
		mean_radius /= s;
		variance -= flann_dist(centers[c],centers[c]+veclen_,zero);

		node->childs[c] = pool.allocate<KMeansNodeSt>();
		node->childs[c]->ID = 0;
		node->childs[c]->childs = NULL;
		node->childs[c]->indices = NULL;
		node->childs[c]->level = -1;
		node->childs[c]->size = 0;
		node->childs[c]->radius = radiuses[c];
		node->childs[c]->pivot = centers[c];
		node->childs[c]->variance = variance;
		node->childs[c]->mean_radius = mean_radius;
		computeClustering(node->childs[c],indices+start, end-start, branching, level+1);
		start=end;
	}

	delete[] centers;
	delete[] radiuses;
	delete[] count;
	delete[] belongs_to;
}


void KMeansTree::findNN(KMeansNode node, ResultSet& result, float* vec, int& checks, int maxChecks)
{
	// Ignore those clusters that are too far away
	{
		float bsq = flann_dist(vec, vec+veclen_, node->pivot);
		float rsq = node->radius;
		float wsq = result.worstDist();

		float val = bsq-rsq-wsq;
		float val2 = val*val-4*rsq*wsq;

 		//if (val>0) {
		if (val>0 && val2>0) {
			return;
		}
	}

	if (node->childs==NULL) {
        if (checks>=maxChecks) {
            if (result.full()) return;
        }
        checks += node->size;
		for (int i=0;i<node->size;++i) {
			result.addPoint(dataset[node->indices[i]], node->indices[i]);
		}
	}
	else {
		int closest_center = exploreNodeBranches(node, vec);
		findNN(node->childs[closest_center],result,vec, checks, maxChecks);
	}
}

int KMeansTree::exploreNodeBranches(KMeansNode node, float* q)
{

	int best_index = 0;
	domain_distances[best_index] = flann_dist(q,q+veclen_,node->childs[best_index]->pivot);
	for (int i=1;i<branching;++i) {
		domain_distances[i] = flann_dist(q,q+veclen_,node->childs[i]->pivot);
		if (domain_distances[i]<domain_distances[best_index]) {
			best_index = i;
		}
	}

//	float* best_center = node->childs[best_index]->pivot;
	for (int i=0;i<branching;++i) {
		if (i != best_index) {
			domain_distances[i] -= cb_index*node->childs[i]->variance;

//				float dist_to_border = getDistanceToBorder(node.childs[i].pivot,best_center,q);
//				if (domain_distances[i]<dist_to_border) {
//					domain_distances[i] = dist_to_border;
//				}
			heap->insert(BranchSt::make_branch(node->childs[i],domain_distances[i]));
		}
	}

	return best_index;
}

int KMeansTree::exploreNodeBranches(KMeansNode node, float* q,const std::vector<KMeansTree::KMeansNode> & visitedNodes)
{

	int best_index = 0;
	domain_distances[best_index] = flann_dist(q,q+veclen_,node->childs[best_index]->pivot);
	for (int i=1;i<branching;++i) {
		domain_distances[i] = flann_dist(q,q+veclen_,node->childs[i]->pivot);
		if (domain_distances[i]<domain_distances[best_index]) {
			best_index = i;
		}
	}

//	float* best_center = node->childs[best_index]->pivot;
	for (int i=0;i<branching;++i) {
		if (i != best_index) {
			domain_distances[i] -= cb_index*node->childs[i]->variance;


			heap->insert(BranchSt(node->childs[i],domain_distances[i], visitedNodes));
		}
	}

	return best_index;
}

void KMeansTree::findExactNN(KMeansNode node, ResultSet& result, float* vec)
{
	// Ignore those clusters that are too far away
	{
		float bsq = flann_dist(vec, vec+veclen_, node->pivot);
		float rsq = node->radius;
		float wsq = result.worstDist();

		float val = bsq-rsq-wsq;
		float val2 = val*val-4*rsq*wsq;

//  		if (val>0) {
		if (val>0 && val2>0) {
			return;
		}
	}


	if (node->childs==NULL) {
		for (int i=0;i<node->size;++i) {
			result.addPoint(dataset[node->indices[i]], node->indices[i]);
		}
	}
	else {
		int* sort_indices = new int[branching];

		getCenterOrdering(node, vec, sort_indices);

		for (int i=0; i<branching; ++i) {
				findExactNN(node->childs[sort_indices[i]],result,vec);
		}

		delete[] sort_indices;
	}
}

void KMeansTree::getCenterOrdering(KMeansNode node, float* q, int* sort_indices)
{
	for (int i=0;i<branching;++i) {
		float dist = flann_dist(q, q+veclen_, node->childs[i]->pivot);

		int j=0;
		while (domain_distances[j]<dist && j<i) j++;
		for (int k=i;k>j;--k) {
			domain_distances[k] = domain_distances[k-1];
			sort_indices[k] = sort_indices[k-1];
		}
		domain_distances[j] = dist;
		sort_indices[j] = i;
	}
}

float KMeansTree::getDistanceToBorder(float* p, float* c, float* q)
{
	float sum = 0;
	float sum2 = 0;

	for (int i=0;i<veclen_; ++i) {
		float t = c[i]-p[i];
		sum += t*(q[i]-(c[i]+p[i])/2);
		sum2 += t*t;
	}

	return sum*sum/sum2;
}

int KMeansTree::getMinVarianceClusters(KMeansNode root, KMeansNode* clusters, int clusters_length, float& varianceValue)
{
	int clusterCount = 1;
	clusters[0] = root;

	float meanVariance = root->variance*root->size;

	while (clusterCount<clusters_length) {
		float minVariance = numeric_limits<float>::max();
		int splitIndex = -1;

		for (int i=0;i<clusterCount;++i) {
			if (clusters[i]->childs != NULL) {

				float variance = meanVariance - clusters[i]->variance*clusters[i]->size;

				for (int j=0;j<branching;++j) {
				 	variance += clusters[i]->childs[j]->variance*clusters[i]->childs[j]->size;
				}
				if (variance<minVariance) {
					minVariance = variance;
					splitIndex = i;
				}
			}
		}

		if (splitIndex==-1) break;
		if ( (branching+clusterCount-1) > clusters_length) break;

		meanVariance = minVariance;

		// split node
		KMeansNode toSplit = clusters[splitIndex];
		clusters[splitIndex] = toSplit->childs[0];
		for (int i=1;i<branching;++i) {
			clusters[clusterCount++] = toSplit->childs[i];
		}
	}

	varianceValue = meanVariance/root->size;
	return clusterCount;
}
