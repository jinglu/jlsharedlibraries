/*
Copyright 2008-2009  Marius Muja (mariusm@cs.ubc.ca). All rights reserved.
Copyright 2008-2009  David G. Lowe (lowe@cs.ubc.ca). All rights reserved.

THE BSD LICENSE

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef KMEANSTREE_H
#define KMEANSTREE_H

#ifdef WIN32
/* win32 dll export/import directives */
#ifdef flann_EXPORTS
#define LIBSPEC __declspec(dllexport)
#else
#define LIBSPEC __declspec(dllimport)
#endif
#else
/* unix needs nothing */
#define LIBSPEC
#endif

#include "constants.h"
#include <algorithm>
#include <string>
#include <map>
#include <cassert>
#include <limits>
#include <cmath>
#include <vector>
#include <fstream>
#include "NNIndex.h"
#include "util/Dataset.h"
#include "util/Allocator.h"

using namespace std;


template <typename T>
class Heap;

template <typename T>
struct BranchStruct;



typedef void (*centersAlgFunction)(int, Dataset<float>&, int*, int, float**, int&);


//* Chooses the initial centers in the k-means clustering in a random manner.
//*
//* Params:
//*     k = number of centers
//*     vecs = the dataset of points
//*     indices = indices in the dataset
//*     indices_length = length of indices vector
//*
//*/
void chooseCentersRandom(int k, Dataset<float>& vecs, int* indices, int indices_length, float** centers, int& centers_length);
//
//
///**
//* Chooses the initial centers in the k-means using Gonzales' algorithm
//* so that the centers are spaced apart from each other.
//*
//* Params:
//*     k = number of centers
//*     vecs = the dataset of points
//*     indices = indices in the dataset
//* Returns:
//*/
void chooseCentersGonzales(int k, Dataset<float>& vecs, int* indices, int indices_length, float** centers, int& centers_length);
//
//
///**
//* Chooses the initial centers in the k-means using the algorithm
//* proposed in the KMeans++ paper:
//* Arthur, David; Vassilvitskii, Sergei - k-means++: The Advantages of Careful Seeding
//*
//* Implementation of this function was converted from the one provided in Arthur's code.
//*
//* Params:
//*     k = number of centers
//*     vecs = the dataset of points
//*     indices = indices in the dataset
//* Returns:
//*/
void chooseCentersKMeanspp(int k, Dataset<float>& vecs, int* indices, int indices_length, float** centers, int& centers_length);








/**
 * Hierarchical kmeans index
 *
 * Contains a tree constructed through a hierarchical kmeans clustering
 * and other information for indexing a set of points for nearest-neighbor matching.
 */
class KMeansTree : public NNIndex
{
public:
	/**
	 * Struture representing a node in the hierarchical k-means tree.
	 */
	struct KMeansNodeSt	{
    	KMeansNodeSt():ID(-1), pivot(NULL), radius(0)
    	, mean_radius(0), variance(0), size(0), childs(NULL)
    	, indices(NULL), level(-1){}
    	/*
    	 * The id of this node
    	 */
    	int ID;
		/**
		 * The cluster center.
		 */
		float* pivot;
		/**
		 * The cluster squared radius.
		 */
		float radius;
		/**
		 * The cluster mean radius.
		 */
		float mean_radius;
		/**
		 * The cluster variance.
		 */
		float variance;
		/**
		 * The cluster size (number of points in the cluster)
		 */
		int size;
		/**
		 * Child nodes (only for non-terminal nodes)
		 */
		KMeansNodeSt** childs;
		/**
		 * Node points (only for terminal nodes)
		 */
		int* indices;
		/**
		 * Level
		 */
		int level;
	};
    typedef KMeansNodeSt* KMeansNode;
protected:

    /*!
     *
     */
    int cachedNodeNum;

    /*!
	 *
	 */
	int cachedTreeDepth;

	/*!
	 *
	 */
	int cachedLeavesNodeNum;

	/*!
	 * Store the center_init method for persistence.
	 * Added by FANG Tian
	 */
	int center_initStorage;

	/*!
	 * Indicate whether this tree is only for classification.
	 * By the original design of FLANN, this value is false.
	 * Added by FANG Tian
	 */
	bool isForClassificationOnly;

	/**
	 * The branching factor used in the hierarchical k-means clustering
	 */
	int branching;

	/**
	 * Maximum number of iterations to use when performing k-means
	 * clustering
	 */
	int max_iter;

     /**
     * Cluster border index. This is used in the tree search phase when determining
     * the closest cluster to explore next. A zero value takes into account only
     * the cluster centers, a value greater then zero also take into account the size
     * of the cluster.
     */
    float cb_index;

	/**
	 * The dataset used by this index
	 */
    Dataset<float>& dataset;

    /**
    * Number of eatures in the dataset.
    */
    int size_;

    /**
    * Length of each feature.
    */
    int veclen_;


    /**
     * Alias definition for a nicer syntax.
     */
    typedef BranchStruct<KMeansNode> BranchSt;

    /**
     * Priority queue storing intermediate branches in the best-bin-first search
     */
    Heap<BranchSt>* heap;



	/**
	 * The root node in the tree.
	 */
	KMeansNode root;

	/**
	 *  Array of indices to vectors in the dataset.
	 */
	int* indices;


	/**
	 * Pooled memory allocator.
	 *
	 * Using a pooled memory allocator is more efficient
	 * than allocating memory directly when there is a large
	 * number small of memory allocations.
	 */
	PooledAllocator pool;

	/**
	 * Memory occupied by the index.
	 */
	int memoryCounter;


	/**
	 * Array with distances to the kmeans domains of a node.
	 * Used during search phase.
	 */
	float* domain_distances;

    /**
    * The function used for choosing the cluster centers.
    */
    centersAlgFunction chooseCenters;


    Dataset<float> * pDummyDataSet;

    map<flann_centers_init_t,centersAlgFunction> centerAlgs;

    void centers_init()
    {
        centerAlgs[CENTERS_RANDOM] = &chooseCentersRandom;
        centerAlgs[CENTERS_GONZALES] = &chooseCentersGonzales;
        centerAlgs[CENTERS_KMEANSPP] = &chooseCentersKMeanspp;
    }
    /*
     * Added by Tian
     */
	void LoadFromFile(std::ifstream  & inputStream);
	void WriteToFile(std::ofstream & outputStream, KMeansNode * pStopNodes, const int stopNodeNum);

	KMeansTree::KMeansNode FindClusterUnit(KMeansTree::KMeansNode node
			, float* vec
			, int veclen
			, float & minDis
			, float & maxDis
			, std::vector<KMeansTree::KMeansNode> & visitedNode
			, std::vector<KMeansTree::KMeansNode> & track);
	KMeansTree::KMeansNode ApproFindClusterUnit(KMeansTree::KMeansNode node
			, float* vec
			, int veclen
			, float & minDis
			, float & maxDis
			, std::vector<KMeansTree::KMeansNode> & visitedNode
			, std::vector<KMeansTree::KMeansNode> & track
			, int& checks, const int maxChecks);

	int exploreNodeBranches(KMeansNode node, float* q, const std::vector<KMeansTree::KMeansNode> & visitedNodes);


public:



    flann_algorithm_t getType() const
    {
        return KMEANS;
    }

	/**
	 * Index constructor
	 *
	 * Params:
	 * 		inputData = dataset with the input features
	 * 		params = parameters passed to the hierarchical k-means algorithm
	 */
	KMeansTree(Dataset<float>& inputData, Params params);

	KMeansTree(std::ifstream & inputStream, Dataset<float>& inputData);

	KMeansTree(std::ifstream & inputStream);

	/*
	 * @brief The file doesn't contain the underlying data.
	 * It only contains the clustering tree structure and the leaves
	 * pointing to the data by an integer index.
	 * ------------------FORMAT----------------------------
	 * NumberOfDataPoint DataDimension Branching
	 * max_iter centerInitStratigy
	 * cb_index
	 *
	 * (Each node is inserted as BFS)
	 * "Node #:"
	 * level mean_radius radius variance size
	 * "NodePivot:"
	 * array for the pivot in one line.
	 * "NodeIndices: #" (# is the number of leaves which are
	 * the indices of the underlying data in this cluster node)
	 * array for the indices
	 */

	void WriteToFile(std::ofstream & outputStream);
	void WriteOnlyClusterToFile(std::ofstream & outputStream);
	int WriteOnlyClusterToFile(std::ofstream & outputStream, const int clusterNum);

	int GetNodeNum();
	int GetDepth();
	int GetLeavesClusterNum();

	/*!
	 * @brief Find which cluster the input vec belongs to.
	 * @param vec The input vector
	 * @param veclen The length of input vector
	 * @param visitedNodeID A vector buffer for storing the ID of the visited nodes during finding.
	 * @return The ID of the found cluster. -1 if error occurs.
	 */
	int FindCluster(float * vec
			, const int veclen
			, const int maxCheck
			, std::vector<int> * visitedNodeID);

	KMeansTree::KMeansNode FindCluster(float * vec
			, const int veclen
			, const int maxCheck
			, std::vector<KMeansTree::KMeansNode> * visitedNode);
	KMeansTree::KMeansNode FindCluster(float * vec
			, const int veclen
			, std::vector<KMeansTree::KMeansNode> * visitedNode);


	/**
	 * Index destructor.
	 *
	 * Release the memory used by the index.
	 */
	virtual ~KMeansTree();

    /**
    *  Returns size of index.
    */
    int size() const
    {
        return size_;
    }

    /**
    * Returns the length of an index feature.
    */
    int veclen() const
    {
        return veclen_;
    }


    void set_cb_index( float index)
    {
        cb_index = index;
    }


	/**
	 * Computes the inde memory usage
	 * Returns: memory used by the index
	 */
	int usedMemory() const
	{
		return  pool.usedMemory+pool.wastedMemory+memoryCounter;
	}

	/**
	 * Builds the index
	 */
	void buildIndex();

    /**
     * Find set of nearest neighbors to vec. Their indices are stored inside
     * the result object.
     *
     * Params:
     *     result = the result object in which the indices of the nearest-neighbors are stored
     *     vec = the vector for which to search the nearest neighbors
     *     searchParams = parameters that influence the search algorithm (checks, cb_index)
     */
    void findNeighbors(ResultSet& result, float* vec, Params searchParams);


    /**
     * Clustering function that takes a cut in the hierarchical k-means
     * tree and return the clusters centers of that clustering.
     * Params:
     *     numClusters = number of clusters to have in the clustering computed
     * Returns: number of cluster centers
     */
    int getClusterCenters(int numClusters, float* centers);

    Params estimateSearchParams(float precision, Dataset<float>* testset = NULL);

    inline bool IsForClassificationOnly() const { return isForClassificationOnly; };

    void InitializeNodeIDs();

private:


    /**
    * Helper function
    */
    void free_centers(KMeansNode node);

	/**
	 * Computes the statistics of a node (mean, radius, variance).
	 *
	 * Params:
	 *     node = the node to use
	 *     indices = the indices of the points belonging to the node
	 */
	void computeNodeStatistics(KMeansNode node, int* indices, int indices_length);


	/**
	 * The method responsible with actually doing the recursive hierarchical
	 * clustering
	 *
	 * Params:
	 *     node = the node to cluster
	 *     indices = indices of the points belonging to the current node
	 *     branching = the branching factor to use in the clustering
	 *
	 * TODO: for 1-sized clusters don't store a cluster center (it's the same as the single cluster point)
	 */
	void computeClustering(KMeansNode node, int* indices, int indices_length, int branching, int level);

	/**
	 * Performs one descent in the hierarchical k-means tree. The branches not
	 * visited are stored in a priority queue.
     *
     * Params:
     *      node = node to explore
     *      result = container for the k-nearest neighbors found
     *      vec = query points
     *      checks = how many points in the dataset have been checked so far
     *      maxChecks = maximum dataset points to checks
     */


	void findNN(KMeansNode node, ResultSet& result, float* vec, int& checks, int maxChecks);

	/**
	 * Helper function that computes the nearest childs of a node to a given query point.
	 * Params:
	 *     node = the node
	 *     q = the query point
	 *     distances = array with the distances to each child node.
	 * Returns:
	 */
	int exploreNodeBranches(KMeansNode node, float* q);


	/**
	 * Function the performs exact nearest neighbor search by traversing the entire tree.
	 */
	void findExactNN(KMeansNode node, ResultSet& result, float* vec);


	/**
	 * Helper function.
	 *
	 * I computes the order in which to traverse the child nodes of a particular node.
	 */
	void getCenterOrdering(KMeansNode node, float* q, int* sort_indices);

	/**
	 * Method that computes the squared distance from the query point q
	 * from inside region with center c to the border between this
	 * region and the region with center p
	 */
	float getDistanceToBorder(float* p, float* c, float* q);

	/**
	 * Helper function the descends in the hierarchical k-means tree by spliting those clusters that minimize
	 * the overall variance of the clustering.
	 * Params:
	 *     root = root node
	 *     clusters = array with clusters centers (return value)
	 *     varianceValue = variance of the clustering (return value)
	 * Returns:
	 */
	int getMinVarianceClusters(KMeansNode root, KMeansNode* clusters, int clusters_length, float& varianceValue);

};



register_index(KMEANS,KMeansTree)


#endif //KMEANSTREE_H
